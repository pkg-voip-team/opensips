/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_CFG_TAB_H_INCLUDED
# define YY_YY_CFG_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    FORWARD = 258,
    SEND = 259,
    DROP = 260,
    ASSERT = 261,
    EXIT = 262,
    RETURN = 263,
    LOG_TOK = 264,
    ERROR = 265,
    ROUTE = 266,
    ROUTE_FAILURE = 267,
    ROUTE_ONREPLY = 268,
    ROUTE_BRANCH = 269,
    ROUTE_ERROR = 270,
    ROUTE_LOCAL = 271,
    ROUTE_STARTUP = 272,
    ROUTE_TIMER = 273,
    ROUTE_EVENT = 274,
    SET_HOST = 275,
    SET_HOSTPORT = 276,
    PREFIX = 277,
    STRIP = 278,
    STRIP_TAIL = 279,
    APPEND_BRANCH = 280,
    REMOVE_BRANCH = 281,
    PV_PRINTF = 282,
    SET_USER = 283,
    SET_USERPASS = 284,
    SET_PORT = 285,
    SET_URI = 286,
    REVERT_URI = 287,
    SET_DSTURI = 288,
    RESET_DSTURI = 289,
    ISDSTURISET = 290,
    FORCE_RPORT = 291,
    FORCE_LOCAL_RPORT = 292,
    FORCE_TCP_ALIAS = 293,
    IF = 294,
    ELSE = 295,
    SWITCH = 296,
    CASE = 297,
    DEFAULT = 298,
    SBREAK = 299,
    WHILE = 300,
    FOR = 301,
    IN = 302,
    SET_ADV_ADDRESS = 303,
    SET_ADV_PORT = 304,
    FORCE_SEND_SOCKET = 305,
    SERIALIZE_BRANCHES = 306,
    NEXT_BRANCHES = 307,
    USE_BLACKLIST = 308,
    UNUSE_BLACKLIST = 309,
    MAX_LEN = 310,
    SETDEBUG = 311,
    SETFLAG = 312,
    RESETFLAG = 313,
    ISFLAGSET = 314,
    SETBFLAG = 315,
    RESETBFLAG = 316,
    ISBFLAGSET = 317,
    SETSFLAG = 318,
    RESETSFLAG = 319,
    ISSFLAGSET = 320,
    METHOD = 321,
    URI = 322,
    FROM_URI = 323,
    TO_URI = 324,
    SRCIP = 325,
    SRCPORT = 326,
    DSTIP = 327,
    DSTPORT = 328,
    PROTO = 329,
    AF = 330,
    MYSELF = 331,
    MSGLEN = 332,
    NULLV = 333,
    CACHE_STORE = 334,
    CACHE_FETCH = 335,
    CACHE_COUNTER_FETCH = 336,
    CACHE_REMOVE = 337,
    CACHE_ADD = 338,
    CACHE_SUB = 339,
    CACHE_RAW_QUERY = 340,
    XDBG = 341,
    XLOG = 342,
    XLOG_BUF_SIZE = 343,
    XLOG_FORCE_COLOR = 344,
    RAISE_EVENT = 345,
    SUBSCRIBE_EVENT = 346,
    CONSTRUCT_URI = 347,
    GET_TIMESTAMP = 348,
    SCRIPT_TRACE = 349,
    DEBUG = 350,
    ENABLE_ASSERTS = 351,
    ABORT_ON_ASSERT = 352,
    FORK = 353,
    LOGSTDERROR = 354,
    LOGFACILITY = 355,
    LOGNAME = 356,
    AVP_ALIASES = 357,
    LISTEN = 358,
    BIN_LISTEN = 359,
    BIN_CHILDREN = 360,
    ALIAS = 361,
    AUTO_ALIASES = 362,
    DNS = 363,
    REV_DNS = 364,
    DNS_TRY_IPV6 = 365,
    DNS_TRY_NAPTR = 366,
    DNS_RETR_TIME = 367,
    DNS_RETR_NO = 368,
    DNS_SERVERS_NO = 369,
    DNS_USE_SEARCH = 370,
    MAX_WHILE_LOOPS = 371,
    CHILDREN = 372,
    CHECK_VIA = 373,
    SHM_HASH_SPLIT_PERCENTAGE = 374,
    SHM_SECONDARY_HASH_SIZE = 375,
    MEM_WARMING_ENABLED = 376,
    MEM_WARMING_PATTERN_FILE = 377,
    MEM_WARMING_PERCENTAGE = 378,
    MEMLOG = 379,
    MEMDUMP = 380,
    EXECMSGTHRESHOLD = 381,
    EXECDNSTHRESHOLD = 382,
    TCPTHRESHOLD = 383,
    EVENT_SHM_THRESHOLD = 384,
    EVENT_PKG_THRESHOLD = 385,
    QUERYBUFFERSIZE = 386,
    QUERYFLUSHTIME = 387,
    SIP_WARNING = 388,
    SOCK_MODE = 389,
    SOCK_USER = 390,
    SOCK_GROUP = 391,
    UNIX_SOCK = 392,
    UNIX_SOCK_CHILDREN = 393,
    UNIX_TX_TIMEOUT = 394,
    SERVER_SIGNATURE = 395,
    SERVER_HEADER = 396,
    USER_AGENT_HEADER = 397,
    LOADMODULE = 398,
    MPATH = 399,
    MODPARAM = 400,
    MAXBUFFER = 401,
    USER = 402,
    GROUP = 403,
    CHROOT = 404,
    WDIR = 405,
    MHOMED = 406,
    POLL_METHOD = 407,
    TCP_ACCEPT_ALIASES = 408,
    TCP_CHILDREN = 409,
    TCP_CONNECT_TIMEOUT = 410,
    TCP_CON_LIFETIME = 411,
    TCP_LISTEN_BACKLOG = 412,
    TCP_MAX_CONNECTIONS = 413,
    TCP_NO_NEW_CONN_BFLAG = 414,
    TCP_KEEPALIVE = 415,
    TCP_KEEPCOUNT = 416,
    TCP_KEEPIDLE = 417,
    TCP_KEEPINTERVAL = 418,
    TCP_MAX_MSG_TIME = 419,
    ADVERTISED_ADDRESS = 420,
    ADVERTISED_PORT = 421,
    DISABLE_CORE = 422,
    OPEN_FD_LIMIT = 423,
    MCAST_LOOPBACK = 424,
    MCAST_TTL = 425,
    TOS = 426,
    DISABLE_DNS_FAILOVER = 427,
    DISABLE_DNS_BLACKLIST = 428,
    DST_BLACKLIST = 429,
    DISABLE_STATELESS_FWD = 430,
    DB_VERSION_TABLE = 431,
    DB_DEFAULT_URL = 432,
    DB_MAX_ASYNC_CONNECTIONS = 433,
    DISABLE_503_TRANSLATION = 434,
    SYNC_TOKEN = 435,
    ASYNC_TOKEN = 436,
    EQUAL = 437,
    EQUAL_T = 438,
    GT = 439,
    LT = 440,
    GTE = 441,
    LTE = 442,
    DIFF = 443,
    MATCH = 444,
    NOTMATCH = 445,
    COLONEQ = 446,
    PLUSEQ = 447,
    MINUSEQ = 448,
    SLASHEQ = 449,
    MULTEQ = 450,
    MODULOEQ = 451,
    BANDEQ = 452,
    BOREQ = 453,
    BXOREQ = 454,
    OR = 455,
    AND = 456,
    BOR = 457,
    BAND = 458,
    BXOR = 459,
    BLSHIFT = 460,
    BRSHIFT = 461,
    PLUS = 462,
    MINUS = 463,
    SLASH = 464,
    MULT = 465,
    MODULO = 466,
    NOT = 467,
    BNOT = 468,
    NUMBER = 469,
    ZERO = 470,
    ID = 471,
    STRING = 472,
    SCRIPTVAR = 473,
    IPV6ADDR = 474,
    COMMA = 475,
    SEMICOLON = 476,
    RPAREN = 477,
    LPAREN = 478,
    LBRACE = 479,
    RBRACE = 480,
    LBRACK = 481,
    RBRACK = 482,
    AS = 483,
    USE_CHILDREN = 484,
    DOT = 485,
    CR = 486,
    COLON = 487,
    ANY = 488,
    SCRIPTVARERR = 489
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 189 "cfg.y" /* yacc.c:1909  */

	long intval;
	unsigned long uval;
	char* strval;
	struct expr* expr;
	struct action* action;
	struct net* ipnet;
	struct ip_addr* ipaddr;
	struct socket_id* sockid;
	struct _pv_spec *specval;

#line 301 "cfg.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_CFG_TAB_H_INCLUDED  */
