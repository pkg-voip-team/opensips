Source: opensips
Section: net
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Bjoern Boschman <bjoern@boschman.de>, Alejandro Rios P. <alerios@debian.org>, Tzafrir Cohen <tzafrir@debian.org>
Build-Depends: debhelper (>= 8), libmysqlclient-dev, libexpat1-dev, libxml2-dev, libpq-dev, libradiusclient-ng-dev, flex, bison, zlib1g-dev, unixodbc-dev, libxmlrpc-c3-dev, libperl-dev, libsnmp-dev, dpkg-dev (>= 1.13.19), libdb-dev (>= 4.6.19), xsltproc, libconfuse-dev, libldap2-dev, libcurl4-gnutls-dev, libpcre3-dev, libmemcached-dev
Standards-Version: 3.9.2
Homepage: http://www.opensips.org/
Vcs-Svn: svn://svn.debian.org/pkg-voip/opensips/trunk/
Vcs-Browser: http://svn.debian.org/wsvn/pkg-voip/opensips/

Package: opensips
Architecture: any
# Depend on python since dbtext support needs it.
Depends: ${shlibs:Depends}, ${misc:Depends}, adduser, python
Suggests: opensips-mysql-module, opensips-postgres-module, opensips-unixodbc-module, opensips-jabber-module, opensips-cpl-module, opensips-radius-modules, opensips-presence-modules, opensips-xmlrpc-module, opensips-perl-modules, opensips-snmpstats-module, opensips-xmpp-module, opensips-carrierroute-module, opensips-berkeley-module, opensips-ldap-modules, opensips-regex-module, opensips-identity-module, opensips-b2bua-module, opensips-dbhttp-module, opensips-console
Description: very fast and configurable SIP server
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 C Shell-like scripting language provides full control over the server's
 behaviour. Its modular architecture allows only required functionality to be
 loaded.
 .
 Among others, the following modules are available: Digest Authentication, CPL
 scripts, Instant Messaging, MySQL support, Presence Agent, Radius
 Authentication, Record Routing, SMS Gateway, Jabber/XMPP Gateway, Transaction
 Module, Registrar and User Location, Load Balaning/Dispatching/LCR, 
 XMLRPC Interface.
 .
 This package contains the main OpenSIPS binary along with the principal modules
 and support binaries.

Package: opensips-dbg
Priority: extra
Architecture: any
Section: debug
Depends: opensips (= ${binary:Version}), ${misc:Depends}
Description: very fast and configurable SIP server [debug symbols]
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package contains the debugging symbols for the opensips binaries and
 modules. You only need to install it if you need to debug opensips.

Package: opensips-mysql-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), mysql-client, ${misc:Depends}
Description: MySQL database connectivity module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 opensips is a very fast and flexible SIP (RFC3261)
 server. Written entirely in C, opensips can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the MySQL database driver for opensips.

Package: opensips-postgres-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), postgresql-client, ${misc:Depends} 
Description: PostgreSQL database connectivity module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 opensips is a very fast and flexible SIP (RFC3261)
 server. Written entirely in C, opensips can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the PostgreSQL database driver for opensips.

Package: opensips-jabber-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: Jabber gateway module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the SIP to Jabber translator module for OpenSIPS.

Package: opensips-cpl-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: CPL module (CPL interpreter engine) for opensips
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides a CPL (Call Processing Language) interpreter for
 opensips, turning opensips into a CPL server (storage and interpreter).

Package: opensips-radius-modules
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: RADIUS modules for opensips
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides a set of RADIUS modules for opensips, for
 authentication, group membership and messages URIs checking against a
 RADIUS server.

Package: opensips-unixodbc-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: unixODBC database connectivity module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the unixODBC database driver for opensips.

Package: opensips-presence-modules
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: SIMPLE presence modules for opensips
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides several opensips modules for implementing presence
 server and presence user agent for RICH presence, registrar-based presence,
 external triggered presence and XCAP support.

Package: opensips-xmlrpc-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: XML-RPC support for opensips's Management Interface
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the XMLRPC transport implementation for opensips's
 Management Interface.

Package: opensips-perl-modules
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Replaces: opensips-perl-module
Conflicts: opensips-perl-module
Description: Perl extensions and database driver for opensips
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides an interface for opensips to write Perl extensions and
 the perlvdb database driver for opensips.

Package: opensips-snmpstats-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), snmpd, ${misc:Depends}
Description: SNMP AgentX subagent module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the snmpstats module for OpenSIPS. This module acts
 as an AgentX subagent which connects to a master agent.

Package: opensips-xmpp-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: XMPP gateway module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the SIP to XMPP IM translator module for OpenSIPS.

Package: opensips-carrierroute-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: carrierroute module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the carrierroute module for OpenSIPS, an integrated
 solution for routing, balancing and blacklisting.

Package: opensips-berkeley-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), db4.6-util, ${misc:Depends}
Description: Berkeley database module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the Berkeley database module for OpenSIPS, a
 high-performance embedded DB kernel. All database tables are stored
 in files, no additional server is necessary.

Package: opensips-ldap-modules
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: LDAP modules for opensips
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the ldap and h350 modules for opensips, enabling LDAP
 queries from the opensips config and storage of SIP account data in an LDAP
 directory.

Package: opensips-osp-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), osptoolkit, ${misc:Depends}
Description: Open Settlement Protocol (OSP) module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package contains the Open Settlement Protocol (OSP) module for OpenSIPS,
 providing support for secure multi-lateral peering using the Open Settlement
 Protocol.

Package: opensips-regex-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: PCRE regexp modules for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides a module for matching operations against regular
 expressions using the powerful PCRE library. By default, OpenSIPS support
 sed-like regular expresions; PCRE library brings perl-like regular expresions.

Package: opensips-identity-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: SIP Identity module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides support for SIP Identity (see RFC 4474).

Package: opensips-b2bua-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: B2B User Agent modules for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides modules for B2BUA suppor in OpenSIPS. Both the 
 implementation and controll (XML based scenario description) are included.

Package: opensips-dbhttp-module
Architecture: any
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}), ${misc:Depends}
Description: HTTP database connectivity module for OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides the HTTP-based database driver for OpenSIPS

Package: opensips-console
Architecture: any
# Depend on python since dbtext support needs it.
Depends: ${shlibs:Depends}, opensips (= ${binary:Version}),${misc:Depends}, libfrontier-rpc-perl, libnet-ip-perl, libberkeleydb-perl, libdbi-perl, libterm-readline-gnu-perl, python
Suggests: opensips-mysql-module, opensips-postgres-module, opensips-unixodbc-module, opensips-xmlrpc-module, opensips-berkeley-module, libdbd-mysql-perl, libdbd-pg-perl 
Description: Console utility for provisioning OpenSIPS
 OpenSIPS is a very fast and flexible SIP (RFC3261) server based on
 OpenSER. Written entirely in C, OpenSIPS can handle thousands calls
 per second even on low-budget hardware.
 .
 This package provides an OpenSIPS Console written in Perl for OpenSIPS
 provisioning.
