/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 79 "cfg.y" /* yacc.c:339  */


#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include "route_struct.h"
#include "globals.h"
#include "route.h"
#include "dprint.h"
#include "sr_module.h"
#include "modparam.h"
#include "ip_addr.h"
#include "resolve.h"
#include "socket_info.h"
#include "name_alias.h"
#include "ut.h"
#include "dset.h"
#include "pvar.h"
#include "blacklists.h"
#include "xlog.h"
#include "db/db_insertq.h"
#include "bin_interface.h"
#include "net/trans.h"
#include "config.h"

#ifdef DEBUG_DMALLOC
#include <dmalloc.h>
#endif

/* hack to avoid alloca usage in the generated C file (needed for compiler
 with no built in alloca, like icc*/
#undef _ALLOCA_H


extern int yylex();
static void yyerror(char* s);
static void yyerrorf(char* fmt, ...);
static char* tmp;
static int i_tmp;
static void* cmd_tmp;
static struct socket_id* lst_tmp;
static int rt;  /* Type of route block for find_export */
static str s_tmp;
static str tstr;
static struct ip_addr* ip_tmp;
static pv_spec_t *spec;
static pv_elem_t *pvmodel;
static struct bl_rule *bl_head = 0;
static struct bl_rule *bl_tail = 0;

action_elem_t elems[MAX_ACTION_ELEMS];
static action_elem_t route_elems[MAX_ACTION_ELEMS];
action_elem_t *a_tmp;

static inline void warn(char* s);
static struct socket_id* mk_listen_id(char*, enum sip_protos, int);
static struct socket_id* set_listen_id_adv(struct socket_id *, char *, int);

extern int line;
extern int column;
extern int startcolumn;
extern char *finame;

#define get_cfg_file_name \
	((finame) ? finame : cfg_file ? cfg_file : "default")



#define mk_action_(_res, _type, _no, _elems) \
	do { \
		_res = mk_action(_type, _no, _elems, line, get_cfg_file_name); \
	} while(0)
#define mk_action0(_res, _type, _p1_type, _p2_type, _p1, _p2) \
	do { \
		_res = mk_action(_type, 0, 0, line, get_cfg_file_name); \
	} while(0)
#define mk_action1(_res, _type, _p1_type, _p1) \
	do { \
		elems[0].type = _p1_type; \
		elems[0].u.data = _p1; \
		_res = mk_action(_type, 1, elems, line, get_cfg_file_name); \
	} while(0)
#define	mk_action2(_res, _type, _p1_type, _p2_type, _p1, _p2) \
	do { \
		elems[0].type = _p1_type; \
		elems[0].u.data = _p1; \
		elems[1].type = _p2_type; \
		elems[1].u.data = _p2; \
		_res = mk_action(_type, 2, elems, line, get_cfg_file_name); \
	} while(0)
#define mk_action3(_res, _type, _p1_type, _p2_type, _p3_type, _p1, _p2, _p3) \
	do { \
		elems[0].type = _p1_type; \
		elems[0].u.data = _p1; \
		elems[1].type = _p2_type; \
		elems[1].u.data = _p2; \
		elems[2].type = _p3_type; \
		elems[2].u.data = _p3; \
		_res = mk_action(_type, 3, elems, line, get_cfg_file_name); \
	} while(0)


#line 176 "cfg.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "cfg.tab.h".  */
#ifndef YY_YY_CFG_TAB_H_INCLUDED
# define YY_YY_CFG_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    FORWARD = 258,
    SEND = 259,
    DROP = 260,
    ASSERT = 261,
    EXIT = 262,
    RETURN = 263,
    LOG_TOK = 264,
    ERROR = 265,
    ROUTE = 266,
    ROUTE_FAILURE = 267,
    ROUTE_ONREPLY = 268,
    ROUTE_BRANCH = 269,
    ROUTE_ERROR = 270,
    ROUTE_LOCAL = 271,
    ROUTE_STARTUP = 272,
    ROUTE_TIMER = 273,
    ROUTE_EVENT = 274,
    SET_HOST = 275,
    SET_HOSTPORT = 276,
    PREFIX = 277,
    STRIP = 278,
    STRIP_TAIL = 279,
    APPEND_BRANCH = 280,
    REMOVE_BRANCH = 281,
    PV_PRINTF = 282,
    SET_USER = 283,
    SET_USERPASS = 284,
    SET_PORT = 285,
    SET_URI = 286,
    REVERT_URI = 287,
    SET_DSTURI = 288,
    RESET_DSTURI = 289,
    ISDSTURISET = 290,
    FORCE_RPORT = 291,
    FORCE_LOCAL_RPORT = 292,
    FORCE_TCP_ALIAS = 293,
    IF = 294,
    ELSE = 295,
    SWITCH = 296,
    CASE = 297,
    DEFAULT = 298,
    SBREAK = 299,
    WHILE = 300,
    FOR = 301,
    IN = 302,
    SET_ADV_ADDRESS = 303,
    SET_ADV_PORT = 304,
    FORCE_SEND_SOCKET = 305,
    SERIALIZE_BRANCHES = 306,
    NEXT_BRANCHES = 307,
    USE_BLACKLIST = 308,
    UNUSE_BLACKLIST = 309,
    MAX_LEN = 310,
    SETDEBUG = 311,
    SETFLAG = 312,
    RESETFLAG = 313,
    ISFLAGSET = 314,
    SETBFLAG = 315,
    RESETBFLAG = 316,
    ISBFLAGSET = 317,
    SETSFLAG = 318,
    RESETSFLAG = 319,
    ISSFLAGSET = 320,
    METHOD = 321,
    URI = 322,
    FROM_URI = 323,
    TO_URI = 324,
    SRCIP = 325,
    SRCPORT = 326,
    DSTIP = 327,
    DSTPORT = 328,
    PROTO = 329,
    AF = 330,
    MYSELF = 331,
    MSGLEN = 332,
    NULLV = 333,
    CACHE_STORE = 334,
    CACHE_FETCH = 335,
    CACHE_COUNTER_FETCH = 336,
    CACHE_REMOVE = 337,
    CACHE_ADD = 338,
    CACHE_SUB = 339,
    CACHE_RAW_QUERY = 340,
    XDBG = 341,
    XLOG = 342,
    XLOG_BUF_SIZE = 343,
    XLOG_FORCE_COLOR = 344,
    RAISE_EVENT = 345,
    SUBSCRIBE_EVENT = 346,
    CONSTRUCT_URI = 347,
    GET_TIMESTAMP = 348,
    SCRIPT_TRACE = 349,
    DEBUG = 350,
    ENABLE_ASSERTS = 351,
    ABORT_ON_ASSERT = 352,
    FORK = 353,
    LOGSTDERROR = 354,
    LOGFACILITY = 355,
    LOGNAME = 356,
    AVP_ALIASES = 357,
    LISTEN = 358,
    BIN_LISTEN = 359,
    BIN_CHILDREN = 360,
    ALIAS = 361,
    AUTO_ALIASES = 362,
    DNS = 363,
    REV_DNS = 364,
    DNS_TRY_IPV6 = 365,
    DNS_TRY_NAPTR = 366,
    DNS_RETR_TIME = 367,
    DNS_RETR_NO = 368,
    DNS_SERVERS_NO = 369,
    DNS_USE_SEARCH = 370,
    MAX_WHILE_LOOPS = 371,
    CHILDREN = 372,
    CHECK_VIA = 373,
    SHM_HASH_SPLIT_PERCENTAGE = 374,
    SHM_SECONDARY_HASH_SIZE = 375,
    MEM_WARMING_ENABLED = 376,
    MEM_WARMING_PATTERN_FILE = 377,
    MEM_WARMING_PERCENTAGE = 378,
    MEMLOG = 379,
    MEMDUMP = 380,
    EXECMSGTHRESHOLD = 381,
    EXECDNSTHRESHOLD = 382,
    TCPTHRESHOLD = 383,
    EVENT_SHM_THRESHOLD = 384,
    EVENT_PKG_THRESHOLD = 385,
    QUERYBUFFERSIZE = 386,
    QUERYFLUSHTIME = 387,
    SIP_WARNING = 388,
    SOCK_MODE = 389,
    SOCK_USER = 390,
    SOCK_GROUP = 391,
    UNIX_SOCK = 392,
    UNIX_SOCK_CHILDREN = 393,
    UNIX_TX_TIMEOUT = 394,
    SERVER_SIGNATURE = 395,
    SERVER_HEADER = 396,
    USER_AGENT_HEADER = 397,
    LOADMODULE = 398,
    MPATH = 399,
    MODPARAM = 400,
    MAXBUFFER = 401,
    USER = 402,
    GROUP = 403,
    CHROOT = 404,
    WDIR = 405,
    MHOMED = 406,
    POLL_METHOD = 407,
    TCP_ACCEPT_ALIASES = 408,
    TCP_CHILDREN = 409,
    TCP_CONNECT_TIMEOUT = 410,
    TCP_CON_LIFETIME = 411,
    TCP_LISTEN_BACKLOG = 412,
    TCP_MAX_CONNECTIONS = 413,
    TCP_NO_NEW_CONN_BFLAG = 414,
    TCP_KEEPALIVE = 415,
    TCP_KEEPCOUNT = 416,
    TCP_KEEPIDLE = 417,
    TCP_KEEPINTERVAL = 418,
    TCP_MAX_MSG_TIME = 419,
    ADVERTISED_ADDRESS = 420,
    ADVERTISED_PORT = 421,
    DISABLE_CORE = 422,
    OPEN_FD_LIMIT = 423,
    MCAST_LOOPBACK = 424,
    MCAST_TTL = 425,
    TOS = 426,
    DISABLE_DNS_FAILOVER = 427,
    DISABLE_DNS_BLACKLIST = 428,
    DST_BLACKLIST = 429,
    DISABLE_STATELESS_FWD = 430,
    DB_VERSION_TABLE = 431,
    DB_DEFAULT_URL = 432,
    DB_MAX_ASYNC_CONNECTIONS = 433,
    DISABLE_503_TRANSLATION = 434,
    SYNC_TOKEN = 435,
    ASYNC_TOKEN = 436,
    EQUAL = 437,
    EQUAL_T = 438,
    GT = 439,
    LT = 440,
    GTE = 441,
    LTE = 442,
    DIFF = 443,
    MATCH = 444,
    NOTMATCH = 445,
    COLONEQ = 446,
    PLUSEQ = 447,
    MINUSEQ = 448,
    SLASHEQ = 449,
    MULTEQ = 450,
    MODULOEQ = 451,
    BANDEQ = 452,
    BOREQ = 453,
    BXOREQ = 454,
    OR = 455,
    AND = 456,
    BOR = 457,
    BAND = 458,
    BXOR = 459,
    BLSHIFT = 460,
    BRSHIFT = 461,
    PLUS = 462,
    MINUS = 463,
    SLASH = 464,
    MULT = 465,
    MODULO = 466,
    NOT = 467,
    BNOT = 468,
    NUMBER = 469,
    ZERO = 470,
    ID = 471,
    STRING = 472,
    SCRIPTVAR = 473,
    IPV6ADDR = 474,
    COMMA = 475,
    SEMICOLON = 476,
    RPAREN = 477,
    LPAREN = 478,
    LBRACE = 479,
    RBRACE = 480,
    LBRACK = 481,
    RBRACK = 482,
    AS = 483,
    USE_CHILDREN = 484,
    DOT = 485,
    CR = 486,
    COLON = 487,
    ANY = 488,
    SCRIPTVARERR = 489
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 189 "cfg.y" /* yacc.c:355  */

	long intval;
	unsigned long uval;
	char* strval;
	struct expr* expr;
	struct action* action;
	struct net* ipnet;
	struct ip_addr* ipaddr;
	struct socket_id* sockid;
	struct _pv_spec *specval;

#line 463 "cfg.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_CFG_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 480 "cfg.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  190
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   5777

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  235
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  72
/* YYNRULES -- Number of rules.  */
#define YYNRULES  603
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  1277

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   489

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   476,   476,   479,   480,   481,   484,   485,   486,   486,
     487,   487,   488,   488,   489,   489,   490,   490,   491,   491,
     492,   492,   493,   493,   494,   494,   496,   499,   513,   521,
     535,   544,   545,   548,   549,   550,   554,   555,   556,   562,
     563,   564,   565,   569,   572,   573,   577,   578,   579,   582,
     586,   589,   595,   596,   598,   605,   615,   616,   617,   621,
     624,   625,   626,   627,   628,   629,   630,   631,   632,   633,
     639,   640,   641,   642,   645,   646,   647,   648,   649,   650,
     651,   652,   653,   654,   655,   656,   657,   658,   659,   660,
     661,   662,   663,   664,   665,   666,   667,   668,   669,   670,
     678,   679,   687,   688,   696,   697,   705,   706,   714,   715,
     716,   717,   718,   719,   720,   721,   722,   723,   724,   725,
     735,   736,   746,   747,   748,   749,   750,   751,   752,   753,
     754,   755,   756,   757,   758,   759,   760,   761,   762,   763,
     764,   765,   766,   767,   777,   787,   788,   791,   792,   795,
     796,   799,   800,   803,   804,   807,   808,   811,   812,   821,
     828,   829,   832,   833,   836,   837,   844,   845,   852,   853,
     860,   861,   862,   863,   866,   867,   870,   871,   872,   873,
     874,   875,   882,   885,   904,   907,   908,   909,   914,   916,
     917,   918,   924,   926,   940,   942,   945,   946,   949,   950,
     957,   958,   965,   966,   970,   997,   998,  1006,  1007,  1010,
    1011,  1014,  1015,  1025,  1028,  1029,  1030,  1031,  1032,  1033,
    1036,  1037,  1040,  1043,  1047,  1048,  1054,  1060,  1064,  1065,
    1068,  1102,  1118,  1119,  1122,  1125,  1134,  1139,  1147,  1162,
    1165,  1171,  1174,  1182,  1188,  1191,  1197,  1200,  1208,  1211,
    1219,  1222,  1230,  1233,  1245,  1249,  1269,  1290,  1311,  1315,
    1316,  1317,  1318,  1319,  1320,  1323,  1324,  1327,  1328,  1329,
    1330,  1332,  1333,  1336,  1337,  1340,  1341,  1342,  1345,  1346,
    1347,  1350,  1365,  1370,  1371,  1372,  1374,  1377,  1379,  1381,
    1383,  1385,  1389,  1391,  1393,  1394,  1397,  1400,  1403,  1406,
    1409,  1412,  1415,  1417,  1419,  1420,  1423,  1425,  1426,  1427,
    1429,  1430,  1431,  1433,  1436,  1437,  1439,  1440,  1441,  1443,
    1445,  1446,  1447,  1461,  1463,  1465,  1467,  1481,  1483,  1485,
    1487,  1489,  1491,  1493,  1495,  1500,  1501,  1512,  1513,  1520,
    1521,  1524,  1525,  1538,  1542,  1543,  1544,  1545,  1546,  1547,
    1548,  1549,  1550,  1551,  1555,  1556,  1557,  1558,  1559,  1560,
    1561,  1564,  1567,  1570,  1573,  1576,  1579,  1582,  1585,  1588,
    1591,  1594,  1597,  1610,  1623,  1645,  1646,  1647,  1648,  1649,
    1652,  1653,  1654,  1657,  1658,  1659,  1662,  1663,  1664,  1665,
    1666,  1667,  1668,  1669,  1672,  1680,  1690,  1698,  1715,  1724,
    1725,  1727,  1728,  1731,  1740,  1749,  1757,  1765,  1774,  1783,
    1791,  1802,  1808,  1816,  1821,  1831,  1838,  1845,  1855,  1860,
    1865,  1872,  1877,  1882,  1887,  1892,  1902,  1912,  1924,  1936,
    1948,  1952,  1958,  1964,  1971,  1972,  1975,  1981,  1987,  1988,
    1990,  1993,  1994,  1995,  1996,  1997,  2003,  2009,  2015,  2021,
    2024,  2030,  2031,  2033,  2036,  2037,  2038,  2041,  2043,  2044,
    2046,  2048,  2049,  2051,  2053,  2054,  2056,  2058,  2059,  2061,
    2063,  2064,  2066,  2068,  2069,  2073,  2077,  2080,  2083,  2084,
    2088,  2092,  2096,  2100,  2101,  2105,  2109,  2113,  2117,  2118,
    2124,  2125,  2127,  2134,  2155,  2156,  2158,  2160,  2161,  2164,
    2166,  2167,  2169,  2171,  2172,  2175,  2177,  2178,  2180,  2188,
    2190,  2192,  2194,  2196,  2198,  2221,  2236,  2238,  2239,  2241,
    2243,  2244,  2246,  2248,  2249,  2251,  2253,  2254,  2256,  2258,
    2259,  2261,  2262,  2263,  2265,  2266,  2268,  2270,  2271,  2272,
    2273,  2275,  2276,  2278,  2280,  2284,  2287,  2290,  2293,  2297,
    2299,  2300,  2313,  2318,  2320,  2321,  2325,  2328,  2329,  2333,
    2336,  2337,  2340,  2343,  2344,  2348,  2351,  2352,  2356,  2359,
    2360,  2369,  2381,  2394,  2401,  2410,  2419,  2430,  2441,  2454,
    2467,  2478,  2489,  2502,  2515,  2524,  2531,  2548,  2565,  2567,
    2570,  2572,  2574,  2576,  2578,  2580,  2583,  2585,  2588,  2602,
    2608,  2610,  2618,  2626
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "FORWARD", "SEND", "DROP", "ASSERT",
  "EXIT", "RETURN", "LOG_TOK", "ERROR", "ROUTE", "ROUTE_FAILURE",
  "ROUTE_ONREPLY", "ROUTE_BRANCH", "ROUTE_ERROR", "ROUTE_LOCAL",
  "ROUTE_STARTUP", "ROUTE_TIMER", "ROUTE_EVENT", "SET_HOST",
  "SET_HOSTPORT", "PREFIX", "STRIP", "STRIP_TAIL", "APPEND_BRANCH",
  "REMOVE_BRANCH", "PV_PRINTF", "SET_USER", "SET_USERPASS", "SET_PORT",
  "SET_URI", "REVERT_URI", "SET_DSTURI", "RESET_DSTURI", "ISDSTURISET",
  "FORCE_RPORT", "FORCE_LOCAL_RPORT", "FORCE_TCP_ALIAS", "IF", "ELSE",
  "SWITCH", "CASE", "DEFAULT", "SBREAK", "WHILE", "FOR", "IN",
  "SET_ADV_ADDRESS", "SET_ADV_PORT", "FORCE_SEND_SOCKET",
  "SERIALIZE_BRANCHES", "NEXT_BRANCHES", "USE_BLACKLIST",
  "UNUSE_BLACKLIST", "MAX_LEN", "SETDEBUG", "SETFLAG", "RESETFLAG",
  "ISFLAGSET", "SETBFLAG", "RESETBFLAG", "ISBFLAGSET", "SETSFLAG",
  "RESETSFLAG", "ISSFLAGSET", "METHOD", "URI", "FROM_URI", "TO_URI",
  "SRCIP", "SRCPORT", "DSTIP", "DSTPORT", "PROTO", "AF", "MYSELF",
  "MSGLEN", "NULLV", "CACHE_STORE", "CACHE_FETCH", "CACHE_COUNTER_FETCH",
  "CACHE_REMOVE", "CACHE_ADD", "CACHE_SUB", "CACHE_RAW_QUERY", "XDBG",
  "XLOG", "XLOG_BUF_SIZE", "XLOG_FORCE_COLOR", "RAISE_EVENT",
  "SUBSCRIBE_EVENT", "CONSTRUCT_URI", "GET_TIMESTAMP", "SCRIPT_TRACE",
  "DEBUG", "ENABLE_ASSERTS", "ABORT_ON_ASSERT", "FORK", "LOGSTDERROR",
  "LOGFACILITY", "LOGNAME", "AVP_ALIASES", "LISTEN", "BIN_LISTEN",
  "BIN_CHILDREN", "ALIAS", "AUTO_ALIASES", "DNS", "REV_DNS",
  "DNS_TRY_IPV6", "DNS_TRY_NAPTR", "DNS_RETR_TIME", "DNS_RETR_NO",
  "DNS_SERVERS_NO", "DNS_USE_SEARCH", "MAX_WHILE_LOOPS", "CHILDREN",
  "CHECK_VIA", "SHM_HASH_SPLIT_PERCENTAGE", "SHM_SECONDARY_HASH_SIZE",
  "MEM_WARMING_ENABLED", "MEM_WARMING_PATTERN_FILE",
  "MEM_WARMING_PERCENTAGE", "MEMLOG", "MEMDUMP", "EXECMSGTHRESHOLD",
  "EXECDNSTHRESHOLD", "TCPTHRESHOLD", "EVENT_SHM_THRESHOLD",
  "EVENT_PKG_THRESHOLD", "QUERYBUFFERSIZE", "QUERYFLUSHTIME",
  "SIP_WARNING", "SOCK_MODE", "SOCK_USER", "SOCK_GROUP", "UNIX_SOCK",
  "UNIX_SOCK_CHILDREN", "UNIX_TX_TIMEOUT", "SERVER_SIGNATURE",
  "SERVER_HEADER", "USER_AGENT_HEADER", "LOADMODULE", "MPATH", "MODPARAM",
  "MAXBUFFER", "USER", "GROUP", "CHROOT", "WDIR", "MHOMED", "POLL_METHOD",
  "TCP_ACCEPT_ALIASES", "TCP_CHILDREN", "TCP_CONNECT_TIMEOUT",
  "TCP_CON_LIFETIME", "TCP_LISTEN_BACKLOG", "TCP_MAX_CONNECTIONS",
  "TCP_NO_NEW_CONN_BFLAG", "TCP_KEEPALIVE", "TCP_KEEPCOUNT",
  "TCP_KEEPIDLE", "TCP_KEEPINTERVAL", "TCP_MAX_MSG_TIME",
  "ADVERTISED_ADDRESS", "ADVERTISED_PORT", "DISABLE_CORE", "OPEN_FD_LIMIT",
  "MCAST_LOOPBACK", "MCAST_TTL", "TOS", "DISABLE_DNS_FAILOVER",
  "DISABLE_DNS_BLACKLIST", "DST_BLACKLIST", "DISABLE_STATELESS_FWD",
  "DB_VERSION_TABLE", "DB_DEFAULT_URL", "DB_MAX_ASYNC_CONNECTIONS",
  "DISABLE_503_TRANSLATION", "SYNC_TOKEN", "ASYNC_TOKEN", "EQUAL",
  "EQUAL_T", "GT", "LT", "GTE", "LTE", "DIFF", "MATCH", "NOTMATCH",
  "COLONEQ", "PLUSEQ", "MINUSEQ", "SLASHEQ", "MULTEQ", "MODULOEQ",
  "BANDEQ", "BOREQ", "BXOREQ", "OR", "AND", "BOR", "BAND", "BXOR",
  "BLSHIFT", "BRSHIFT", "PLUS", "MINUS", "SLASH", "MULT", "MODULO", "NOT",
  "BNOT", "NUMBER", "ZERO", "ID", "STRING", "SCRIPTVAR", "IPV6ADDR",
  "COMMA", "SEMICOLON", "RPAREN", "LPAREN", "LBRACE", "RBRACE", "LBRACK",
  "RBRACK", "AS", "USE_CHILDREN", "DOT", "CR", "COLON", "ANY",
  "SCRIPTVARERR", "$accept", "cfg", "statements", "statement", "$@1",
  "$@2", "$@3", "$@4", "$@5", "$@6", "$@7", "$@8", "$@9", "listen_id",
  "proto", "port", "snumber", "phostport", "alias_def", "id_lst",
  "listen_def", "any_proto", "blst_elem", "blst_elem_list", "assign_stm",
  "module_stm", "ip", "ipv4", "ipv6addr", "ipv6", "route_name",
  "route_stm", "failure_route_stm", "onreply_route_stm",
  "branch_route_stm", "error_route_stm", "local_route_stm",
  "startup_route_stm", "timer_route_stm", "event_route_stm", "exp",
  "equalop", "compop", "matchop", "intop", "strop", "uri_type",
  "script_var", "exp_elem", "exp_cond", "ipnet", "host_sep", "host",
  "assignop", "assignexp", "assign_cmd", "exp_stm", "stm", "actions",
  "action", "if_cmd", "while_cmd", "foreach_cmd", "switch_cmd",
  "switch_stm", "case_stms", "case_stm", "default_stm",
  "module_func_param", "route_param", "async_func", "cmd", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489
};
# endif

#define YYPACT_NINF -753

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-753)))

#define YYTABLE_NINF -412

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    5546,  -125,  -117,   -44,    36,   276,   321,   325,   349,   358,
     377,   409,   438,   452,   455,   469,   472,   495,   528,   203,
     483,   501,   541,   543,   585,   532,   551,   554,   563,   587,
     598,   601,   605,   616,   622,   670,   693,   726,   744,   766,
     773,   781,   802,   803,   814,   815,   128,   848,    61,   849,
     869,   894,   901,   911,   940,   946,   949,   957,   958,   998,
    1005,  1013,  1034,  1035,  1036,  1046,  1047,  1053,  1054,  1055,
    1056,  1057,  1058,  1059,  1060,   586,   588,  1061,  1063,  1073,
    1074,  1075,  1076,  -753,   236,  3960,  -753,    80,   235,   238,
     634,   653,   669,   692,   584,   984,  -753,  -753,  -753,   164,
     176,   248,   247,   251,   296,   319,   114,   136,   155,   119,
      39,   322,    13,   323,   346,   374,  -753,   790,  -753,   827,
    -753,   878,  -753,  1045,  -753,  1048,  -753,  1069,   387,   390,
     424,   442,   444,   445,   177,   446,   447,   462,   465,   466,
     468,   470,   475,   476,   477,   482,   484,   179,   205,  -753,
    -753,   211,  -753,   873,   485,   148,   152,   170,   195,   486,
     198,   487,   488,   489,   490,   491,   492,    46,   493,   494,
     497,   503,   504,    47,   505,   507,   508,   510,   512,    89,
    -753,  1071,  -753,  1072,  1044,  1084,   212,   214,   513,   514,
    -753,  -125,  -753,    18,  -753,    15,  -753,    51,  -753,    31,
    -753,    54,  -753,    58,  -753,    62,  -753,    42,  -753,    48,
    -753,  -753,  -753,  -753,  -753,  -753,  1095,  1096,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  1029,   -42,  -753,  -753,
    1081,  -753,  -753,  -753,  1068,  1080,  -753,  -753,  -753,  -753,
     -82,  -753,  -753,  -753,  1082,  1083,  -753,  -753,   861,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  1070,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  1085,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  5406,
     583,  -753,   583,  -753,  5406,   583,  -753,   583,  -753,  5406,
    -753,  5406,  -753,  5406,  -753,   583,  -753,   583,  -753,  -753,
     865,   865,  1099,  1102,  1091,   -35,  -753,   124,  1103,   865,
    -753,  1115,  1114,    65,    66,  1097,  1116,  1126,  1127,    67,
      68,    69,    70,    71,    72,    73,    75,  1128,  1129,  1130,
      76,    77,    78,    79,  1131,    81,  1132,  1134,  1136,  1137,
    1139,  4144,  1141,  4144,  1143,    83,    84,    85,    86,    88,
      91,    92,    93,    94,    95,    99,   102,   103,   104,   106,
     109,   110,  1144,  1145,  1146,  1147,  1148,  1150,  1152,  1153,
    1154,  1155,  1156,  1157,  1158,  1159,  1160,   111,  -753,  -753,
    -753,  1149,  1142,  1243,  -753,  -753,  -753,  -753,  -753,    33,
    -753,  -753,  -753,  1161,  1162,  1475,  1163,  1164,  1578,  1810,
    1913,  1165,   -30,  1166,   -37,  -753,  1167,  -753,  -753,  -753,
    -753,  -753,  -753,  1168,  1172,   -62,  -753,   120,  -753,   216,
    1171,  4144,  1173,   399,  -753,   202,  -753,   218,  -753,   127,
    -753,   219,  -753,   220,  -753,   221,  -753,   515,  -753,   516,
     324,   -91,   676,  -753,   222,  -753,   223,  -753,   224,  -753,
     225,  1174,  -753,   227,  1177,  1179,  1180,  1181,   132,   456,
    -753,  -753,  -753,   567,   607,   575,   795,   859,   870,   498,
     876,  4144,  4144,  4549,  4236,  -753,  4457,   589,   913,  -753,
    -753,  -753,  -753,  -753,  -753,   -88,  4457,   -88,  -753,    53,
    -753,   143,  -753,   167,  -753,   517,  -753,    59,  -753,   232,
    -753,   233,  -753,   826,  -753,     0,  -753,   369,  -753,   381,
    -753,   389,  -753,   441,  -753,   459,  -753,   603,  -753,   604,
    -753,   631,  1169,  1170,  1187,  1188,  1189,  1190,  1191,  1192,
    1193,  1194,  1195,  1196,   -88,  -158,  1178,  -753,   141,  1294,
    1306,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  4236,
    -753,  -753,  -753,  -753,  -753,  -753,  1197,  1198,  -753,  1199,
    1201,  -753,  -753,  -753,  1200,   668,  1202,    17,  1203,   -35,
    1204,    26,   665,  1205,   -75,  -753,    60,  1207,  1208,  -753,
    1209,   -68,  -753,   -65,  -753,  -753,  1210,  1211,  1212,  1213,
    1216,  1215,  1218,  1217,   654,  1219,  1220,  1221,  1222,  1223,
    1224,  1225,  1226,  1227,  1228,   658,  -753,  1229,  1230,  1233,
    1234,  1235,  1236,  1238,  1240,  1241,  1244,  1245,  1247,  -753,
    1248,  1249,  -753,  -753,  -753,  -753,  1250,  1251,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,   201,  -753,   -26,   204,  -753,  -753,  -753,   518,  -753,
     523,   207,  -753,   529,  -753,   244,  -753,   534,  -753,   542,
    -753,    96,  -753,   -83,  -753,  2145,   567,   575,  4236,   805,
    -753,  4236,  -753,   589,   717,  -753,   420,  -753,  4144,  4144,
    4781,  1375,  -753,  -753,  1340,   210,   -25,   331,   527,   568,
    1252,  -753,  1372,  1253,  1265,  1266,  1267,  1268,  1269,  1270,
    1271,  1272,  1293,  -753,  1295,  1296,  1297,  1300,  -753,  1308,
    1319,  1320,  1321,  1322,  1323,  1324,   661,  1325,   700,  1326,
     754,  1327,  1328,  1329,  1330,  1331,  1341,  1342,  1350,  1351,
    1352,  1353,  1354,  1355,  1356,  1358,   755,   797,  1357,  1370,
    1371,  1373,  -753,   113,  1374,  1396,  -753,  -753,   352,  -753,
     798,  -753,  -753,  1004,  5406,  5406,  5406,  5406,  1365,  1368,
    1369,  5406,  -753,  -753,  -753,  1349,  1367,  -753,  -753,  1398,
    1399,   -75,  -753,  -753,  1402,  -753,   -62,  -753,  -753,  -753,
    -753,  1239,  -753,  1408,  -753,  -753,  -753,  -753,  1416,  -753,
    1427,  -753,   -33,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  1428,  -753,  -753,  -753,  1429,  1430,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  1231,  -753,  -753,  -753,
     -82,  -753,  -753,  -753,  -753,  -753,  -753,   -82,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  1344,   234,  1379,   237,  -753,   863,   242,
    4236,  4236,  4236,  4236,  4236,  4236,  4236,  4236,  4236,  4236,
    -753,  -753,  -753,  -753,  2248,  4873,  -753,  -753,  -753,   -82,
    -753,  -753,  -753,  -753,  -753,  -753,  1424,   -88,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,   808,
    -753,  -753,   821,  -753,  -753,   822,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,  -753,  1432,  1433,  1434,  1435,  1436,  1437,
    1438,  -753,  1449,  -753,   -88,  -753,  1450,  1456,   -88,  1457,
    -753,   175,   583,  -753,  -753,  -753,   365,  -753,  2480,  2583,
    2815,  2918,  1451,  1452,  1453,  3150,  1464,  1465,  -753,  -753,
    1460,   448,  -753,  1459,  1461,  1462,  1463,  -753,  -753,  -753,
    -753,   830,  1466,  1467,  1468,    57,  -753,   927,   927,   927,
     927,   927,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  1385,
    1470,  1472,  1473,  1476,  1477,  1479,  1480,  1483,  1484,  1485,
    1486,  1487,  1490,   866,  1489,   895,   896,  1492,  1491,   899,
    1493,  -753,   905,  1494,  -753,  -753,  -753,  -753,  -753,  -753,
    5406,  5406,  5406,  -753,  -753,  -753,   448,  1497,  -753,  -753,
    -753,  -753,   401,  -753,  -753,  -753,  -753,  -753,  1081,  -753,
     906,  1481,   621,  -753,  4873,  -753,  -753,  -753,  -753,  -753,
    -753,  1469,   -88,   -88,  -753,   382,   418,  1501,  -753,  -753,
     -88,  -753,  1500,  -753,  1502,  -753,  1503,  -753,  -753,  -753,
    -753,  3253,  3485,  3588,  1504,   -35,  -753,  -753,  -753,  1455,
    1495,  -753,  1496,  -753,  -753,  -753,   910,  1499,  1507,  1505,
    1506,  1510,  1511,  1512,  1513,  1514,  1517,  1516,  -753,  -753,
    -753,   -35,  1519,  5095,  5187,  5406,   435,  -753,  -753,  -753,
    1508,  1509,  1518,  1526,  -753,  -753,  -753,  1524,  -753,  1522,
    1527,  1525,   908,  1528,  1140,  3820,  1521,  1523,   932,   933,
     936,   937,  1530,  1531,  1529,  -753,  1532,  -753,  1533,  -753,
    -753,   -88,  -753,   -88,  -753,   -88,  -753,   -88,  -753,  1535,
    1534,  -753,  -753,  -753,  1536,  1538,  1539,  1540,  1537,  -753,
    -753,  -753,  -753,  -753,   -88,  1541,  -753
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    26,     0,     0,     4,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     6,     7,   222,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    80,     0,    82,     0,
      84,     0,    86,     0,    88,     0,    90,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   224,
     223,     0,   227,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     209,     0,   211,     0,     0,     0,     0,     0,     0,     0,
       1,     5,     3,     0,     9,     0,    11,     0,    13,     0,
      15,     0,    17,     0,    19,     0,    21,     0,    23,     0,
      25,   179,   177,   180,   178,    60,     0,     0,    33,    59,
      62,    61,    64,    63,    66,    65,    68,    67,    70,    69,
      72,    71,    74,    73,   182,    30,     0,    46,   181,   184,
       0,   341,    28,   231,     0,     0,    27,   228,   232,   229,
      29,   186,   185,   188,   341,     0,    39,    43,    44,   187,
     190,   189,    76,    75,    78,    77,    79,    81,    83,    85,
      87,    89,    92,    91,    96,    95,    98,    97,   100,    99,
     102,   101,   104,   103,   106,   105,   108,   107,   110,   109,
     112,   111,   114,   113,   116,   115,   118,   117,   120,   119,
     122,   121,   124,   123,   126,   125,   128,   127,   172,   171,
     174,   173,   176,   175,   207,   206,     0,    94,    93,   131,
     130,   129,   134,   133,   132,   137,   136,   135,   140,   139,
     138,   142,   141,   145,   143,   144,   147,   146,   149,   148,
     151,   150,   153,   152,   155,   154,   157,   156,   160,   158,
     159,   162,   161,   166,   165,   168,   167,   170,   169,   164,
     163,   192,   191,   194,   193,   196,   195,   198,   197,   200,
     199,   202,   201,   205,   203,   204,   208,   210,     0,   213,
     215,   214,   217,   216,   219,   218,   221,   220,   239,     0,
       0,   241,     0,   244,     0,     0,   246,     0,   248,     0,
     250,     0,   252,     0,   254,     0,   258,     0,    34,    35,
       0,     0,     0,     0,     0,     0,   340,     0,     0,     0,
      45,     0,     0,     0,     0,   442,     0,   444,   448,     0,
       0,     0,     0,     0,     0,     0,     0,   511,     0,     0,
       0,     0,     0,     0,   532,     0,   537,   539,   541,   543,
     546,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   281,   392,
     282,     0,     0,     0,   384,   387,   388,   389,   390,     0,
     235,   234,   236,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    36,    48,    47,     0,   233,    31,    32,
     183,   343,   342,    40,     0,     0,   434,     0,   438,     0,
       0,     0,     0,     0,   451,     0,   490,     0,   494,     0,
     497,     0,   517,     0,   500,     0,   506,     0,   503,     0,
       0,     0,     0,   523,     0,   526,     0,   520,     0,   529,
       0,     0,   534,     0,     0,     0,     0,     0,     0,     0,
     278,   279,   280,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   285,     0,     0,   286,   264,
     283,   377,   284,   376,   375,     0,     0,     0,   550,     0,
     554,     0,   557,     0,   560,     0,   563,     0,   566,     0,
     569,     0,   455,     0,   458,     0,   461,     0,   464,     0,
     478,     0,   483,     0,   488,     0,   467,     0,   470,     0,
     473,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   589,     0,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,     0,
     391,   385,   237,   383,   393,   386,     0,     0,   242,     0,
       0,   247,   249,   251,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    57,     0,     0,     0,   433,
       0,     0,   441,     0,   443,   447,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   510,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   531,
       0,     0,   536,   538,   540,   542,     0,     0,   545,   295,
     265,   267,   268,   269,   270,   266,   271,   272,   275,   276,
     277,     0,   325,   275,     0,   308,   273,   274,     0,   329,
     275,     0,   311,     0,   314,     0,   317,     0,   334,     0,
     321,     0,   261,     0,   379,     0,     0,     0,     0,     0,
     355,     0,   354,     0,   357,   358,     0,   359,     0,     0,
       0,   394,   380,   305,   275,     0,   275,   276,     0,     0,
       0,   396,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   561,     0,     0,     0,     0,   454,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   600,     0,     0,     0,   418,   413,   415,   586,
       0,   373,   374,   372,     0,     0,     0,     0,     0,     0,
       0,     0,    38,    37,    49,    50,     0,    42,    41,     0,
       0,     0,    52,    53,     0,    58,     0,   212,   435,   432,
     439,     0,   436,     0,   445,   446,   452,   449,     0,   491,
       0,   495,     0,   492,   498,   496,   518,   516,   501,   499,
     507,   505,   504,   502,     0,   509,   512,   513,     0,     0,
     524,   522,   527,   525,   521,   519,   530,   528,   535,   533,
     547,   544,   294,   293,   292,   323,   337,   290,   324,   322,
     291,   307,   306,   327,   288,   328,   326,   289,   310,   309,
     313,   312,   316,   315,   333,   331,   332,   330,   320,   319,
     318,   262,   378,   275,     0,   275,     0,   370,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     263,   260,   259,   382,     0,     0,   303,   304,   302,   287,
     300,   301,   299,   298,   297,   296,     0,     0,   549,   548,
     553,   551,   552,   556,   555,   559,   558,   562,   565,   564,
     568,   567,   453,   456,   457,   459,   460,   462,   463,     0,
     476,   477,     0,   481,   482,     0,   486,   487,   465,   466,
     468,   469,   471,   472,     0,     0,     0,     0,     0,     0,
       0,   590,     0,   591,     0,   593,     0,     0,     0,     0,
     431,     0,     0,   588,   419,   416,   417,   587,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   225,   226,
       0,     0,    56,     0,     0,     0,     0,   423,   422,   421,
     424,     0,     0,     0,     0,     0,   371,   366,   365,   367,
     368,   369,   360,   361,   363,   362,   364,   381,   395,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   428,     0,     0,   420,   414,   238,   240,   243,   245,
       0,     0,     0,   255,    51,   230,     0,     0,   437,   440,
     450,   489,     0,   493,   508,   514,   515,   338,   336,   335,
       0,     0,   400,   402,     0,   474,   475,   479,   480,   484,
     485,     0,     0,     0,   573,     0,     0,     0,   585,   592,
       0,   594,     0,   596,     0,   599,     0,   601,   430,   429,
     603,     0,     0,     0,     0,     0,   426,   425,   427,     0,
       0,   398,     0,   401,   399,   397,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   253,   256,
     257,     0,     0,   410,   406,   412,     0,   570,   574,   575,
       0,     0,     0,     0,   584,   595,   597,     0,   602,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   408,     0,   404,     0,   571,
     572,     0,   576,     0,   577,     0,   580,     0,   581,     0,
       0,    54,   407,   403,     0,     0,     0,     0,     0,    55,
     578,   579,   582,   583,     0,     0,   598
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -753,  -753,  -753,  1380,  -753,  -753,  -753,  -753,  -753,  -753,
    -753,  -753,  -753,   -98,  -231,  -653,  -101,  -103,  -753,  1439,
    -753,   801,   847,  -753,  -753,  -753,  -723,  -753,  1520,  -753,
    -382,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,  -753,
    -407,     5,   -13,  -753,   347,  -105,  -418,  -450,  -753,  -415,
    -752,  -753,  -655,  -753,  -571,  -412,  -753,  -592,  -392,  -488,
    -409,  -753,  -753,  -753,  -753,  -753,   595,  -753,   704,  -753,
    -753,  -422
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,   256,   236,   520,   782,   257,   258,   259,
     238,   884,   685,   686,    96,    97,   246,   247,   248,   249,
     503,   194,   196,   198,   200,   202,   204,   206,   208,   210,
     586,   756,   749,   750,   798,   799,   783,   491,   589,   785,
     937,   418,   250,   659,   786,   492,   592,   791,   493,   494,
     495,   496,   497,   498,  1151,  1152,  1153,  1194,   860,  1081,
     854,   499
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     219,   588,   505,   588,   801,   663,   237,   508,   944,   509,
     504,   510,   245,   506,   253,   507,   391,   663,   872,   388,
     663,   663,   663,   511,   873,   512,   875,   877,   878,   594,
     936,   594,   396,   587,   664,   587,   590,   936,   590,   591,
     239,   591,   593,   404,   593,  1077,   596,   348,   361,   406,
     935,   990,   393,   991,   803,   398,   851,    98,  1147,   400,
     812,   885,   152,   402,   852,    99,   526,   528,   534,   536,
     538,   540,   542,   544,   546,   362,   548,   553,   555,   557,
     559,   588,   562,   697,   598,   600,   602,   604,   863,   606,
     373,   193,   608,   610,   612,   614,   616,   958,   792,   940,
     618,   718,   720,   620,   622,   624,   947,   626,   792,   594,
     628,   630,   647,   587,  1050,   228,   590,   788,   789,   591,
     234,   687,   593,   717,   693,   521,   416,   488,   703,   149,
     488,   588,   588,   736,   784,   788,   789,   230,   100,   961,
     989,   235,   855,   490,   805,   800,   490,   802,   417,   319,
     683,   959,   891,   322,   892,   893,   232,   704,   882,   594,
     594,   684,   787,   587,   587,   211,   590,   590,   808,   591,
     591,   325,   593,   593,   772,   773,  1120,   213,   284,   518,
     310,  1078,  -273,  -273,  1079,   488,   411,   412,   240,  -273,
     675,   775,   678,   243,   850,   679,   328,   676,   519,   333,
     244,   490,   932,   698,   116,   938,   312,   967,   945,   784,
     968,   987,   314,   380,   820,   382,   821,   690,   101,   701,
     705,   707,   709,   721,   723,   725,   727,   240,   730,   254,
     242,   518,   243,   814,   816,   938,   190,   787,   945,   244,
     518,   392,   389,   987,   390,   950,   255,   195,   220,   215,
     519,   197,   222,   240,   665,   241,   242,   397,   243,   519,
     349,   240,   350,   241,   242,   244,   243,   240,   405,   241,
     242,  1148,   243,   244,   407,   394,   243,   395,   399,   244,
     886,   813,   401,   244,   153,   887,   403,   663,   527,   529,
     535,   537,   539,   541,   543,   545,   547,   224,   549,   554,
     556,   558,   560,   374,   563,   375,   599,   601,   603,   605,
     960,   607,   513,   514,   609,   611,   613,   615,   617,  1137,
     226,   523,   619,   251,   260,   621,   623,   625,   784,   627,
     229,   784,   629,   631,   648,   235,  1051,   688,   588,   588,
    -339,   500,   689,   501,   502,   150,   737,   262,   936,   995,
     585,   957,   585,   231,   738,   856,   787,   806,   857,   787,
     807,   858,  1149,   859,   320,   321,   594,   594,   323,   324,
     587,   587,   233,   590,   590,   264,   591,   591,   212,   593,
     593,   981,   982,   235,  1184,   117,   326,   327,   272,   856,
     214,   274,   857,  1098,   285,   858,   311,  1121,   984,  1087,
    1088,  1089,  1090,  1091,  1092,  1093,  1094,  1095,  1096,   216,
     217,   329,   330,   936,   334,   335,   218,   933,   934,   699,
     241,   939,   313,   241,   946,   276,   241,   988,   315,   381,
     585,   383,   696,   691,   700,   702,   706,   708,   710,   722,
     724,   726,   728,   278,   731,   280,   282,   286,   288,   815,
     817,   939,  1080,   883,   946,   216,   217,   739,   102,   988,
     235,   221,   218,   290,   751,   223,   292,   294,   754,   296,
     761,   298,  1058,  1059,  1060,  1061,   300,   302,   304,  1065,
     585,   585,   795,   306,   118,   308,   317,   331,   336,   338,
     340,   342,   344,   346,   351,   353,   663,   792,   355,   768,
     809,   804,   120,   103,   357,   359,   363,   104,   365,   367,
     225,   369,   819,   371,   384,   386,   711,   713,   810,   941,
     784,   784,   784,   784,   784,   784,   784,   784,   784,   784,
     948,   105,  1212,   227,   951,   952,   252,   261,  -274,  -274,
     106,   715,   122,   954,   124,  -274,   716,  1100,   787,   787,
     787,   787,   787,   787,   787,   787,   787,   787,  1229,   107,
     263,   757,  1195,   757,   757,   757,  1054,   757,   752,  1055,
     663,   663,   663,   663,   748,   797,   759,   663,   753,  1124,
     760,   880,  1125,   822,   769,   823,   126,   180,   265,   182,
     793,   108,   794,   796,  1115,   824,  1199,   825,  1118,   943,
     488,   273,   207,   826,   275,   827,   216,   217,   755,   570,
     571,   572,   955,   218,   956,  1186,   490,   488,  1187,   488,
     109,   695,   970,   971,   972,   973,   974,   975,   976,   977,
     978,   979,  1201,   490,   110,   490,   488,   111,   277,   740,
     741,   742,   743,   744,   745,   746,   747,   980,   199,  1236,
     883,   112,   490,   488,   113,   828,   279,   829,   281,   283,
     287,   289,   240,  1150,  1192,   119,   792,   243,   201,   490,
    1123,   964,   966,   830,   244,   831,   291,   114,   969,   293,
     295,   740,   297,   121,   299,   203,   745,   585,   585,   301,
     303,   305,  1188,   663,   663,   663,   307,   992,   309,   318,
     332,   337,   339,   341,   343,   345,   347,   352,   354,   205,
     115,   356,  1197,  1198,   128,  1200,  1202,   358,   360,   364,
    1204,   366,   368,   123,   370,   125,   372,   385,   387,   712,
     714,   811,   942,   129,   216,   217,   130,   240,  1181,  1182,
    1183,   218,   243,   949,   663,   131,   663,   663,   953,   244,
     740,   741,   742,   743,   744,   745,   746,   747,   740,   741,
     742,   743,   744,   745,   746,   747,  1237,   127,   181,   132,
     183,   797,   740,   741,   742,   743,   744,   745,   746,   747,
     133,   963,   965,   134,   993,   994,   488,   135,   794,   796,
     740,   741,   742,   743,   744,   745,   762,   500,   136,   501,
     502,  1264,   490,  1265,   137,  1266,   647,  1267,  -356,  -356,
    -356,  -356,  -356,  -356,  -356,  -356,  -356,   832,   834,   833,
     835,  1232,  1234,  1235,  1275,  -356,  -356,  -356,  -356,  -356,
    -356,  -356,  -356,  -356,  -356,  -356,  -356,  -356,  -356,  -356,
    -356,  -356,  -356,  -356,  -356,   836,  -356,   837,   869,   870,
    -356,  -356,   138,  -356,  -356,  -356,  -356,  -356,  -356,  -356,
     764,  -356,  -356,  -356,  -356,  -356,  -356,  -356,  -356,  -356,
    -356,   766,   216,   217,   902,   139,   903,   770,   914,   218,
     915,  1019,   879,  1020,  -356,  -356,  -356,  -356,  -356,  -356,
    -356,  -356,  -356,   719,   488,  -356,  -356,  -356,  -356,  -356,
     740,   741,   742,   743,   744,   745,   746,   747,   140,   661,
     490,   423,   424,   425,   426,   427,   428,   429,   430,   431,
    1022,   758,  1023,   763,   765,   767,   141,   771,   432,   433,
     434,   435,   436,   437,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   449,   450,   451,   142,   452,
    -409,  -409,  1246,   453,   454,   143,   455,   456,   457,   458,
     459,   460,   461,   144,   462,   463,   464,   465,   466,   467,
     468,   469,   470,   471,  1025,  1042,  1026,  1043,   740,   741,
     742,   743,   744,   745,   145,   146,  -356,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   147,   148,   481,   482,
     483,   484,   485,   209,   266,  -356,  -356,  -356,  -356,  -356,
    -356,  -356,  -356,  -356,  -356,  -356,  -356,  1044,  1056,  1045,
    1057,  -356,  1101,  -356,  1102,  -356,  -356,  -356,   648,  -356,
     151,   154,  -356,   216,   217,  1103,  1105,  1104,  1106,  -356,
     218,   267,   740,   741,   742,   743,   744,   745,   818,  1190,
    1142,   155,  1143,   740,   741,   742,   743,   744,   745,   740,
     741,   742,   743,   744,   745,   970,   971,   972,   973,   974,
     975,   976,   977,   978,   979,   240,   156,   254,   242,   240,
     243,   241,   242,   157,   243,  1086,  1167,   244,  1168,   486,
     316,   244,   268,   158,   255,   649,   740,   741,   742,   743,
     744,   745,   746,   747,   650,   651,   652,   653,   654,   655,
     656,   657,   658,   216,   217,  1170,  1172,  1171,  1173,  1176,
     218,  1177,   159,  1189,   487,  1056,   488,  1179,   160,   489,
    1216,   161,  1217,  -409,   975,   976,   977,   978,   979,   162,
     163,   661,   490,   423,   424,   425,   426,   427,   428,   429,
     430,   431,  1251,  1253,  1252,  1254,  1255,  1257,  1256,  1258,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
     164,   452,  -405,  -405,  1248,   453,   454,   165,   455,   456,
     457,   458,   459,   460,   461,   166,   462,   463,   464,   465,
     466,   467,   468,   469,   470,   471,   970,   971,   972,   973,
     974,   975,   976,   977,   978,   979,   167,   168,   169,   472,
     473,   474,   475,   476,   477,   478,   479,   480,   170,   171,
     481,   482,   483,   484,   485,   172,   173,   174,   175,   176,
     177,   178,   179,   184,   661,   185,   423,   424,   425,   426,
     427,   428,   429,   430,   431,   186,   187,   188,   189,   269,
     378,   410,   270,   432,   433,   434,   435,   436,   437,   438,
     439,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     449,   450,   451,   271,   452,   376,   377,   243,   453,   454,
     421,   455,   456,   457,   458,   459,   460,   461,   379,   462,
     463,   464,   465,   466,   467,   468,   469,   470,   471,   408,
     409,   413,   415,   515,   -30,   419,   516,   422,   517,   522,
     530,   486,   472,   473,   474,   475,   476,   477,   478,   479,
     480,   649,   524,   481,   482,   483,   484,   485,   525,   531,
     650,   651,   652,   653,   654,   655,   656,   657,   658,   532,
     533,   550,   551,   552,   561,   564,   487,   565,   488,   566,
     567,   489,   568,   660,   595,  -405,   597,   632,   633,   634,
     635,   636,   861,   637,   490,   638,   639,   640,   641,   642,
     643,   644,   645,   646,   862,   674,   838,   839,   666,   667,
     669,   670,   682,   692,   853,   694,   729,   680,   677,   732,
     681,   733,   734,   735,   840,   841,   842,   843,   844,   845,
     846,   847,   848,   849,   868,   985,   986,   874,   876,   997,
     935,   864,   865,   866,   486,   867,   871,  1150,   881,   888,
     889,   890,   894,   895,   896,   897,   898,   899,   900,   901,
    1085,   904,   905,   906,   907,   908,   909,   910,   911,   912,
     913,   916,   917,   918,   919,   943,  1073,   920,   921,   487,
     922,   488,   923,   924,   489,   192,   925,   926,   662,   927,
     928,   929,   930,   931,   996,   998,   661,   490,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   999,  1000,  1001,
    1002,  1003,  1004,  1005,  1006,   432,   433,   434,   435,   436,
     437,   438,   439,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   449,   450,   451,  1007,   452,  1008,  1009,  1010,
     453,   454,  1011,   455,   456,   457,   458,   459,   460,   461,
    1012,   462,   463,   464,   465,   466,   467,   468,   469,   470,
     471,  1013,  1014,  1015,  1016,  1017,  1018,  1021,  1024,  1027,
    1028,  1029,  1030,  1031,   472,   473,   474,   475,   476,   477,
     478,   479,   480,  1032,  1033,   481,   482,   483,   484,   485,
    1034,  1035,  1036,  1037,  1038,  1039,  1040,  1046,  1066,   661,
    1041,   423,   424,   425,   426,   427,   428,   429,   430,   431,
    1047,  1048,  1062,  1049,  1052,  1063,  1064,  1067,   432,   433,
     434,   435,   436,   437,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   449,   450,   451,  1053,   452,
    1068,  1069,  1071,   453,   454,  1074,   455,   456,   457,   458,
     459,   460,   461,  1075,   462,   463,   464,   465,   466,   467,
     468,   469,   470,   471,  1076,  1082,  1083,  1084,  1099,  1107,
    1108,  1109,  1110,  1111,  1112,  1113,   486,   472,   473,   474,
     475,   476,   477,   478,   479,   480,  1114,  1116,   481,   482,
     483,   484,   485,  1117,  1119,  1130,  1131,  1132,  1134,  1135,
    1136,  1138,  1070,  1139,  1140,  1141,  1196,  1213,  1144,  1145,
    1146,   487,  1154,   488,  1155,  1156,   489,   420,  1157,  1158,
     668,  1159,  1160,  1161,  1162,  1163,  1191,  1165,  1164,   490,
    1166,  1169,  1174,  1175,  1205,  1178,  1180,  1185,  1203,  1206,
    1207,  1218,  1238,  1239,  1211,  1220,  1221,  1214,  1215,  1219,
    1222,  1223,  1240,  1072,  1224,  1225,  1226,  1227,  1228,  1230,
    1241,  1242,  1243,  1249,  1244,  1250,  1245,  1193,  1260,  1247,
    1259,  1261,  1268,  1262,  1263,  1122,  1269,  1274,  1270,   486,
    1271,  1272,  1273,  1276,   414,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   487,     0,   488,     0,     0,   489,
       0,     0,     0,   671,     0,     0,     0,     0,     0,     0,
       0,   661,   490,   423,   424,   425,   426,   427,   428,   429,
     430,   431,     0,     0,     0,     0,     0,     0,     0,     0,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
       0,   452,     0,     0,     0,   453,   454,     0,   455,   456,
     457,   458,   459,   460,   461,     0,   462,   463,   464,   465,
     466,   467,   468,   469,   470,   471,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   472,
     473,   474,   475,   476,   477,   478,   479,   480,     0,     0,
     481,   482,   483,   484,   485,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   661,     0,   423,   424,   425,   426,
     427,   428,   429,   430,   431,     0,     0,     0,     0,     0,
       0,     0,     0,   432,   433,   434,   435,   436,   437,   438,
     439,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     449,   450,   451,     0,   452,     0,     0,     0,   453,   454,
       0,   455,   456,   457,   458,   459,   460,   461,     0,   462,
     463,   464,   465,   466,   467,   468,   469,   470,   471,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   486,   472,   473,   474,   475,   476,   477,   478,   479,
     480,     0,     0,   481,   482,   483,   484,   485,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   487,     0,   488,     0,
       0,   489,     0,     0,     0,   672,     0,     0,     0,     0,
       0,     0,     0,     0,   490,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   486,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   487,
       0,   488,     0,     0,   489,     0,     0,     0,   673,     0,
       0,     0,     0,     0,     0,     0,   661,   490,   423,   424,
     425,   426,   427,   428,   429,   430,   431,     0,     0,     0,
       0,     0,     0,     0,     0,   432,   433,   434,   435,   436,
     437,   438,   439,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   449,   450,   451,     0,   452,     0,     0,     0,
     453,   454,     0,   455,   456,   457,   458,   459,   460,   461,
       0,   462,   463,   464,   465,   466,   467,   468,   469,   470,
     471,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   472,   473,   474,   475,   476,   477,
     478,   479,   480,     0,     0,   481,   482,   483,   484,   485,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   661,
       0,   423,   424,   425,   426,   427,   428,   429,   430,   431,
       0,     0,     0,     0,     0,     0,     0,     0,   432,   433,
     434,   435,   436,   437,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   449,   450,   451,     0,   452,
       0,     0,     0,   453,   454,     0,   455,   456,   457,   458,
     459,   460,   461,     0,   462,   463,   464,   465,   466,   467,
     468,   469,   470,   471,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   486,   472,   473,   474,
     475,   476,   477,   478,   479,   480,     0,     0,   481,   482,
     483,   484,   485,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   487,     0,   488,     0,     0,   489,     0,     0,     0,
     962,     0,     0,     0,     0,     0,     0,     0,     0,   490,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   486,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   487,     0,   488,     0,     0,   489,
       0,     0,     0,  1097,     0,     0,     0,     0,     0,     0,
       0,   661,   490,   423,   424,   425,   426,   427,   428,   429,
     430,   431,     0,     0,     0,     0,     0,     0,     0,     0,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
       0,   452,     0,     0,     0,   453,   454,     0,   455,   456,
     457,   458,   459,   460,   461,     0,   462,   463,   464,   465,
     466,   467,   468,   469,   470,   471,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   472,
     473,   474,   475,   476,   477,   478,   479,   480,     0,     0,
     481,   482,   483,   484,   485,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   661,     0,   423,   424,   425,   426,
     427,   428,   429,   430,   431,     0,     0,     0,     0,     0,
       0,     0,     0,   432,   433,   434,   435,   436,   437,   438,
     439,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     449,   450,   451,     0,   452,     0,     0,     0,   453,   454,
       0,   455,   456,   457,   458,   459,   460,   461,     0,   462,
     463,   464,   465,   466,   467,   468,   469,   470,   471,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   486,   472,   473,   474,   475,   476,   477,   478,   479,
     480,     0,     0,   481,   482,   483,   484,   485,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   487,     0,   488,     0,
       0,   489,     0,     0,     0,  1126,     0,     0,     0,     0,
       0,     0,     0,     0,   490,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   486,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   487,
       0,   488,     0,     0,   489,     0,     0,     0,  1127,     0,
       0,     0,     0,     0,     0,     0,   661,   490,   423,   424,
     425,   426,   427,   428,   429,   430,   431,     0,     0,     0,
       0,     0,     0,     0,     0,   432,   433,   434,   435,   436,
     437,   438,   439,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   449,   450,   451,     0,   452,     0,     0,     0,
     453,   454,     0,   455,   456,   457,   458,   459,   460,   461,
       0,   462,   463,   464,   465,   466,   467,   468,   469,   470,
     471,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   472,   473,   474,   475,   476,   477,
     478,   479,   480,     0,     0,   481,   482,   483,   484,   485,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   661,
       0,   423,   424,   425,   426,   427,   428,   429,   430,   431,
       0,     0,     0,     0,     0,     0,     0,     0,   432,   433,
     434,   435,   436,   437,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   449,   450,   451,     0,   452,
       0,     0,     0,   453,   454,     0,   455,   456,   457,   458,
     459,   460,   461,     0,   462,   463,   464,   465,   466,   467,
     468,   469,   470,   471,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   486,   472,   473,   474,
     475,   476,   477,   478,   479,   480,     0,     0,   481,   482,
     483,   484,   485,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   487,     0,   488,     0,     0,   489,     0,     0,     0,
    1128,     0,     0,     0,     0,     0,     0,     0,     0,   490,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   486,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   487,     0,   488,     0,     0,   489,
       0,     0,     0,  1129,     0,     0,     0,     0,     0,     0,
       0,   661,   490,   423,   424,   425,   426,   427,   428,   429,
     430,   431,     0,     0,     0,     0,     0,     0,     0,     0,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
       0,   452,     0,     0,     0,   453,   454,     0,   455,   456,
     457,   458,   459,   460,   461,     0,   462,   463,   464,   465,
     466,   467,   468,   469,   470,   471,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   472,
     473,   474,   475,   476,   477,   478,   479,   480,     0,     0,
     481,   482,   483,   484,   485,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   661,     0,   423,   424,   425,   426,
     427,   428,   429,   430,   431,     0,     0,     0,     0,     0,
       0,     0,     0,   432,   433,   434,   435,   436,   437,   438,
     439,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     449,   450,   451,     0,   452,     0,     0,     0,   453,   454,
       0,   455,   456,   457,   458,   459,   460,   461,     0,   462,
     463,   464,   465,   466,   467,   468,   469,   470,   471,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   486,   472,   473,   474,   475,   476,   477,   478,   479,
     480,     0,     0,   481,   482,   483,   484,   485,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   487,     0,   488,     0,
       0,   489,     0,     0,     0,  1133,     0,     0,     0,     0,
       0,     0,     0,     0,   490,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   486,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   487,
       0,   488,     0,     0,   489,     0,     0,     0,  1208,     0,
       0,     0,     0,     0,     0,     0,   661,   490,   423,   424,
     425,   426,   427,   428,   429,   430,   431,     0,     0,     0,
       0,     0,     0,     0,     0,   432,   433,   434,   435,   436,
     437,   438,   439,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   449,   450,   451,     0,   452,     0,     0,     0,
     453,   454,     0,   455,   456,   457,   458,   459,   460,   461,
       0,   462,   463,   464,   465,   466,   467,   468,   469,   470,
     471,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   472,   473,   474,   475,   476,   477,
     478,   479,   480,     0,     0,   481,   482,   483,   484,   485,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   661,
       0,   423,   424,   425,   426,   427,   428,   429,   430,   431,
       0,     0,     0,     0,     0,     0,     0,     0,   432,   433,
     434,   435,   436,   437,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   449,   450,   451,     0,   452,
       0,     0,     0,   453,   454,     0,   455,   456,   457,   458,
     459,   460,   461,     0,   462,   463,   464,   465,   466,   467,
     468,   469,   470,   471,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   486,   472,   473,   474,
     475,   476,   477,   478,   479,   480,     0,     0,   481,   482,
     483,   484,   485,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   487,     0,   488,     0,     0,   489,     0,     0,     0,
    1209,     0,     0,     0,     0,     0,     0,     0,     0,   490,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   486,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   487,     0,   488,     0,     0,   489,
       0,     0,     0,  1210,     0,     0,     0,     0,     0,     0,
       0,   661,   490,   423,   424,   425,   426,   427,   428,   429,
     430,   431,     0,     0,     0,     0,     0,     0,     0,     0,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
       0,   452,     0,     0,     0,   453,   454,     0,   455,   456,
     457,   458,   459,   460,   461,     0,   462,   463,   464,   465,
     466,   467,   468,   469,   470,   471,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   472,
     473,   474,   475,   476,   477,   478,   479,   480,     0,     0,
     481,   482,   483,   484,   485,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      -2,   191,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    -8,   -10,   -12,   -14,   -16,   -18,   -20,   -22,   -24,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   486,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   487,     0,   488,     0,
       0,   489,     0,     0,     0,  -411,     0,     0,     2,     3,
       0,     0,     0,     0,   490,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,     0,     0,     0,     0,     0,     0,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
       0,     0,     0,     0,     0,     0,     0,   423,   424,   425,
     426,   427,   428,   429,   430,   431,     0,     0,     0,     0,
       0,     0,     0,     0,   432,   433,   434,   435,   436,   437,
     438,   439,   440,   441,   442,   443,   444,   445,   446,   447,
     448,   449,   450,   451,     0,     0,     0,     0,     0,     0,
       0,    83,   455,   456,   457,   458,   459,   460,   461,     0,
     462,   463,   464,   465,   466,   467,   468,   469,   470,   471,
     569,   570,   571,   572,   573,   574,   575,   576,   577,   578,
     579,   580,     0,   472,   473,   474,   475,   476,   477,   478,
     479,   480,     0,     0,   481,   482,   483,   484,   485,   423,
     424,   425,   426,   427,   428,   429,   430,   431,     0,     0,
       0,     0,     0,     0,     0,     0,   432,   433,   434,   435,
     436,   437,   438,   439,   440,   441,   442,   443,   444,   445,
     446,   447,   448,   449,   450,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   455,   456,   457,   458,   459,   460,
     461,     0,   462,   463,   464,   465,   466,   467,   468,   469,
     470,   471,   569,   570,   571,   572,   776,   574,   777,   576,
     577,   578,   579,   580,     0,   472,   473,   474,   475,   476,
     477,   478,   479,   480,     0,   486,   481,   482,   483,   484,
     485,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   216,   217,     0,     0,     0,   581,     0,   218,     0,
     487,     0,   488,     0,     0,     0,     0,   582,   583,     0,
     584,     0,     0,     0,     0,     0,     0,     0,   490,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   486,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   216,   217,     0,     0,     0,     0,   778,
     218,     0,   779,   780,   488,     0,     0,     0,     0,   781,
     423,   424,   425,   426,   427,   428,   429,   430,   431,     0,
     490,     0,     0,     0,     0,     0,     0,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,     0,   452,     0,
       0,     0,   453,   454,     0,   455,   456,   457,   458,   459,
     460,   461,     0,   462,   463,   464,   465,   466,   467,   468,
     469,   470,   471,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   472,   473,   474,   475,
     476,   477,   478,   479,   480,     0,     0,   481,   482,   483,
     484,   485,   423,   424,   425,   426,   427,   428,   429,   430,
     431,     0,     0,     0,     0,     0,     0,     0,     0,   432,
     433,   434,   435,   436,   437,   438,   439,   440,   441,   442,
     443,   444,   445,   446,   447,   448,   449,   450,   451,     0,
     452,     0,     0,     0,   453,   454,     0,   455,   456,   457,
     458,   459,   460,   461,     0,   462,   463,   464,   465,   466,
     467,   468,   469,   470,   471,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   472,   473,
     474,   475,   476,   477,   478,   479,   480,     0,   486,   481,
     482,   483,   484,   485,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   788,   789,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   487,     0,   488,     0,     0,   489,     0,
       0,   790,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   490,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     486,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   487,     0,   488,     0,     0,
     489,     0,     0,     0,   774,     0,     0,     0,     0,     0,
       0,     0,     0,   490,   423,   424,   425,   426,   427,   428,
     429,   430,   431,     0,     0,     0,     0,     0,     0,     0,
       0,   432,   433,   434,   435,   436,   437,   438,   439,   440,
     441,   442,   443,   444,   445,   446,   447,   448,   449,   450,
     451,     0,   452,     0,     0,     0,   453,   454,     0,   455,
     456,   457,   458,   459,   460,   461,     0,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     472,   473,   474,   475,   476,   477,   478,   479,   480,     0,
       0,   481,   482,   483,   484,   485,   423,   424,   425,   426,
     427,   428,   429,   430,   431,     0,     0,     0,     0,     0,
       0,     0,     0,   432,   433,   434,   435,   436,   437,   438,
     439,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     449,   450,   451,     0,   452,     0,     0,     0,   453,   454,
       0,   455,   456,   457,   458,   459,   460,   461,     0,   462,
     463,   464,   465,   466,   467,   468,   469,   470,   471,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   472,   473,   474,   475,   476,   477,   478,   479,
     480,     0,   486,   481,   482,   483,   484,   485,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   487,     0,   488,
       0,     0,   489,     0,     0,     0,   983,     0,     0,     0,
       0,     0,     0,     0,     0,   490,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   486,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   487,
       0,   488,     0,     0,   489,     0,     0,   790,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   490,     0,     0,
       0,     0,     0,     0,     0,   432,   433,   434,   435,   436,
     437,   438,   439,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   449,   450,   451,     0,   452,     0,     0,  1231,
     453,   454,     0,   455,   456,   457,   458,   459,   460,   461,
       0,   462,   463,   464,   465,   466,   467,   468,   469,   470,
     471,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   472,   473,   474,   475,   476,   477,
     478,   479,   480,     0,     0,   481,   482,   483,   484,   485,
     423,   424,   425,   426,   427,   428,   429,   430,   431,     0,
       0,     0,     0,     0,     0,     0,     0,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,     0,   452,     0,
       0,  1233,   453,   454,     0,   455,   456,   457,   458,   459,
     460,   461,     0,   462,   463,   464,   465,   466,   467,   468,
     469,   470,   471,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   472,   473,   474,   475,
     476,   477,   478,   479,   480,     0,   486,   481,   482,   483,
     484,   485,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   487,     0,   488,     0,     0,   489,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   490,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   486,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   487,     0,   488,     0,     0,   489,   423,
     424,   425,   426,   427,   428,   429,   430,   431,     0,     0,
       0,   490,     0,     0,     0,     0,   432,   433,   434,   435,
     436,   437,   438,   439,   440,   441,   442,   443,   444,   445,
     446,   447,   448,   449,   450,   451,     0,   452,     0,     0,
       0,   453,   454,     0,   455,   456,   457,   458,   459,   460,
     461,     0,   462,   463,   464,   465,   466,   467,   468,   469,
     470,   471,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   472,   473,   474,   475,   476,
     477,   478,   479,   480,     0,     0,   481,   482,   483,   484,
     485,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     1,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    -8,   -10,   -12,
     -14,   -16,   -18,   -20,   -22,   -24,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   486,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   487,     0,   488,     0,     0,   489,     0,     0,
       0,     0,     0,     0,     2,     3,     0,     0,     0,     0,
     490,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
       0,     0,     0,     0,     0,     0,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    83
};

static const yytype_int16 yycheck[] =
{
     101,   451,   394,   453,   596,   493,   109,   399,   760,   401,
     392,   403,   110,   395,     1,   397,     1,   505,     1,     1,
     508,   509,   510,   405,   677,   407,   679,     1,   681,   451,
     753,   453,     1,   451,     1,   453,   451,   760,   453,   451,
       1,   453,   451,     1,   453,    78,   453,     1,     1,     1,
      76,    76,     1,    78,     1,     1,   214,   182,     1,     1,
       1,     1,     1,     1,   222,   182,     1,     1,     1,     1,
       1,     1,     1,     1,     1,   173,     1,     1,     1,     1,
       1,   531,     1,   533,     1,     1,     1,     1,   659,     1,
       1,    11,     1,     1,     1,     1,     1,     1,   586,   754,
       1,   551,   552,     1,     1,     1,   761,     1,   596,   531,
       1,     1,     1,   531,     1,     1,   531,   200,   201,   531,
       1,     1,   531,   214,   531,     1,   208,   218,     1,     1,
     218,   581,   582,     1,   584,   200,   201,     1,   182,   222,
     795,   216,     1,   234,     1,   595,   234,   597,   230,     1,
     212,    55,   220,     1,   222,   220,     1,   539,   233,   581,
     582,   223,   584,   581,   582,     1,   581,   582,     1,   581,
     582,     1,   581,   582,   581,   582,     1,     1,     1,   214,
       1,   214,   207,   208,   217,   218,   228,   229,   214,   214,
     220,   583,   229,   219,   644,   232,     1,   227,   233,     1,
     226,   234,     1,     1,     1,     1,     1,   778,     1,   659,
     781,     1,     1,     1,   214,     1,   216,     1,   182,     1,
       1,     1,     1,     1,     1,     1,     1,   214,     1,   216,
     217,   214,   219,     1,     1,     1,     0,   659,     1,   226,
     214,   226,   224,     1,   226,     1,   233,    12,     1,     1,
     233,    13,     1,   214,   221,   216,   217,   226,   219,   233,
     214,   214,   216,   216,   217,   226,   219,   214,   226,   216,
     217,   214,   219,   226,   226,   224,   219,   226,   224,   226,
     220,   222,   224,   226,   223,   225,   224,   775,   223,   223,
     223,   223,   223,   223,   223,   223,   223,     1,   223,   223,
     223,   223,   223,   214,   223,   216,   223,   223,   223,   223,
     214,   223,   410,   411,   223,   223,   223,   223,   223,  1071,
       1,   419,   223,     1,     1,   223,   223,   223,   778,   223,
     216,   781,   223,   223,   223,   216,   223,   217,   788,   789,
     216,   214,   222,   216,   217,   217,   214,     1,  1071,   799,
     451,   769,   453,   217,   222,   214,   778,   214,   217,   781,
     217,   220,  1085,   222,   216,   217,   788,   789,   216,   217,
     788,   789,   217,   788,   789,     1,   788,   789,   214,   788,
     789,   788,   789,   216,  1136,   182,   216,   217,     1,   214,
     214,     1,   217,   985,   217,   220,   217,   222,   790,   970,
     971,   972,   973,   974,   975,   976,   977,   978,   979,   207,
     208,   216,   217,  1136,   216,   217,   214,   216,   217,   217,
     216,   217,   217,   216,   217,     1,   216,   217,   217,   217,
     531,   217,   533,   217,   535,   217,   217,   217,   217,   217,
     217,   217,   217,     1,   217,     1,     1,     1,     1,   217,
     217,   217,   902,   684,   217,   207,   208,     1,   182,   217,
     216,   214,   214,     1,   569,   214,     1,     1,   573,     1,
     575,     1,   864,   865,   866,   867,     1,     1,     1,   871,
     581,   582,   587,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,   984,   985,     1,     1,
     603,   599,     1,   182,     1,     1,     1,   182,     1,     1,
     214,     1,   613,     1,     1,     1,     1,     1,     1,     1,
     970,   971,   972,   973,   974,   975,   976,   977,   978,   979,
       1,   182,  1185,   214,   765,     1,   214,   214,   207,   208,
     182,   217,     1,     1,     1,   214,   222,   997,   970,   971,
     972,   973,   974,   975,   976,   977,   978,   979,  1211,   182,
     214,   574,  1154,   576,   577,   578,   214,   580,     1,   217,
    1058,  1059,  1060,  1061,   569,   588,     1,  1065,   573,   214,
     575,   682,   217,   214,   579,   216,     1,     1,   214,     1,
       1,   182,   587,   588,  1044,   214,   214,   216,  1048,    76,
     218,   214,    18,   214,   214,   216,   207,   208,     1,    67,
      68,    69,    70,   214,    72,   214,   234,   218,   217,   218,
     182,   222,   202,   203,   204,   205,   206,   207,   208,   209,
     210,   211,   214,   234,   182,   234,   218,   182,   214,   183,
     184,   185,   186,   187,   188,   189,   190,   227,    14,   214,
     881,   182,   234,   218,   182,   214,   214,   216,   214,   214,
     214,   214,   214,    42,    43,   182,  1154,   219,    15,   234,
    1052,   776,   777,   214,   226,   216,   214,   182,   783,   214,
     214,   183,   214,   182,   214,    16,   188,   788,   789,   214,
     214,   214,  1142,  1181,  1182,  1183,   214,   798,   214,   214,
     214,   214,   214,   214,   214,   214,   214,   214,   214,    17,
     182,   214,  1162,  1163,   182,  1165,  1166,   214,   214,   214,
    1170,   214,   214,   182,   214,   182,   214,   214,   214,   214,
     214,   214,   214,   182,   207,   208,   182,   214,  1130,  1131,
    1132,   214,   219,   214,  1232,   182,  1234,  1235,   214,   226,
     183,   184,   185,   186,   187,   188,   189,   190,   183,   184,
     185,   186,   187,   188,   189,   190,  1216,   182,   182,   182,
     182,   784,   183,   184,   185,   186,   187,   188,   189,   190,
     182,   776,   777,   182,   216,   217,   218,   182,   783,   784,
     183,   184,   185,   186,   187,   188,     1,   214,   182,   216,
     217,  1251,   234,  1253,   182,  1255,     1,  1257,     3,     4,
       5,     6,     7,     8,     9,    10,    11,   214,   214,   216,
     216,  1213,  1214,  1215,  1274,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,   214,    41,   216,   180,   181,
      45,    46,   182,    48,    49,    50,    51,    52,    53,    54,
       1,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,     1,   207,   208,   220,   182,   222,     1,   220,   214,
     222,   220,   217,   222,    79,    80,    81,    82,    83,    84,
      85,    86,    87,   217,   218,    90,    91,    92,    93,    94,
     183,   184,   185,   186,   187,   188,   189,   190,   182,     1,
     234,     3,     4,     5,     6,     7,     8,     9,    10,    11,
     220,   574,   222,   576,   577,   578,   182,   580,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,   182,    41,
      42,    43,    44,    45,    46,   182,    48,    49,    50,    51,
      52,    53,    54,   182,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,   220,   220,   222,   222,   183,   184,
     185,   186,   187,   188,   182,   182,   181,    79,    80,    81,
      82,    83,    84,    85,    86,    87,   182,   182,    90,    91,
      92,    93,    94,    19,   214,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   220,   220,   222,
     222,   216,   214,   218,   216,   220,   221,   222,   223,   224,
     182,   182,   227,   207,   208,   214,   214,   216,   216,   234,
     214,   214,   183,   184,   185,   186,   187,   188,   222,  1150,
     220,   182,   222,   183,   184,   185,   186,   187,   188,   183,
     184,   185,   186,   187,   188,   202,   203,   204,   205,   206,
     207,   208,   209,   210,   211,   214,   182,   216,   217,   214,
     219,   216,   217,   182,   219,   222,   220,   226,   222,   181,
     217,   226,   214,   182,   233,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,   199,   207,   208,   220,   220,   222,   222,   220,
     214,   222,   182,   217,   216,   220,   218,   222,   182,   221,
     220,   182,   222,   225,   207,   208,   209,   210,   211,   182,
     182,     1,   234,     3,     4,     5,     6,     7,     8,     9,
      10,    11,   220,   220,   222,   222,   220,   220,   222,   222,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
     182,    41,    42,    43,    44,    45,    46,   182,    48,    49,
      50,    51,    52,    53,    54,   182,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,   202,   203,   204,   205,
     206,   207,   208,   209,   210,   211,   182,   182,   182,    79,
      80,    81,    82,    83,    84,    85,    86,    87,   182,   182,
      90,    91,    92,    93,    94,   182,   182,   182,   182,   182,
     182,   182,   182,   182,     1,   182,     3,     4,     5,     6,
       7,     8,     9,    10,    11,   182,   182,   182,   182,   214,
     216,   232,   214,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,   214,    41,   214,   214,   219,    45,    46,
     220,    48,    49,    50,    51,    52,    53,    54,   214,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,   214,
     214,   230,   232,   214,   232,   232,   214,   232,   227,   216,
     223,   181,    79,    80,    81,    82,    83,    84,    85,    86,
      87,   182,   217,    90,    91,    92,    93,    94,   224,   223,
     191,   192,   193,   194,   195,   196,   197,   198,   199,   223,
     223,   223,   223,   223,   223,   223,   216,   223,   218,   223,
     223,   221,   223,   221,   223,   225,   223,   223,   223,   223,
     223,   223,    78,   223,   234,   223,   223,   223,   223,   223,
     223,   223,   223,   223,    78,   220,   217,   217,   227,   227,
     227,   227,   220,   222,   216,   222,   222,   230,   232,   222,
     232,   222,   222,   222,   217,   217,   217,   217,   217,   217,
     217,   217,   217,   217,   214,    40,    76,   214,   214,    47,
      76,   224,   224,   224,   181,   224,   224,    42,   223,   222,
     222,   222,   222,   222,   222,   222,   220,   222,   220,   222,
     209,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   220,   220,    76,   217,   222,   222,   216,
     222,   218,   222,   222,   221,    85,   222,   222,   225,   222,
     222,   222,   222,   222,   222,   222,     1,   234,     3,     4,
       5,     6,     7,     8,     9,    10,    11,   222,   222,   222,
     222,   222,   222,   222,   222,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,   222,    41,   222,   222,   222,
      45,    46,   222,    48,    49,    50,    51,    52,    53,    54,
     222,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,    79,    80,    81,    82,    83,    84,
      85,    86,    87,   222,   222,    90,    91,    92,    93,    94,
     220,   220,   220,   220,   220,   220,   220,   220,   229,     1,
     222,     3,     4,     5,     6,     7,     8,     9,    10,    11,
     220,   220,   227,   220,   220,   227,   227,   230,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,   222,    41,
     222,   222,   220,    45,    46,   217,    48,    49,    50,    51,
      52,    53,    54,   217,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,   217,   217,   217,   217,   224,   217,
     217,   217,   217,   217,   217,   217,   181,    79,    80,    81,
      82,    83,    84,    85,    86,    87,   217,   217,    90,    91,
      92,    93,    94,   217,   217,   224,   224,   224,   214,   214,
     220,   222,   881,   222,   222,   222,   217,   232,   222,   222,
     222,   216,   222,   218,   222,   222,   221,   258,   222,   222,
     225,   222,   222,   220,   220,   220,   225,   220,   222,   234,
     220,   222,   220,   222,   214,   222,   222,   220,   217,   217,
     217,   222,   214,   214,   220,   220,   220,   232,   232,   222,
     220,   220,   214,   886,   222,   222,   222,   220,   222,   220,
     214,   217,   220,   222,   217,   222,   221,  1152,   217,   221,
     220,   222,   217,   221,   221,  1051,   222,   220,   222,   181,
     222,   222,   222,   222,   244,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   216,    -1,   218,    -1,    -1,   221,
      -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     1,   234,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    41,    -1,    -1,    -1,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    -1,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    -1,    -1,
      90,    91,    92,    93,    94,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    41,    -1,    -1,    -1,    45,    46,
      -1,    48,    49,    50,    51,    52,    53,    54,    -1,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   181,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    -1,    -1,    90,    91,    92,    93,    94,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   216,    -1,   218,    -1,
      -1,   221,    -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   234,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   181,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   216,
      -1,   218,    -1,    -1,   221,    -1,    -1,    -1,   225,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     1,   234,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    41,    -1,    -1,    -1,
      45,    46,    -1,    48,    49,    50,    51,    52,    53,    54,
      -1,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    -1,    -1,    90,    91,    92,    93,    94,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    -1,    41,
      -1,    -1,    -1,    45,    46,    -1,    48,    49,    50,    51,
      52,    53,    54,    -1,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   181,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    -1,    -1,    90,    91,
      92,    93,    94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   216,    -1,   218,    -1,    -1,   221,    -1,    -1,    -1,
     225,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   181,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   216,    -1,   218,    -1,    -1,   221,
      -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     1,   234,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    41,    -1,    -1,    -1,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    -1,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    -1,    -1,
      90,    91,    92,    93,    94,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    41,    -1,    -1,    -1,    45,    46,
      -1,    48,    49,    50,    51,    52,    53,    54,    -1,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   181,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    -1,    -1,    90,    91,    92,    93,    94,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   216,    -1,   218,    -1,
      -1,   221,    -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   234,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   181,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   216,
      -1,   218,    -1,    -1,   221,    -1,    -1,    -1,   225,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     1,   234,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    41,    -1,    -1,    -1,
      45,    46,    -1,    48,    49,    50,    51,    52,    53,    54,
      -1,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    -1,    -1,    90,    91,    92,    93,    94,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    -1,    41,
      -1,    -1,    -1,    45,    46,    -1,    48,    49,    50,    51,
      52,    53,    54,    -1,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   181,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    -1,    -1,    90,    91,
      92,    93,    94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   216,    -1,   218,    -1,    -1,   221,    -1,    -1,    -1,
     225,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   181,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   216,    -1,   218,    -1,    -1,   221,
      -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     1,   234,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    41,    -1,    -1,    -1,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    -1,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    -1,    -1,
      90,    91,    92,    93,    94,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    41,    -1,    -1,    -1,    45,    46,
      -1,    48,    49,    50,    51,    52,    53,    54,    -1,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   181,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    -1,    -1,    90,    91,    92,    93,    94,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   216,    -1,   218,    -1,
      -1,   221,    -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   234,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   181,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   216,
      -1,   218,    -1,    -1,   221,    -1,    -1,    -1,   225,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     1,   234,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    41,    -1,    -1,    -1,
      45,    46,    -1,    48,    49,    50,    51,    52,    53,    54,
      -1,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    -1,    -1,    90,    91,    92,    93,    94,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    -1,    41,
      -1,    -1,    -1,    45,    46,    -1,    48,    49,    50,    51,
      52,    53,    54,    -1,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   181,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    -1,    -1,    90,    91,
      92,    93,    94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   216,    -1,   218,    -1,    -1,   221,    -1,    -1,    -1,
     225,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   181,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   216,    -1,   218,    -1,    -1,   221,
      -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     1,   234,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    41,    -1,    -1,    -1,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    -1,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    -1,    -1,
      90,    91,    92,    93,    94,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       0,     1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   181,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   216,    -1,   218,    -1,
      -1,   221,    -1,    -1,    -1,   225,    -1,    -1,    88,    89,
      -1,    -1,    -1,    -1,   234,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,    -1,    -1,    -1,    -1,    -1,    -1,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   231,    48,    49,    50,    51,    52,    53,    54,    -1,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    -1,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    -1,    -1,    90,    91,    92,    93,    94,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    48,    49,    50,    51,    52,    53,
      54,    -1,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    -1,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    -1,   181,    90,    91,    92,    93,
      94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   207,   208,    -1,    -1,    -1,   212,    -1,   214,    -1,
     216,    -1,   218,    -1,    -1,    -1,    -1,   223,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   181,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   207,   208,    -1,    -1,    -1,    -1,   213,
     214,    -1,   216,   217,   218,    -1,    -1,    -1,    -1,   223,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    -1,
     234,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    -1,    41,    -1,
      -1,    -1,    45,    46,    -1,    48,    49,    50,    51,    52,
      53,    54,    -1,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    -1,    -1,    90,    91,    92,
      93,    94,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    -1,
      41,    -1,    -1,    -1,    45,    46,    -1,    48,    49,    50,
      51,    52,    53,    54,    -1,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    -1,   181,    90,
      91,    92,    93,    94,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   200,   201,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   216,    -1,   218,    -1,    -1,   221,    -1,
      -1,   224,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   234,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     181,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   216,    -1,   218,    -1,    -1,
     221,    -1,    -1,    -1,   225,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   234,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    -1,    41,    -1,    -1,    -1,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    -1,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    -1,
      -1,    90,    91,    92,    93,    94,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    41,    -1,    -1,    -1,    45,    46,
      -1,    48,    49,    50,    51,    52,    53,    54,    -1,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    -1,   181,    90,    91,    92,    93,    94,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   216,    -1,   218,
      -1,    -1,   221,    -1,    -1,    -1,   225,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   234,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   181,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   216,
      -1,   218,    -1,    -1,   221,    -1,    -1,   224,     3,     4,
       5,     6,     7,     8,     9,    10,    11,   234,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    41,    -1,    -1,    44,
      45,    46,    -1,    48,    49,    50,    51,    52,    53,    54,
      -1,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    -1,    -1,    90,    91,    92,    93,    94,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    -1,    41,    -1,
      -1,    44,    45,    46,    -1,    48,    49,    50,    51,    52,
      53,    54,    -1,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    -1,   181,    90,    91,    92,
      93,    94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   216,    -1,   218,    -1,    -1,   221,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   181,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   216,    -1,   218,    -1,    -1,   221,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    -1,    -1,
      -1,   234,    -1,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    -1,    41,    -1,    -1,
      -1,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    -1,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    -1,    -1,    90,    91,    92,    93,
      94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   181,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   216,    -1,   218,    -1,    -1,   221,    -1,    -1,
      -1,    -1,    -1,    -1,    88,    89,    -1,    -1,    -1,    -1,
     234,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
      -1,    -1,    -1,    -1,    -1,    -1,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   231
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     1,    88,    89,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   231,   236,   237,   238,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   259,   260,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,     1,   182,     1,   182,
       1,   182,     1,   182,     1,   182,     1,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,     1,
     217,   182,     1,   223,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
       1,   182,     1,   182,   182,   182,   182,   182,   182,   182,
       0,     1,   238,    11,   266,    12,   267,    13,   268,    14,
     269,    15,   270,    16,   271,    17,   272,    18,   273,    19,
     274,     1,   214,     1,   214,     1,   207,   208,   214,   251,
       1,   214,     1,   214,     1,   214,     1,   214,     1,   216,
       1,   217,     1,   217,     1,   216,   249,   252,   255,     1,
     214,   216,   217,   219,   226,   248,   261,   262,   263,   264,
     287,     1,   214,     1,   216,   233,   248,   252,   253,   254,
       1,   214,     1,   214,     1,   214,   214,   214,   214,   214,
     214,   214,     1,   214,     1,   214,     1,   214,     1,   214,
       1,   214,     1,   214,     1,   217,     1,   214,     1,   214,
       1,   214,     1,   214,     1,   214,     1,   214,     1,   214,
       1,   214,     1,   214,     1,   214,     1,   214,     1,   214,
       1,   217,     1,   217,     1,   217,   217,     1,   214,     1,
     216,   217,     1,   216,   217,     1,   216,   217,     1,   216,
     217,     1,   214,     1,   216,   217,     1,   214,     1,   214,
       1,   214,     1,   214,     1,   214,     1,   214,     1,   214,
     216,     1,   214,     1,   214,     1,   214,     1,   214,     1,
     214,     1,   248,     1,   214,     1,   214,     1,   214,     1,
     214,     1,   214,     1,   214,   216,   214,   214,   216,   214,
       1,   217,     1,   217,     1,   214,     1,   214,     1,   224,
     226,     1,   226,     1,   224,   226,     1,   226,     1,   224,
       1,   224,     1,   224,     1,   226,     1,   226,   214,   214,
     232,   228,   229,   230,   263,   232,   208,   230,   286,   232,
     254,   220,   232,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    41,    45,    46,    48,    49,    50,    51,    52,
      53,    54,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    90,    91,    92,    93,    94,   181,   216,   218,   221,
     234,   282,   290,   293,   294,   295,   296,   297,   298,   306,
     214,   216,   217,   265,   265,   293,   265,   265,   293,   293,
     293,   265,   265,   248,   248,   214,   214,   227,   214,   233,
     250,     1,   216,   248,   217,   224,     1,   223,     1,   223,
     223,   223,   223,   223,     1,   223,     1,   223,     1,   223,
       1,   223,     1,   223,     1,   223,     1,   223,     1,   223,
     223,   223,   223,     1,   223,     1,   223,     1,   223,     1,
     223,   223,     1,   223,   223,   223,   223,   223,   223,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,   212,   223,   224,   226,   251,   275,   281,   282,   283,
     284,   290,   291,   295,   306,   223,   275,   223,     1,   223,
       1,   223,     1,   223,     1,   223,     1,   223,     1,   223,
       1,   223,     1,   223,     1,   223,     1,   223,     1,   223,
       1,   223,     1,   223,     1,   223,     1,   223,     1,   223,
       1,   223,   223,   223,   223,   223,   223,   223,   223,   223,
     223,   223,   223,   223,   223,   223,   223,     1,   223,   182,
     191,   192,   193,   194,   195,   196,   197,   198,   199,   288,
     221,     1,   225,   294,     1,   221,   227,   227,   225,   227,
     227,   225,   225,   225,   220,   220,   227,   232,   229,   232,
     230,   232,   220,   212,   223,   257,   258,     1,   217,   222,
       1,   217,   222,   275,   222,   222,   251,   282,     1,   217,
     251,     1,   217,     1,   265,     1,   217,     1,   217,     1,
     217,     1,   214,     1,   214,   217,   222,   214,   282,   217,
     282,     1,   217,     1,   217,     1,   217,     1,   217,   222,
       1,   217,   222,   222,   222,   222,     1,   214,   222,     1,
     183,   184,   185,   186,   187,   188,   189,   190,   276,   277,
     278,   280,     1,   276,   280,     1,   276,   277,   279,     1,
     276,   280,     1,   279,     1,   279,     1,   279,     1,   276,
       1,   279,   275,   275,   225,   293,    70,    72,   213,   216,
     217,   223,   251,   281,   282,   284,   289,   306,   200,   201,
     224,   292,   294,     1,   276,   280,   276,   277,   279,   280,
     282,   292,   282,     1,   248,     1,   214,   217,     1,   252,
       1,   214,     1,   222,     1,   217,     1,   217,   222,   251,
     214,   216,   214,   216,   214,   216,   214,   216,   214,   216,
     214,   216,   214,   216,   214,   216,   214,   216,   217,   217,
     217,   217,   217,   217,   217,   217,   217,   217,   217,   217,
     282,   214,   222,   216,   305,     1,   214,   217,   220,   222,
     303,    78,    78,   289,   224,   224,   224,   224,   214,   180,
     181,   224,     1,   250,   214,   250,   214,     1,   250,   217,
     251,   223,   233,   249,   256,     1,   220,   225,   222,   222,
     222,   220,   222,   220,   222,   222,   222,   222,   220,   222,
     220,   222,   220,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   220,   222,   222,   222,   220,   220,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,     1,   216,   217,    76,   261,   285,     1,   217,
     287,     1,   214,    76,   285,     1,   217,   287,     1,   214,
       1,   249,     1,   214,     1,    70,    72,   281,     1,    55,
     214,   222,   225,   276,   280,   276,   280,   289,   289,   280,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     227,   275,   275,   225,   293,    40,    76,     1,   217,   287,
      76,    78,   251,   216,   217,   282,   222,    47,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   220,
     222,   222,   220,   222,   222,   220,   222,   222,   222,   222,
     222,   222,   222,   222,   220,   220,   220,   220,   220,   220,
     220,   222,   220,   222,   220,   222,   220,   220,   220,   220,
       1,   223,   220,   222,   214,   217,   220,   222,   293,   293,
     293,   293,   227,   227,   227,   293,   229,   230,   222,   222,
     256,   220,   257,   217,   217,   217,   217,    78,   214,   217,
     282,   304,   217,   217,   217,   209,   222,   289,   289,   289,
     289,   289,   289,   289,   289,   289,   289,   225,   292,   224,
     282,   214,   216,   214,   216,   214,   216,   217,   217,   217,
     217,   217,   217,   217,   217,   282,   217,   217,   282,   217,
       1,   222,   303,   265,   214,   217,   225,   225,   225,   225,
     224,   224,   224,   225,   214,   214,   220,   285,   222,   222,
     222,   222,   220,   222,   222,   222,   222,     1,   214,   261,
      42,   299,   300,   301,   222,   222,   222,   222,   222,   222,
     222,   220,   220,   220,   222,   220,   220,   220,   222,   222,
     220,   222,   220,   222,   220,   222,   220,   222,   222,   222,
     222,   293,   293,   293,   285,   220,   214,   217,   282,   217,
     251,   225,    43,   301,   302,   292,   217,   282,   282,   214,
     282,   214,   282,   217,   282,   214,   217,   217,   225,   225,
     225,   220,   250,   232,   232,   232,   220,   222,   222,   222,
     220,   220,   220,   220,   222,   222,   222,   220,   222,   250,
     220,    44,   293,    44,   293,   293,   214,   282,   214,   214,
     214,   214,   217,   220,   217,   221,    44,   221,    44,   222,
     222,   220,   222,   220,   222,   220,   222,   220,   222,   220,
     217,   222,   221,   221,   282,   282,   282,   282,   217,   222,
     222,   222,   222,   222,   220,   282,   222
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   235,   236,   237,   237,   237,   238,   238,   239,   238,
     240,   238,   241,   238,   242,   238,   243,   238,   244,   238,
     245,   238,   246,   238,   247,   238,   238,   248,   248,   248,
     249,   250,   250,   251,   251,   251,   252,   252,   252,   253,
     253,   253,   253,   253,   254,   254,   255,   255,   255,   255,
     255,   255,   256,   256,   257,   257,   258,   258,   258,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   260,   260,   260,   260,   260,   261,   261,
     262,   263,   264,   264,   265,   265,   265,   266,   266,   266,
     267,   267,   268,   268,   268,   269,   269,   270,   270,   271,
     271,   272,   272,   273,   273,   274,   274,   274,   274,   275,
     275,   275,   275,   275,   275,   276,   276,   277,   277,   277,
     277,   278,   278,   279,   279,   280,   280,   280,   281,   281,
     281,   282,   282,   283,   283,   283,   283,   283,   283,   283,
     283,   283,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   285,   285,   285,   285,   286,
     286,   287,   287,   287,   288,   288,   288,   288,   288,   288,
     288,   288,   288,   288,   289,   289,   289,   289,   289,   289,
     289,   289,   289,   289,   289,   289,   289,   289,   289,   289,
     289,   289,   290,   290,   290,   291,   291,   291,   291,   291,
     292,   292,   292,   293,   293,   293,   294,   294,   294,   294,
     294,   294,   294,   294,   295,   295,   296,   297,   298,   299,
     299,   300,   300,   301,   301,   301,   301,   301,   301,   301,
     301,   302,   302,   303,   303,   303,   303,   303,   303,   303,
     303,   304,   304,   304,   304,   304,   304,   304,   305,   305,
     305,   305,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306,   306,   306,   306,   306,   306,   306,
     306,   306,   306,   306
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     1,     2,     1,     1,     0,     2,
       0,     2,     0,     2,     0,     2,     0,     2,     0,     2,
       0,     2,     0,     2,     0,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     3,     5,     5,     1,
       3,     5,     5,     1,     1,     2,     1,     3,     3,     5,
       5,     7,     1,     1,     9,    10,     3,     1,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     3,     2,     3,     2,     3,     2,     3,     2,     3,
       2,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     5,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     2,
       3,     2,     7,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     2,     2,     8,     8,     2,     1,     1,
       7,     1,     1,     3,     1,     1,     1,     4,     7,     2,
       7,     2,     4,     7,     2,     7,     2,     4,     2,     4,
       2,     4,     2,     9,     2,     7,     9,     9,     2,     3,
       3,     2,     3,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     3,     3,     2,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     2,     3,     3,     2,     3,
       3,     2,     3,     3,     2,     3,     3,     2,     3,     3,
       3,     2,     3,     3,     3,     2,     3,     3,     3,     2,
       3,     3,     3,     3,     2,     3,     3,     1,     3,     1,
       1,     1,     3,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     3,     3,     3,     3,     1,     1,     1,     3,     2,
       1,     3,     2,     2,     1,     2,     2,     1,     1,     1,
       1,     2,     1,     2,     3,     5,     3,     7,     7,     2,
       1,     2,     1,     6,     5,     4,     3,     6,     5,     4,
       3,     3,     2,     1,     3,     1,     2,     2,     1,     2,
       3,     1,     1,     1,     1,     3,     3,     3,     3,     4,
       4,     2,     4,     3,     2,     4,     4,     6,     2,     4,
       6,     3,     1,     3,     1,     4,     4,     3,     1,     4,
       6,     2,     4,     4,     3,     2,     4,     4,     2,     4,
       4,     2,     4,     4,     2,     4,     4,     2,     4,     4,
       2,     4,     4,     2,     6,     6,     4,     4,     2,     6,
       6,     4,     4,     2,     6,     6,     4,     4,     2,     6,
       2,     4,     4,     6,     2,     4,     4,     2,     4,     4,
       2,     4,     4,     2,     4,     4,     2,     4,     6,     4,
       3,     1,     4,     4,     6,     6,     4,     2,     4,     4,
       2,     4,     4,     2,     4,     4,     2,     4,     4,     2,
       4,     3,     1,     4,     2,     4,     3,     1,     3,     1,
       3,     1,     3,     1,     4,     3,     1,     4,     4,     4,
       2,     4,     4,     4,     2,     4,     4,     2,     4,     4,
       2,     3,     4,     2,     4,     4,     2,     4,     4,     2,
       8,    10,    10,     6,     8,     8,    10,    10,    12,    12,
      10,    10,    12,    12,     8,     6,     3,     4,     4,     2,
       4,     4,     6,     4,     6,     8,     6,     8,    14,     6,
       3,     6,     8,     6
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 479 "cfg.y" /* yacc.c:1646  */
    {}
#line 3381 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 480 "cfg.y" /* yacc.c:1646  */
    {}
#line 3387 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 481 "cfg.y" /* yacc.c:1646  */
    { yyerror(""); YYABORT;}
#line 3393 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 486 "cfg.y" /* yacc.c:1646  */
    {rt=REQUEST_ROUTE;}
#line 3399 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 487 "cfg.y" /* yacc.c:1646  */
    {rt=FAILURE_ROUTE;}
#line 3405 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 488 "cfg.y" /* yacc.c:1646  */
    {rt=ONREPLY_ROUTE;}
#line 3411 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 489 "cfg.y" /* yacc.c:1646  */
    {rt=BRANCH_ROUTE;}
#line 3417 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 490 "cfg.y" /* yacc.c:1646  */
    {rt=ERROR_ROUTE;}
#line 3423 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 491 "cfg.y" /* yacc.c:1646  */
    {rt=LOCAL_ROUTE;}
#line 3429 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 492 "cfg.y" /* yacc.c:1646  */
    {rt=STARTUP_ROUTE;}
#line 3435 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 493 "cfg.y" /* yacc.c:1646  */
    {rt=TIMER_ROUTE;}
#line 3441 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 494 "cfg.y" /* yacc.c:1646  */
    {rt=EVENT_ROUTE;}
#line 3447 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 499 "cfg.y" /* yacc.c:1646  */
    {	tmp=ip_addr2a((yyvsp[0].ipaddr));
							if(tmp==0){
								LM_CRIT("cfg. parser: bad ip address.\n");
								(yyval.strval)=0;
							}else{
								(yyval.strval)=pkg_malloc(strlen(tmp)+1);
								if ((yyval.strval)==0){
									LM_CRIT("cfg. parser: out of memory.\n");
									YYABORT;
								}else{
									strncpy((yyval.strval), tmp, strlen(tmp)+1);
								}
							}
						}
#line 3466 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 513 "cfg.y" /* yacc.c:1646  */
    {	(yyval.strval)=pkg_malloc(strlen((yyvsp[0].strval))+1);
							if ((yyval.strval)==0){
									LM_CRIT("cfg. parser: out of memory.\n");
									YYABORT;
							}else{
									strncpy((yyval.strval), (yyvsp[0].strval), strlen((yyvsp[0].strval))+1);
							}
						}
#line 3479 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 521 "cfg.y" /* yacc.c:1646  */
    {	if ((yyvsp[0].strval)==0) {
								(yyval.strval) = 0;
							} else {
								(yyval.strval)=pkg_malloc(strlen((yyvsp[0].strval))+1);
								if ((yyval.strval)==0){
									LM_CRIT("cfg. parser: out of memory.\n");
									YYABORT;
								}else{
									strncpy((yyval.strval), (yyvsp[0].strval), strlen((yyvsp[0].strval))+1);
								}
							}
						}
#line 3496 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 535 "cfg.y" /* yacc.c:1646  */
    {
		if (parse_proto((unsigned char *)(yyvsp[0].strval), strlen((yyvsp[0].strval)), &i_tmp) < 0) {
			yyerrorf("cannot handle protocol <%s>\n", (yyvsp[0].strval));
			YYABORT;
		}
		(yyval.intval) = i_tmp;
	 }
#line 3508 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 544 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=(yyvsp[0].intval); }
#line 3514 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 545 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=0; }
#line 3520 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 548 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=(yyvsp[0].intval); }
#line 3526 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 549 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=(yyvsp[0].intval); }
#line 3532 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 550 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=-(yyvsp[0].intval); }
#line 3538 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 554 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=mk_listen_id((yyvsp[0].strval), (yyvsp[-2].intval), 0); }
#line 3544 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 555 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=mk_listen_id((yyvsp[-2].strval), (yyvsp[-4].intval), (yyvsp[0].intval));}
#line 3550 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 556 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.sockid)=0;
				yyerror(" port number expected");
				}
#line 3559 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 562 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=mk_listen_id((yyvsp[0].strval), PROTO_NONE, 0); }
#line 3565 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 563 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=mk_listen_id((yyvsp[0].strval), PROTO_NONE, 0); }
#line 3571 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 564 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=mk_listen_id((yyvsp[-2].strval), PROTO_NONE, (yyvsp[0].intval)); }
#line 3577 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 565 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.sockid)=0;
				yyerror(" port number expected");
				}
#line 3586 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 572 "cfg.y" /* yacc.c:1646  */
    {  (yyval.sockid)=(yyvsp[0].sockid) ; }
#line 3592 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 573 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=(yyvsp[-1].sockid); (yyval.sockid)->next=(yyvsp[0].sockid); }
#line 3598 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 577 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=(yyvsp[0].sockid); }
#line 3604 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 578 "cfg.y" /* yacc.c:1646  */
    { (yyval.sockid)=(yyvsp[-2].sockid); (yyval.sockid)->children=(yyvsp[0].intval); }
#line 3610 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 579 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.sockid)=(yyvsp[-2].sockid); set_listen_id_adv((struct socket_id *)(yyvsp[-2].sockid), (yyvsp[0].strval), 5060);
				}
#line 3618 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 582 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.sockid)=(yyvsp[-4].sockid); set_listen_id_adv((struct socket_id *)(yyvsp[-4].sockid), (yyvsp[-2].strval), 5060);
				(yyvsp[-4].sockid)->children=(yyvsp[0].intval);
				}
#line 3627 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 586 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.sockid)=(yyvsp[-4].sockid); set_listen_id_adv((struct socket_id *)(yyvsp[-4].sockid), (yyvsp[-2].strval), (yyvsp[0].intval));
				}
#line 3635 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 589 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.sockid)=(yyvsp[-6].sockid); set_listen_id_adv((struct socket_id *)(yyvsp[-6].sockid), (yyvsp[-4].strval), (yyvsp[-2].intval));
				(yyvsp[-6].sockid)->children=(yyvsp[0].intval);
				}
#line 3644 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 595 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=PROTO_NONE; }
#line 3650 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 596 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval)=(yyvsp[0].intval); }
#line 3656 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 598 "cfg.y" /* yacc.c:1646  */
    {
				s_tmp.s=(yyvsp[-1].strval);
				s_tmp.len=strlen((yyvsp[-1].strval));
				if (add_rule_to_list(&bl_head,&bl_tail,(yyvsp[-5].ipnet),&s_tmp,(yyvsp[-3].intval),(yyvsp[-7].intval),0)) {
					yyerror("failed to add backlist element\n");YYABORT;
				}
			}
#line 3668 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 605 "cfg.y" /* yacc.c:1646  */
    {
				s_tmp.s=(yyvsp[-1].strval);
				s_tmp.len=strlen((yyvsp[-1].strval));
				if (add_rule_to_list(&bl_head,&bl_tail,(yyvsp[-5].ipnet),&s_tmp,
				(yyvsp[-3].intval),(yyvsp[-7].intval),BLR_APPLY_CONTRARY)) {
					yyerror("failed to add backlist element\n");YYABORT;
				}
			}
#line 3681 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 615 "cfg.y" /* yacc.c:1646  */
    {}
#line 3687 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 616 "cfg.y" /* yacc.c:1646  */
    {}
#line 3693 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 617 "cfg.y" /* yacc.c:1646  */
    { yyerror("bad black list element");}
#line 3699 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 621 "cfg.y" /* yacc.c:1646  */
    {
					*debug=(yyvsp[0].intval);
			}
#line 3707 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 624 "cfg.y" /* yacc.c:1646  */
    { yyerror("number  expected"); }
#line 3713 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 625 "cfg.y" /* yacc.c:1646  */
    { enable_asserts=(yyvsp[0].intval); }
#line 3719 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 626 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3725 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 627 "cfg.y" /* yacc.c:1646  */
    { abort_on_assert=(yyvsp[0].intval); }
#line 3731 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 628 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3737 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 629 "cfg.y" /* yacc.c:1646  */
    { dont_fork= !dont_fork ? ! (yyvsp[0].intval):1; }
#line 3743 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 630 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3749 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 631 "cfg.y" /* yacc.c:1646  */
    { if (!config_check) log_stderr=(yyvsp[0].intval); }
#line 3755 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 632 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3761 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 633 "cfg.y" /* yacc.c:1646  */
    {
					if ( (i_tmp=str2facility((yyvsp[0].strval)))==-1)
						yyerror("bad facility (see syslog(3) man page)");
					if (!config_check)
						log_facility=i_tmp;
									}
#line 3772 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 639 "cfg.y" /* yacc.c:1646  */
    { yyerror("ID expected"); }
#line 3778 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 640 "cfg.y" /* yacc.c:1646  */
    { log_name=(yyvsp[0].strval); }
#line 3784 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 641 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 3790 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 642 "cfg.y" /* yacc.c:1646  */
    {
				yyerror("AVP_ALIASES shouldn't be used anymore\n");
			}
#line 3798 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 645 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 3804 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 646 "cfg.y" /* yacc.c:1646  */
    { received_dns|= ((yyvsp[0].intval))?DO_DNS:0; }
#line 3810 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 647 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3816 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 648 "cfg.y" /* yacc.c:1646  */
    { received_dns|= ((yyvsp[0].intval))?DO_REV_DNS:0; }
#line 3822 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 649 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3828 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 650 "cfg.y" /* yacc.c:1646  */
    { dns_try_ipv6=(yyvsp[0].intval); }
#line 3834 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 651 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3840 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 652 "cfg.y" /* yacc.c:1646  */
    { dns_try_naptr=(yyvsp[0].intval); }
#line 3846 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 653 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3852 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 654 "cfg.y" /* yacc.c:1646  */
    { dns_retr_time=(yyvsp[0].intval); }
#line 3858 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 655 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3864 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 656 "cfg.y" /* yacc.c:1646  */
    { dns_retr_no=(yyvsp[0].intval); }
#line 3870 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 657 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3876 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 658 "cfg.y" /* yacc.c:1646  */
    { dns_servers_no=(yyvsp[0].intval); }
#line 3882 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 659 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3888 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 660 "cfg.y" /* yacc.c:1646  */
    { dns_search_list=(yyvsp[0].intval); }
#line 3894 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 661 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3900 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 662 "cfg.y" /* yacc.c:1646  */
    { max_while_loops=(yyvsp[0].intval); }
#line 3906 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 663 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3912 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 664 "cfg.y" /* yacc.c:1646  */
    { maxbuffer=(yyvsp[0].intval); }
#line 3918 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 665 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3924 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 666 "cfg.y" /* yacc.c:1646  */
    { children_no=(yyvsp[0].intval); }
#line 3930 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 667 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3936 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 668 "cfg.y" /* yacc.c:1646  */
    { check_via=(yyvsp[0].intval); }
#line 3942 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 669 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 3948 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 670 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef HP_MALLOC
			shm_hash_split_percentage=(yyvsp[0].intval);
			#else
			yyerror("Cannot set parameter; Please recompile with support "
				"for HP_MALLOC");
			#endif
			}
#line 3961 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 678 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3967 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 679 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef HP_MALLOC
			shm_secondary_hash_size=(yyvsp[0].intval);
			#else
			yyerror("Cannot set parameter; Please recompile with support"
				" for HP_MALLOC");
			#endif
			}
#line 3980 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 687 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 3986 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 688 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef HP_MALLOC
			mem_warming_enabled = (yyvsp[0].intval);
			#else
			yyerror("Cannot set parameter; Please recompile with support"
				" for HP_MALLOC");
			#endif
			}
#line 3999 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 696 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4005 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 697 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef HP_MALLOC
			mem_warming_pattern_file = (yyvsp[0].strval);
			#else
			yyerror("Cannot set parameter; Please recompile with "
				"support for HP_MALLOC");
			#endif
			}
#line 4018 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 705 "cfg.y" /* yacc.c:1646  */
    { yyerror("string expected"); }
#line 4024 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 706 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef HP_MALLOC
			mem_warming_percentage = (yyvsp[0].intval);
			#else
			yyerror("Cannot set parameter; Please recompile with "
				"support for HP_MALLOC");
			#endif
			}
#line 4037 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 714 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4043 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 715 "cfg.y" /* yacc.c:1646  */
    { memlog=(yyvsp[0].intval); memdump=(yyvsp[0].intval); }
#line 4049 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 716 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4055 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 717 "cfg.y" /* yacc.c:1646  */
    { memdump=(yyvsp[0].intval); }
#line 4061 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 718 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4067 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 719 "cfg.y" /* yacc.c:1646  */
    { execmsgthreshold=(yyvsp[0].intval); }
#line 4073 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 720 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4079 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 115:
#line 721 "cfg.y" /* yacc.c:1646  */
    { execdnsthreshold=(yyvsp[0].intval); }
#line 4085 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 116:
#line 722 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4091 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 723 "cfg.y" /* yacc.c:1646  */
    { tcpthreshold=(yyvsp[0].intval); }
#line 4097 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 118:
#line 724 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4103 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 725 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef STATISTICS
			if ((yyvsp[0].intval) < 0 || (yyvsp[0].intval) > 100)
				yyerror("SHM threshold has to be a percentage between"
					" 0 and 100");
			event_shm_threshold=(yyvsp[0].intval);
			#else
			yyerror("statistics support not compiled in");
			#endif /* STATISTICS */
			}
#line 4118 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 120:
#line 735 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4124 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 736 "cfg.y" /* yacc.c:1646  */
    {
			#ifdef STATISTICS
			if ((yyvsp[0].intval) < 0 || (yyvsp[0].intval) > 100)
				yyerror("PKG threshold has to be a percentage between "
					"0 and 100");
			event_pkg_threshold=(yyvsp[0].intval);
			#else
			yyerror("statistics support not compiled in");
			#endif
			}
#line 4139 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 122:
#line 746 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4145 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 123:
#line 747 "cfg.y" /* yacc.c:1646  */
    { query_buffer_size=(yyvsp[0].intval); }
#line 4151 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 124:
#line 748 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4157 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 125:
#line 749 "cfg.y" /* yacc.c:1646  */
    { query_flush_time=(yyvsp[0].intval); }
#line 4163 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 126:
#line 750 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4169 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 127:
#line 751 "cfg.y" /* yacc.c:1646  */
    { sip_warning=(yyvsp[0].intval); }
#line 4175 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 128:
#line 752 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4181 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 129:
#line 753 "cfg.y" /* yacc.c:1646  */
    { user=(yyvsp[0].strval); }
#line 4187 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 130:
#line 754 "cfg.y" /* yacc.c:1646  */
    { user=(yyvsp[0].strval); }
#line 4193 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 131:
#line 755 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4199 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 132:
#line 756 "cfg.y" /* yacc.c:1646  */
    { group=(yyvsp[0].strval); }
#line 4205 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 133:
#line 757 "cfg.y" /* yacc.c:1646  */
    { group=(yyvsp[0].strval); }
#line 4211 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 134:
#line 758 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4217 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 135:
#line 759 "cfg.y" /* yacc.c:1646  */
    { chroot_dir=(yyvsp[0].strval); }
#line 4223 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 136:
#line 760 "cfg.y" /* yacc.c:1646  */
    { chroot_dir=(yyvsp[0].strval); }
#line 4229 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 137:
#line 761 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4235 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 138:
#line 762 "cfg.y" /* yacc.c:1646  */
    { working_dir=(yyvsp[0].strval); }
#line 4241 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 139:
#line 763 "cfg.y" /* yacc.c:1646  */
    { working_dir=(yyvsp[0].strval); }
#line 4247 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 140:
#line 764 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4253 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 141:
#line 765 "cfg.y" /* yacc.c:1646  */
    { mhomed=(yyvsp[0].intval); }
#line 4259 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 142:
#line 766 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4265 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 143:
#line 767 "cfg.y" /* yacc.c:1646  */
    {
									io_poll_method=get_poll_type((yyvsp[0].strval));
									if (io_poll_method==POLL_NONE){
										LM_CRIT("bad poll method name:"
											" %s\n, try one of %s.\n",
											(yyvsp[0].strval), poll_support);
										yyerror("bad poll_method "
											"value");
									}
								}
#line 4280 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 144:
#line 777 "cfg.y" /* yacc.c:1646  */
    {
									io_poll_method=get_poll_type((yyvsp[0].strval));
									if (io_poll_method==POLL_NONE){
										LM_CRIT("bad poll method name:"
											" %s\n, try one of %s.\n",
											(yyvsp[0].strval), poll_support);
										yyerror("bad poll_method "
											"value");
									}
									}
#line 4295 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 145:
#line 787 "cfg.y" /* yacc.c:1646  */
    { yyerror("poll method name expected"); }
#line 4301 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 146:
#line 788 "cfg.y" /* yacc.c:1646  */
    {
				tcp_accept_aliases=(yyvsp[0].intval);
		}
#line 4309 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 147:
#line 791 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4315 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 148:
#line 792 "cfg.y" /* yacc.c:1646  */
    {
				tcp_children_no=(yyvsp[0].intval);
		}
#line 4323 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 149:
#line 795 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4329 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 150:
#line 796 "cfg.y" /* yacc.c:1646  */
    {
				tcp_connect_timeout=(yyvsp[0].intval);
		}
#line 4337 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 151:
#line 799 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4343 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 152:
#line 800 "cfg.y" /* yacc.c:1646  */
    {
				tcp_con_lifetime=(yyvsp[0].intval);
		}
#line 4351 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 153:
#line 803 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4357 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 154:
#line 804 "cfg.y" /* yacc.c:1646  */
    {
				tcp_listen_backlog=(yyvsp[0].intval);
		}
#line 4365 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 155:
#line 807 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4371 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 156:
#line 808 "cfg.y" /* yacc.c:1646  */
    {
				tcp_max_connections=(yyvsp[0].intval);
		}
#line 4379 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 157:
#line 811 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4385 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 158:
#line 812 "cfg.y" /* yacc.c:1646  */
    {
				tmp = NULL;
				fix_flag_name(tmp, (yyvsp[0].intval));
				tcp_no_new_conn_bflag =
					get_flag_id_by_name(FLAG_TYPE_BRANCH, tmp);
				if (!flag_in_range( (flag_t)tcp_no_new_conn_bflag ) )
					yyerror("invalid TCP no_new_conn Branch Flag");
				flag_idx2mask( &tcp_no_new_conn_bflag );
		}
#line 4399 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 159:
#line 821 "cfg.y" /* yacc.c:1646  */
    {
				tcp_no_new_conn_bflag =
					get_flag_id_by_name(FLAG_TYPE_BRANCH, (yyvsp[0].strval));
				if (!flag_in_range( (flag_t)tcp_no_new_conn_bflag ) )
					yyerror("invalid TCP no_new_conn Branch Flag");
				flag_idx2mask( &tcp_no_new_conn_bflag );
		}
#line 4411 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 160:
#line 828 "cfg.y" /* yacc.c:1646  */
    { yyerror("number value expected"); }
#line 4417 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 161:
#line 829 "cfg.y" /* yacc.c:1646  */
    {
				tcp_keepalive=(yyvsp[0].intval);
		}
#line 4425 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 162:
#line 832 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4431 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 163:
#line 833 "cfg.y" /* yacc.c:1646  */
    {
				tcp_max_msg_time=(yyvsp[0].intval);
		}
#line 4439 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 164:
#line 836 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4445 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 165:
#line 837 "cfg.y" /* yacc.c:1646  */
    {
			#ifndef HAVE_TCP_KEEPCNT
				warn("cannot be enabled TCP_KEEPCOUNT (no OS support)");
			#else
				tcp_keepcount=(yyvsp[0].intval);
			#endif
		}
#line 4457 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 166:
#line 844 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4463 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 167:
#line 845 "cfg.y" /* yacc.c:1646  */
    {
			#ifndef HAVE_TCP_KEEPIDLE
				warn("cannot be enabled TCP_KEEPIDLE (no OS support)");
			#else
				tcp_keepidle=(yyvsp[0].intval);
			#endif
		}
#line 4475 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 168:
#line 852 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4481 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 169:
#line 853 "cfg.y" /* yacc.c:1646  */
    {
			#ifndef HAVE_TCP_KEEPINTVL
				warn("cannot be enabled TCP_KEEPINTERVAL (no OS support)");
			#else
				tcp_keepinterval=(yyvsp[0].intval);
			 #endif
		}
#line 4493 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 170:
#line 860 "cfg.y" /* yacc.c:1646  */
    { yyerror("int value expected"); }
#line 4499 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 171:
#line 861 "cfg.y" /* yacc.c:1646  */
    { server_signature=(yyvsp[0].intval); }
#line 4505 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 172:
#line 862 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4511 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 173:
#line 863 "cfg.y" /* yacc.c:1646  */
    { server_header.s=(yyvsp[0].strval);
									server_header.len=strlen((yyvsp[0].strval));
									}
#line 4519 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 174:
#line 866 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4525 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 175:
#line 867 "cfg.y" /* yacc.c:1646  */
    { user_agent_header.s=(yyvsp[0].strval);
									user_agent_header.len=strlen((yyvsp[0].strval));
									}
#line 4533 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 176:
#line 870 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4539 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 177:
#line 871 "cfg.y" /* yacc.c:1646  */
    { xlog_buf_size = (yyvsp[0].intval); }
#line 4545 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 178:
#line 872 "cfg.y" /* yacc.c:1646  */
    { xlog_force_color = (yyvsp[0].intval); }
#line 4551 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 179:
#line 873 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4557 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 180:
#line 874 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4563 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 181:
#line 875 "cfg.y" /* yacc.c:1646  */
    {
							if (add_listener((yyvsp[0].sockid), 0)!=0){
								LM_CRIT("cfg. parser: failed"
										" to add listen address\n");
								break;
							}
						}
#line 4575 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 182:
#line 882 "cfg.y" /* yacc.c:1646  */
    { yyerror("ip address or hostname "
						"expected (use quotes if the hostname includes"
						" config keywords)"); }
#line 4583 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 183:
#line 885 "cfg.y" /* yacc.c:1646  */
    {
					if (bin) {
						yyerror("can only define one binary packet interface");
						YYABORT;
					}

					lst_tmp = mk_listen_id((yyvsp[-2].strval), PROTO_UDP, (yyvsp[0].intval));
					bin = new_sock_info(lst_tmp->name,
										lst_tmp->port,
										lst_tmp->proto,
										lst_tmp->adv_name,
										lst_tmp->adv_port,
										lst_tmp->children,
										0);
					if (!bin) {
						LM_CRIT("Failed to create new socket info!\n");
						YYABORT;
					}
							}
#line 4607 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 184:
#line 904 "cfg.y" /* yacc.c:1646  */
    { yyerror("ip address or hostname "
						"expected (use quotes if the hostname includes"
						" config keywords)"); }
#line 4615 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 185:
#line 907 "cfg.y" /* yacc.c:1646  */
    { bin_children=(yyvsp[0].intval); }
#line 4621 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 186:
#line 908 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4627 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 187:
#line 909 "cfg.y" /* yacc.c:1646  */
    {
							for(lst_tmp=(yyvsp[0].sockid); lst_tmp; lst_tmp=lst_tmp->next)
								add_alias(lst_tmp->name, strlen(lst_tmp->name),
											lst_tmp->port, lst_tmp->proto);
							  }
#line 4637 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 188:
#line 914 "cfg.y" /* yacc.c:1646  */
    { yyerror("hostname expected (use quotes"
							" if the hostname includes config keywords)"); }
#line 4644 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 189:
#line 916 "cfg.y" /* yacc.c:1646  */
    { auto_aliases=(yyvsp[0].intval); }
#line 4650 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 190:
#line 917 "cfg.y" /* yacc.c:1646  */
    { yyerror("number  expected"); }
#line 4656 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 191:
#line 918 "cfg.y" /* yacc.c:1646  */
    {
								if ((yyvsp[0].strval)) {
									default_global_address.s=(yyvsp[0].strval);
									default_global_address.len=strlen((yyvsp[0].strval));
								}
								}
#line 4667 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 192:
#line 924 "cfg.y" /* yacc.c:1646  */
    {yyerror("ip address or hostname "
												"expected"); }
#line 4674 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 193:
#line 926 "cfg.y" /* yacc.c:1646  */
    {
								tmp = int2str((yyvsp[0].intval), &i_tmp);
								if (i_tmp > default_global_port.len)
									default_global_port.s =
									pkg_realloc(default_global_port.s, i_tmp);
								if (!default_global_port.s) {
									LM_CRIT("cfg. parser: out of memory.\n");
									YYABORT;
								} else {
									default_global_port.len = i_tmp;
									memcpy(default_global_port.s, tmp,
											default_global_port.len);
								}
								}
#line 4693 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 194:
#line 940 "cfg.y" /* yacc.c:1646  */
    {yyerror("ip address or hostname "
												"expected"); }
#line 4700 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 195:
#line 942 "cfg.y" /* yacc.c:1646  */
    {
										disable_core_dump=(yyvsp[0].intval);
									}
#line 4708 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 196:
#line 945 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4714 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 197:
#line 946 "cfg.y" /* yacc.c:1646  */
    {
										open_files_limit=(yyvsp[0].intval);
									}
#line 4722 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 198:
#line 949 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4728 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 199:
#line 950 "cfg.y" /* yacc.c:1646  */
    {
								#ifdef USE_MCAST
										mcast_loopback=(yyvsp[0].intval);
								#else
									warn("no multicast support compiled in");
								#endif
		  }
#line 4740 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 200:
#line 957 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4746 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 201:
#line 958 "cfg.y" /* yacc.c:1646  */
    {
								#ifdef USE_MCAST
										mcast_ttl=(yyvsp[0].intval);
								#else
									warn("no multicast support compiled in");
								#endif
		  }
#line 4758 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 202:
#line 965 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected as tos"); }
#line 4764 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 203:
#line 966 "cfg.y" /* yacc.c:1646  */
    { tos = (yyvsp[0].intval);
							if (tos<=0)
								yyerror("invalid tos value");
		 }
#line 4773 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 204:
#line 970 "cfg.y" /* yacc.c:1646  */
    { if (strcasecmp((yyvsp[0].strval),"IPTOS_LOWDELAY")) {
								tos=IPTOS_LOWDELAY;
							} else if (strcasecmp((yyvsp[0].strval),"IPTOS_THROUGHPUT")) {
								tos=IPTOS_THROUGHPUT;
							} else if (strcasecmp((yyvsp[0].strval),"IPTOS_RELIABILITY")) {
								tos=IPTOS_RELIABILITY;
#if defined(IPTOS_MINCOST)
							} else if (strcasecmp((yyvsp[0].strval),"IPTOS_MINCOST")) {
								tos=IPTOS_MINCOST;
#endif
#if defined(IPTOS_LOWCOST)
							} else if (strcasecmp((yyvsp[0].strval),"IPTOS_LOWCOST")) {
								tos=IPTOS_LOWCOST;
#endif
							} else {
								yyerror("invalid tos value - allowed: "
									"IPTOS_LOWDELAY,IPTOS_THROUGHPUT,"
									"IPTOS_RELIABILITY"
#if defined(IPTOS_LOWCOST)
									",IPTOS_LOWCOST"
#endif
#if defined(IPTOS_MINCOST)
									",IPTOS_MINCOST"
#endif
									"\n");
							}
		 }
#line 4805 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 205:
#line 997 "cfg.y" /* yacc.c:1646  */
    { yyerror("number expected"); }
#line 4811 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 206:
#line 998 "cfg.y" /* yacc.c:1646  */
    { mpath=(yyvsp[0].strval); strcpy(mpath_buf, (yyvsp[0].strval));
								mpath_len=strlen((yyvsp[0].strval));
								if(mpath_buf[mpath_len-1]!='/') {
									mpath_buf[mpath_len]='/';
									mpath_len++;
									mpath_buf[mpath_len]='\0';
								}
							}
#line 4824 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 207:
#line 1006 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4830 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 208:
#line 1007 "cfg.y" /* yacc.c:1646  */
    {
										disable_dns_failover=(yyvsp[0].intval);
									}
#line 4838 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 209:
#line 1010 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4844 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 210:
#line 1011 "cfg.y" /* yacc.c:1646  */
    {
										disable_dns_blacklist=(yyvsp[0].intval);
									}
#line 4852 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 211:
#line 1014 "cfg.y" /* yacc.c:1646  */
    { yyerror("boolean value expected"); }
#line 4858 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 212:
#line 1015 "cfg.y" /* yacc.c:1646  */
    {
				s_tmp.s = (yyvsp[-4].strval);
				s_tmp.len = strlen((yyvsp[-4].strval));
				if ( create_bl_head( BL_CORE_ID, BL_READONLY_LIST,
				bl_head, bl_tail, &s_tmp)==0) {
					yyerror("failed to create blacklist\n");
					YYABORT;
				}
				bl_head = bl_tail = 0;
				}
#line 4873 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 213:
#line 1025 "cfg.y" /* yacc.c:1646  */
    {
				sl_fwd_disabled=(yyvsp[0].intval);
				}
#line 4881 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 214:
#line 1028 "cfg.y" /* yacc.c:1646  */
    { db_version_table=(yyvsp[0].strval); }
#line 4887 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 215:
#line 1029 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4893 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 216:
#line 1030 "cfg.y" /* yacc.c:1646  */
    { db_default_url=(yyvsp[0].strval); }
#line 4899 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 217:
#line 1031 "cfg.y" /* yacc.c:1646  */
    { yyerror("string value expected"); }
#line 4905 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 218:
#line 1032 "cfg.y" /* yacc.c:1646  */
    { db_max_async_connections=(yyvsp[0].intval); }
#line 4911 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 219:
#line 1033 "cfg.y" /* yacc.c:1646  */
    {
				yyerror("integer value expected");
				}
#line 4919 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 220:
#line 1036 "cfg.y" /* yacc.c:1646  */
    { disable_503_translation=(yyvsp[0].intval); }
#line 4925 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 221:
#line 1037 "cfg.y" /* yacc.c:1646  */
    {
				yyerror("integer value expected");
				}
#line 4933 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 222:
#line 1040 "cfg.y" /* yacc.c:1646  */
    { yyerror("unknown config variable"); }
#line 4939 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 223:
#line 1043 "cfg.y" /* yacc.c:1646  */
    {
			if (load_module((yyvsp[0].strval)) < 0)
				yyerrorf("failed to load module %s\n", (yyvsp[0].strval));
		}
#line 4948 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 224:
#line 1047 "cfg.y" /* yacc.c:1646  */
    { yyerror("string expected");  }
#line 4954 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 225:
#line 1048 "cfg.y" /* yacc.c:1646  */
    {
				if (set_mod_param_regex((yyvsp[-5].strval), (yyvsp[-3].strval), STR_PARAM, (yyvsp[-1].strval)) != 0) {
					yyerrorf("Parameter <%s> not found in module <%s> - "
						"can't set", (yyvsp[-3].strval), (yyvsp[-5].strval));
				}
			}
#line 4965 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 226:
#line 1054 "cfg.y" /* yacc.c:1646  */
    {
				if (set_mod_param_regex((yyvsp[-5].strval), (yyvsp[-3].strval), INT_PARAM, (void*)(yyvsp[-1].intval)) != 0) {
					yyerrorf("Parameter <%s> not found in module <%s> - "
						"can't set", (yyvsp[-3].strval), (yyvsp[-5].strval));
				}
			}
#line 4976 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 227:
#line 1060 "cfg.y" /* yacc.c:1646  */
    { yyerror("Invalid arguments"); }
#line 4982 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 228:
#line 1064 "cfg.y" /* yacc.c:1646  */
    { (yyval.ipaddr)=(yyvsp[0].ipaddr); }
#line 4988 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 229:
#line 1065 "cfg.y" /* yacc.c:1646  */
    { (yyval.ipaddr)=(yyvsp[0].ipaddr); }
#line 4994 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 230:
#line 1068 "cfg.y" /* yacc.c:1646  */
    {
											(yyval.ipaddr)=pkg_malloc(
													sizeof(struct ip_addr));
											if ((yyval.ipaddr)==0){
												LM_CRIT("cfg. parser: "
												        "out of memory\n");
												YYABORT;
											}else{
												memset((yyval.ipaddr), 0,
													sizeof(struct ip_addr));
												(yyval.ipaddr)->af=AF_INET;
												(yyval.ipaddr)->len=4;
												if (((yyvsp[-6].intval)>255) || ((yyvsp[-6].intval)<0) ||
													((yyvsp[-4].intval)>255) || ((yyvsp[-4].intval)<0) ||
													((yyvsp[-2].intval)>255) || ((yyvsp[-2].intval)<0) ||
													((yyvsp[0].intval)>255) || ((yyvsp[0].intval)<0)){
													yyerror("invalid ipv4"
															"address");
													(yyval.ipaddr)->u.addr32[0]=0;
													/* $$=0; */
												}else{
													(yyval.ipaddr)->u.addr[0]=(yyvsp[-6].intval);
													(yyval.ipaddr)->u.addr[1]=(yyvsp[-4].intval);
													(yyval.ipaddr)->u.addr[2]=(yyvsp[-2].intval);
													(yyval.ipaddr)->u.addr[3]=(yyvsp[0].intval);
													/*
													$$=htonl( ($1<<24)|
													($3<<16)| ($5<<8)|$7 );
													*/
												}
											}
												}
#line 5031 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 231:
#line 1102 "cfg.y" /* yacc.c:1646  */
    {
					(yyval.ipaddr)=pkg_malloc(sizeof(struct ip_addr));
					if ((yyval.ipaddr)==0){
						LM_CRIT("ERROR: cfg. parser: out of memory.\n");
						YYABORT;
					}else{
						memset((yyval.ipaddr), 0, sizeof(struct ip_addr));
						(yyval.ipaddr)->af=AF_INET6;
						(yyval.ipaddr)->len=16;
						if (inet_pton(AF_INET6, (yyvsp[0].strval), (yyval.ipaddr)->u.addr)<=0){
							yyerror("bad ipv6 address");
						}
					}
				}
#line 5050 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 232:
#line 1118 "cfg.y" /* yacc.c:1646  */
    { (yyval.ipaddr)=(yyvsp[0].ipaddr); }
#line 5056 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 233:
#line 1119 "cfg.y" /* yacc.c:1646  */
    {(yyval.ipaddr)=(yyvsp[-1].ipaddr); }
#line 5062 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 234:
#line 1122 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.strval) = (yyvsp[0].strval);
				}
#line 5070 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 235:
#line 1125 "cfg.y" /* yacc.c:1646  */
    {
				tmp=int2str((yyvsp[0].intval), &i_tmp);
				if (((yyval.strval)=pkg_malloc(i_tmp+1))==0) {
					yyerror("cfg. parser: out of memory.\n");
					YYABORT;
				}
				memcpy( (yyval.strval), tmp, i_tmp);
				(yyval.strval)[i_tmp] = 0;
				}
#line 5084 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 236:
#line 1134 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.strval) = (yyvsp[0].strval);
		}
#line 5092 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 237:
#line 1139 "cfg.y" /* yacc.c:1646  */
    {
						if (rlist[DEFAULT_RT].a!=0) {
							yyerror("overwriting default "
								"request routing table");
							YYABORT;
						}
						push((yyvsp[-1].action), &rlist[DEFAULT_RT].a);
					}
#line 5105 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 238:
#line 1147 "cfg.y" /* yacc.c:1646  */
    {
						if ( strtol((yyvsp[-4].strval),&tmp,10)==0 && *tmp==0) {
							/* route[0] detected */
							if (rlist[DEFAULT_RT].a!=0) {
								yyerror("overwriting(2) default "
									"request routing table");
								YYABORT;
							}
							push((yyvsp[-1].action), &rlist[DEFAULT_RT].a);
						} else {
							i_tmp = get_script_route_idx((yyvsp[-4].strval),rlist,RT_NO,1);
							if (i_tmp==-1) YYABORT;
							push((yyvsp[-1].action), &rlist[i_tmp].a);
						}
					}
#line 5125 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 239:
#line 1162 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid  route  statement"); }
#line 5131 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 240:
#line 1165 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = get_script_route_idx((yyvsp[-4].strval),failure_rlist,
								FAILURE_RT_NO,1);
						if (i_tmp==-1) YYABORT;
						push((yyvsp[-1].action), &failure_rlist[i_tmp].a);
					}
#line 5142 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 241:
#line 1171 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid failure_route statement"); }
#line 5148 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 242:
#line 1174 "cfg.y" /* yacc.c:1646  */
    {
						if (onreply_rlist[DEFAULT_RT].a!=0) {
							yyerror("overwriting default "
								"onreply routing table");
							YYABORT;
						}
						push((yyvsp[-1].action), &onreply_rlist[DEFAULT_RT].a);
					}
#line 5161 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 243:
#line 1182 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = get_script_route_idx((yyvsp[-4].strval),onreply_rlist,
								ONREPLY_RT_NO,1);
						if (i_tmp==-1) YYABORT;
						push((yyvsp[-1].action), &onreply_rlist[i_tmp].a);
					}
#line 5172 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 244:
#line 1188 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid onreply_route statement"); }
#line 5178 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 245:
#line 1191 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = get_script_route_idx((yyvsp[-4].strval),branch_rlist,
								BRANCH_RT_NO,1);
						if (i_tmp==-1) YYABORT;
						push((yyvsp[-1].action), &branch_rlist[i_tmp].a);
					}
#line 5189 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 246:
#line 1197 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid branch_route statement"); }
#line 5195 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 247:
#line 1200 "cfg.y" /* yacc.c:1646  */
    {
						if (error_rlist.a!=0) {
							yyerror("overwriting default "
								"error routing table");
							YYABORT;
						}
						push((yyvsp[-1].action), &error_rlist.a);
					}
#line 5208 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 248:
#line 1208 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid error_route statement"); }
#line 5214 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 249:
#line 1211 "cfg.y" /* yacc.c:1646  */
    {
						if (local_rlist.a!=0) {
							yyerror("re-definition of local "
								"route detected");
							YYABORT;
						}
						push((yyvsp[-1].action), &local_rlist.a);
					}
#line 5227 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 250:
#line 1219 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid local_route statement"); }
#line 5233 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 251:
#line 1222 "cfg.y" /* yacc.c:1646  */
    {
						if (startup_rlist.a!=0) {
							yyerror("re-definition of startup "
								"route detected");
							YYABORT;
						}
						push((yyvsp[-1].action), &startup_rlist.a);
					}
#line 5246 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 252:
#line 1230 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid startup_route statement"); }
#line 5252 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 253:
#line 1233 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = 0;
						while (timer_rlist[i_tmp].a!=0 && i_tmp < TIMER_RT_NO) {
							i_tmp++;
						}
						if(i_tmp == TIMER_RT_NO) {
							yyerror("Too many timer routes defined\n");
							YYABORT;
						}
						timer_rlist[i_tmp].interval = (yyvsp[-4].intval);
						push((yyvsp[-1].action), &timer_rlist[i_tmp].a);
					}
#line 5269 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 254:
#line 1245 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid timer_route statement"); }
#line 5275 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 255:
#line 1249 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = 1;
						while (event_rlist[i_tmp].a !=0 && i_tmp < EVENT_RT_NO) {
							if (strcmp((yyvsp[-4].strval), event_rlist[i_tmp].name) == 0) {
								LM_ERR("Script route <%s> redefined\n", (yyvsp[-4].strval));
								YYABORT;
							}
							i_tmp++;
						}

						if (i_tmp == EVENT_RT_NO) {
							yyerror("Too many event routes defined\n");
							YYABORT;
						}

						event_rlist[i_tmp].name = (yyvsp[-4].strval);
						event_rlist[i_tmp].mode = EV_ROUTE_SYNC;

						push((yyvsp[-1].action), &event_rlist[i_tmp].a);
					}
#line 5300 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 256:
#line 1269 "cfg.y" /* yacc.c:1646  */
    {

						i_tmp = 1;
						while (event_rlist[i_tmp].a !=0 && i_tmp < EVENT_RT_NO) {
							if (strcmp((yyvsp[-6].strval), event_rlist[i_tmp].name) == 0) {
								LM_ERR("Script route <%s> redefined\n", (yyvsp[-6].strval));
								YYABORT;
							}
							i_tmp++;
						}

						if (i_tmp == EVENT_RT_NO) {
							yyerror("Too many event routes defined\n");
							YYABORT;
						}

						event_rlist[i_tmp].name = (yyvsp[-6].strval);
						event_rlist[i_tmp].mode = EV_ROUTE_SYNC;

						push((yyvsp[-1].action), &event_rlist[i_tmp].a);
					}
#line 5326 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 257:
#line 1290 "cfg.y" /* yacc.c:1646  */
    {

						i_tmp = 1;
						while (event_rlist[i_tmp].a !=0 && i_tmp < EVENT_RT_NO) {
							if (strcmp((yyvsp[-6].strval), event_rlist[i_tmp].name) == 0) {
								LM_ERR("Script route <%s> redefined\n", (yyvsp[-6].strval));
								YYABORT;
							}
							i_tmp++;
						}

						if (i_tmp == EVENT_RT_NO) {
							yyerror("Too many event routes defined\n");
							YYABORT;
						}

						event_rlist[i_tmp].name = (yyvsp[-6].strval);
						event_rlist[i_tmp].mode = EV_ROUTE_ASYNC;

						push((yyvsp[-1].action), &event_rlist[i_tmp].a);
					}
#line 5352 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 258:
#line 1311 "cfg.y" /* yacc.c:1646  */
    { yyerror("invalid event_route statement"); }
#line 5358 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 259:
#line 1315 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_exp(AND_OP, (yyvsp[-2].expr), (yyvsp[0].expr)); }
#line 5364 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 260:
#line 1316 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_exp(OR_OP, (yyvsp[-2].expr), (yyvsp[0].expr));  }
#line 5370 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 261:
#line 1317 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_exp(NOT_OP, (yyvsp[0].expr), 0);  }
#line 5376 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 262:
#line 1318 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_exp(EVAL_OP, (yyvsp[-1].expr), 0); }
#line 5382 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 263:
#line 1319 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=(yyvsp[-1].expr); }
#line 5388 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 264:
#line 1320 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=(yyvsp[0].expr); }
#line 5394 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 265:
#line 1323 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=EQUAL_OP; }
#line 5400 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 266:
#line 1324 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=DIFF_OP; }
#line 5406 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 267:
#line 1327 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=GT_OP; }
#line 5412 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 268:
#line 1328 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=LT_OP; }
#line 5418 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 269:
#line 1329 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=GTE_OP; }
#line 5424 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 270:
#line 1330 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=LTE_OP; }
#line 5430 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 271:
#line 1332 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=MATCH_OP; }
#line 5436 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 272:
#line 1333 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=NOTMATCH_OP; }
#line 5442 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 273:
#line 1336 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=(yyvsp[0].intval); }
#line 5448 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 274:
#line 1337 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=(yyvsp[0].intval); }
#line 5454 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 275:
#line 1340 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=(yyvsp[0].intval); }
#line 5460 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 276:
#line 1341 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=(yyvsp[0].intval); }
#line 5466 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 277:
#line 1342 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=(yyvsp[0].intval); }
#line 5472 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 278:
#line 1345 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=URI_O;}
#line 5478 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 279:
#line 1346 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=FROM_URI_O;}
#line 5484 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 280:
#line 1347 "cfg.y" /* yacc.c:1646  */
    {(yyval.intval)=TO_URI_O;}
#line 5490 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 281:
#line 1350 "cfg.y" /* yacc.c:1646  */
    {
				spec = (pv_spec_t*)pkg_malloc(sizeof(pv_spec_t));
				if (spec==NULL){
					yyerror("no more pkg memory\n");
					YYABORT;
				}
				memset(spec, 0, sizeof(pv_spec_t));
				tstr.s = (yyvsp[0].strval);
				tstr.len = strlen(tstr.s);
				if(pv_parse_spec(&tstr, spec)==NULL)
				{
					yyerror("unknown script variable");
				}
				(yyval.specval) = spec;
			}
#line 5510 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 282:
#line 1365 "cfg.y" /* yacc.c:1646  */
    {
			(yyval.specval)=0; yyerror("invalid script variable name");
		}
#line 5518 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 283:
#line 1370 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr)=(yyvsp[0].expr); }
#line 5524 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 284:
#line 1371 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr)=mk_elem( NO_OP, ACTION_O, 0, ACTIONS_ST, (yyvsp[0].action) ); }
#line 5530 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 285:
#line 1372 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr)=mk_elem( NO_OP, NUMBER_O, 0, NUMBER_ST,
											(void*)(yyvsp[0].intval) ); }
#line 5537 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 286:
#line 1374 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem(NO_OP, SCRIPTVAR_O,0,SCRIPTVAR_ST,(void*)(yyvsp[0].specval));
			}
#line 5545 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 287:
#line 1377 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr) = mk_elem((yyvsp[-1].intval), (yyvsp[-2].intval), 0, STR_ST, (yyvsp[0].strval));
				 			}
#line 5552 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 288:
#line 1379 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), DSTIP_O, 0, NET_ST, (yyvsp[0].ipnet));
								}
#line 5559 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 289:
#line 1381 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), DSTIP_O, 0, STR_ST, (yyvsp[0].strval));
								}
#line 5566 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 290:
#line 1383 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), SRCIP_O, 0, NET_ST, (yyvsp[0].ipnet));
								}
#line 5573 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 291:
#line 1385 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), SRCIP_O, 0, STR_ST, (yyvsp[0].strval));
								}
#line 5580 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 292:
#line 1389 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr)= mk_elem((yyvsp[-1].intval), METHOD_O, 0, STR_ST, (yyvsp[0].strval));
									}
#line 5587 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 293:
#line 1391 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr) = mk_elem((yyvsp[-1].intval), METHOD_O, 0, STR_ST, (yyvsp[0].strval));
				 			}
#line 5594 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 294:
#line 1393 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("string expected"); }
#line 5600 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 295:
#line 1394 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("invalid operator,"
										"== , !=, or =~ expected");
						}
#line 5608 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 296:
#line 1397 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem( (yyvsp[-1].intval), SCRIPTVAR_O,(void*)(yyvsp[-2].specval),SCRIPTVAR_ST,(void*)(yyvsp[0].specval));
			}
#line 5616 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 297:
#line 1400 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem( (yyvsp[-1].intval), SCRIPTVAR_O,(void*)(yyvsp[-2].specval),STR_ST,(yyvsp[0].strval));
			}
#line 5624 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 298:
#line 1403 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem( (yyvsp[-1].intval), SCRIPTVAR_O,(void*)(yyvsp[-2].specval),STR_ST,(yyvsp[0].strval));
			}
#line 5632 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 299:
#line 1406 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem( (yyvsp[-1].intval), SCRIPTVAR_O,(void*)(yyvsp[-2].specval),NUMBER_ST,(void *)(yyvsp[0].intval));
			}
#line 5640 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 300:
#line 1409 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem( (yyvsp[-1].intval), SCRIPTVAR_O,(void*)(yyvsp[-2].specval), MYSELF_ST, 0);
			}
#line 5648 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 301:
#line 1412 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr)=mk_elem( (yyvsp[-1].intval), SCRIPTVAR_O,(void*)(yyvsp[-2].specval), NULLV_ST, 0);
			}
#line 5656 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 302:
#line 1415 "cfg.y" /* yacc.c:1646  */
    {(yyval.expr) = mk_elem((yyvsp[-1].intval), (yyvsp[-2].intval), 0, STR_ST, (yyvsp[0].strval));
				 				}
#line 5663 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 303:
#line 1417 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), (yyvsp[-2].intval), 0, MYSELF_ST, 0);
								}
#line 5670 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 304:
#line 1419 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("string or MYSELF expected"); }
#line 5676 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 305:
#line 1420 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("invalid operator,"
									" == , != or =~ expected");
					}
#line 5684 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 306:
#line 1423 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), SRCPORT_O, 0, NUMBER_ST,
												(void *) (yyvsp[0].intval) ); }
#line 5691 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 307:
#line 1425 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("number expected"); }
#line 5697 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 308:
#line 1426 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("==, !=, <,>, >= or <=  expected"); }
#line 5703 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 309:
#line 1427 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), DSTPORT_O, 0, NUMBER_ST,
												(void *) (yyvsp[0].intval) ); }
#line 5710 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 310:
#line 1429 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("number expected"); }
#line 5716 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 311:
#line 1430 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("==, !=, <,>, >= or <=  expected"); }
#line 5722 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 312:
#line 1431 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), PROTO_O, 0, NUMBER_ST,
												(void *) (yyvsp[0].intval) ); }
#line 5729 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 313:
#line 1433 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0;
								yyerror("protocol expected (udp, tcp or tls)");
							}
#line 5737 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 314:
#line 1436 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("equal/!= operator expected"); }
#line 5743 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 315:
#line 1437 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), AF_O, 0, NUMBER_ST,
												(void *) (yyvsp[0].intval) ); }
#line 5750 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 316:
#line 1439 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("number expected"); }
#line 5756 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 317:
#line 1440 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("equal/!= operator expected"); }
#line 5762 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 318:
#line 1441 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), MSGLEN_O, 0, NUMBER_ST,
												(void *) (yyvsp[0].intval) ); }
#line 5769 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 319:
#line 1443 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), MSGLEN_O, 0, NUMBER_ST,
												(void *) BUF_SIZE); }
#line 5776 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 320:
#line 1445 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("number expected"); }
#line 5782 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 321:
#line 1446 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror("equal/!= operator expected"); }
#line 5788 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 322:
#line 1447 "cfg.y" /* yacc.c:1646  */
    {	s_tmp.s=(yyvsp[0].strval);
									s_tmp.len=strlen((yyvsp[0].strval));
									ip_tmp=str2ip(&s_tmp);
									if (ip_tmp==0)
										ip_tmp=str2ip6(&s_tmp);
									if (ip_tmp){
										(yyval.expr)=mk_elem((yyvsp[-1].intval), SRCIP_O, 0, NET_ST,
												mk_net_bitlen(ip_tmp,
														ip_tmp->len*8) );
									}else{
										(yyval.expr)=mk_elem((yyvsp[-1].intval), SRCIP_O, 0, STR_ST,
												(yyvsp[0].strval));
									}
								}
#line 5807 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 323:
#line 1461 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), SRCIP_O, 0, MYSELF_ST, 0);
								}
#line 5814 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 324:
#line 1463 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror( "ip address or hostname"
						 "expected" ); }
#line 5821 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 325:
#line 1465 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0;
						 yyerror("invalid operator, ==, != or =~ expected");}
#line 5828 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 326:
#line 1467 "cfg.y" /* yacc.c:1646  */
    {	s_tmp.s=(yyvsp[0].strval);
									s_tmp.len=strlen((yyvsp[0].strval));
									ip_tmp=str2ip(&s_tmp);
									if (ip_tmp==0)
										ip_tmp=str2ip6(&s_tmp);
									if (ip_tmp){
										(yyval.expr)=mk_elem((yyvsp[-1].intval), DSTIP_O, 0, NET_ST,
												mk_net_bitlen(ip_tmp,
														ip_tmp->len*8) );
									}else{
										(yyval.expr)=mk_elem((yyvsp[-1].intval), DSTIP_O, 0, STR_ST,
												(yyvsp[0].strval));
									}
								}
#line 5847 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 327:
#line 1481 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), DSTIP_O, 0, MYSELF_ST, 0);
								}
#line 5854 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 328:
#line 1483 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0; yyerror( "ip address or hostname"
						 			"expected" ); }
#line 5861 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 329:
#line 1485 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0;
						yyerror("invalid operator, ==, != or =~ expected");}
#line 5868 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 330:
#line 1487 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), (yyvsp[0].intval), 0, MYSELF_ST, 0);
								}
#line 5875 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 331:
#line 1489 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), SRCIP_O, 0, MYSELF_ST, 0);
								}
#line 5882 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 332:
#line 1491 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem((yyvsp[-1].intval), DSTIP_O, 0, MYSELF_ST, 0);
								}
#line 5889 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 333:
#line 1493 "cfg.y" /* yacc.c:1646  */
    {	(yyval.expr)=0;
									yyerror(" URI, SRCIP or DSTIP expected"); }
#line 5896 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 334:
#line 1495 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=0;
							yyerror ("invalid operator, == or != expected");
						}
#line 5904 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 335:
#line 1500 "cfg.y" /* yacc.c:1646  */
    { (yyval.ipnet)=mk_net((yyvsp[-2].ipaddr), (yyvsp[0].ipaddr)); }
#line 5910 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 336:
#line 1501 "cfg.y" /* yacc.c:1646  */
    {	if (((yyvsp[0].intval)<0) || ((yyvsp[0].intval)>(long)(yyvsp[-2].ipaddr)->len*8)){
								yyerror("invalid bit number in netmask");
								(yyval.ipnet)=0;
							}else{
								(yyval.ipnet)=mk_net_bitlen((yyvsp[-2].ipaddr), (yyvsp[0].intval));
							/*
								$$=mk_net($1,
										htonl( ($3)?~( (1<<(32-$3))-1 ):0 ) );
							*/
							}
						}
#line 5926 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 337:
#line 1512 "cfg.y" /* yacc.c:1646  */
    { (yyval.ipnet)=mk_net_bitlen((yyvsp[0].ipaddr), (yyvsp[0].ipaddr)->len*8); }
#line 5932 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 338:
#line 1513 "cfg.y" /* yacc.c:1646  */
    { (yyval.ipnet)=0;
						 yyerror("netmask (eg:255.0.0.0 or 8) expected");
						}
#line 5940 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 339:
#line 1520 "cfg.y" /* yacc.c:1646  */
    {(yyval.strval)=".";}
#line 5946 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 340:
#line 1521 "cfg.y" /* yacc.c:1646  */
    {(yyval.strval)="-"; }
#line 5952 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 341:
#line 1524 "cfg.y" /* yacc.c:1646  */
    { (yyval.strval)=(yyvsp[0].strval); }
#line 5958 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 342:
#line 1525 "cfg.y" /* yacc.c:1646  */
    { (yyval.strval)=(char*)pkg_malloc(strlen((yyvsp[-2].strval))+1+strlen((yyvsp[0].strval))+1);
						  if ((yyval.strval)==0){
							LM_CRIT("cfg. parser: memory allocation"
										" failure while parsing host\n");
							YYABORT;
						  }else{
							memcpy((yyval.strval), (yyvsp[-2].strval), strlen((yyvsp[-2].strval)));
							(yyval.strval)[strlen((yyvsp[-2].strval))]=*(yyvsp[-1].strval);
							memcpy((yyval.strval)+strlen((yyvsp[-2].strval))+1, (yyvsp[0].strval), strlen((yyvsp[0].strval)));
							(yyval.strval)[strlen((yyvsp[-2].strval))+1+strlen((yyvsp[0].strval))]=0;
						  }
						  pkg_free((yyvsp[-2].strval)); pkg_free((yyvsp[0].strval));
						}
#line 5976 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 343:
#line 1538 "cfg.y" /* yacc.c:1646  */
    { (yyval.strval)=0; pkg_free((yyvsp[-2].strval)); yyerror("invalid hostname (use quotes if hostname has config keywords)"); }
#line 5982 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 344:
#line 1542 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = EQ_T; }
#line 5988 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 345:
#line 1543 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = COLONEQ_T; }
#line 5994 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 346:
#line 1544 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = PLUSEQ_T; }
#line 6000 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 347:
#line 1545 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = MINUSEQ_T;}
#line 6006 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 348:
#line 1546 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = DIVEQ_T; }
#line 6012 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 349:
#line 1547 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = MULTEQ_T; }
#line 6018 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 350:
#line 1548 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = MODULOEQ_T; }
#line 6024 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 351:
#line 1549 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = BANDEQ_T; }
#line 6030 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 352:
#line 1550 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = BOREQ_T; }
#line 6036 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 353:
#line 1551 "cfg.y" /* yacc.c:1646  */
    { (yyval.intval) = BXOREQ_T; }
#line 6042 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 354:
#line 1555 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr) = mk_elem(VALUE_OP, NUMBERV_O, (void*)(yyvsp[0].intval), 0, 0); }
#line 6048 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 355:
#line 1556 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr) = mk_elem(VALUE_OP, STRINGV_O, (yyvsp[0].strval), 0, 0); }
#line 6054 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 356:
#line 1557 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr) = mk_elem(VALUE_OP, STRINGV_O, (yyvsp[0].strval), 0, 0); }
#line 6060 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 357:
#line 1558 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr) = mk_elem(VALUE_OP, SCRIPTVAR_O, (yyvsp[0].specval), 0, 0); }
#line 6066 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 358:
#line 1559 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)= (yyvsp[0].expr); }
#line 6072 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 359:
#line 1560 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr)=mk_elem( NO_OP, ACTION_O, 0, ACTIONS_ST, (yyvsp[0].action) ); }
#line 6078 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 360:
#line 1561 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(PLUS_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6086 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 361:
#line 1564 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(MINUS_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6094 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 362:
#line 1567 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(MULT_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6102 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 363:
#line 1570 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(DIV_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6110 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 364:
#line 1573 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(MODULO_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6118 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 365:
#line 1576 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(BAND_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6126 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 366:
#line 1579 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(BOR_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6134 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 367:
#line 1582 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(BXOR_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6142 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 368:
#line 1585 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(BLSHIFT_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6150 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 369:
#line 1588 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(BRSHIFT_OP, EXPR_O, (yyvsp[-2].expr), EXPR_ST, (yyvsp[0].expr));
			}
#line 6158 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 370:
#line 1591 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.expr) = mk_elem(BNOT_OP, EXPR_O, (yyvsp[0].expr), 0, 0);
			}
#line 6166 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 371:
#line 1594 "cfg.y" /* yacc.c:1646  */
    { (yyval.expr) = (yyvsp[-1].expr); }
#line 6172 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 372:
#line 1597 "cfg.y" /* yacc.c:1646  */
    {
			if(!pv_is_w((yyvsp[-2].specval)))
				yyerror("invalid left operand in assignment");
			if((yyvsp[-2].specval)->trans!=0)
				yyerror("transformations not accepted in right side "
					"of assignment");

			mk_action2( (yyval.action), (yyvsp[-1].intval),
					SCRIPTVAR_ST,
					EXPR_ST,
					(yyvsp[-2].specval),
					(yyvsp[0].expr));
		}
#line 6190 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 373:
#line 1610 "cfg.y" /* yacc.c:1646  */
    {
			if(!pv_is_w((yyvsp[-2].specval)))
				yyerror("invalid left operand in assignment");
			if((yyvsp[-2].specval)->trans!=0)
				yyerror("transformations not accepted in right side "
					"of assignment");

			mk_action2( (yyval.action), EQ_T,
					SCRIPTVAR_ST,
					NULLV_ST,
					(yyvsp[-2].specval),
					0);
		}
#line 6208 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 374:
#line 1623 "cfg.y" /* yacc.c:1646  */
    {
			if(!pv_is_w((yyvsp[-2].specval)))
				yyerror("invalid left operand in assignment");
			/* not all can get NULL with := */
			switch((yyvsp[-2].specval)->type) {
				case PVT_AVP:
				break;
				default:
					yyerror("invalid left operand in NULL assignment");
			}
			if((yyvsp[-2].specval)->trans!=0)
				yyerror("transformations not accepted in right side "
					"of assignment");

			mk_action2( (yyval.action), COLONEQ_T,
					SCRIPTVAR_ST,
					NULLV_ST,
					(yyvsp[-2].specval),
					0);
		}
#line 6233 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 375:
#line 1645 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action); }
#line 6239 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 376:
#line 1646 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action); }
#line 6245 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 377:
#line 1647 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action); }
#line 6251 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 378:
#line 1648 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[-1].action); }
#line 6257 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 379:
#line 1649 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; }
#line 6263 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 380:
#line 1652 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action); }
#line 6269 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 381:
#line 1653 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[-1].action); }
#line 6275 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 382:
#line 1654 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; }
#line 6281 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 383:
#line 1657 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=append_action((yyvsp[-1].action), (yyvsp[0].action)); }
#line 6287 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 384:
#line 1658 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=(yyvsp[0].action);}
#line 6293 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 385:
#line 1659 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad command!)"); }
#line 6299 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 386:
#line 1662 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=(yyvsp[-1].action);}
#line 6305 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 387:
#line 1663 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=(yyvsp[0].action);}
#line 6311 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 388:
#line 1664 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action);}
#line 6317 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 389:
#line 1665 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action);}
#line 6323 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 390:
#line 1666 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=(yyvsp[0].action);}
#line 6329 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 391:
#line 1667 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=(yyvsp[-1].action);}
#line 6335 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 392:
#line 1668 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0;}
#line 6341 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 393:
#line 1669 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad command: missing ';'?"); }
#line 6347 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 394:
#line 1672 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), IF_T,
													 EXPR_ST,
													 ACTIONS_ST,
													 NOSUBTYPE,
													 (yyvsp[-1].expr),
													 (yyvsp[0].action),
													 0);
									}
#line 6360 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 395:
#line 1680 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), IF_T,
													 EXPR_ST,
													 ACTIONS_ST,
													 ACTIONS_ST,
													 (yyvsp[-3].expr),
													 (yyvsp[-2].action),
													 (yyvsp[0].action));
									}
#line 6373 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 396:
#line 1690 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), WHILE_T,
													 EXPR_ST,
													 ACTIONS_ST,
													 (yyvsp[-1].expr),
													 (yyvsp[0].action));
									}
#line 6384 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 397:
#line 1698 "cfg.y" /* yacc.c:1646  */
    {
					if ((yyvsp[-4].specval)->type != PVT_SCRIPTVAR &&
					    (yyvsp[-4].specval)->type != PVT_AVP) {
						yyerror("\nfor-each statement: only \"var\" "
					            "and \"avp\" iterators are supported");
					}

					mk_action3( (yyval.action), FOR_EACH_T,
					            SCRIPTVAR_ST,
					            SCRIPTVAR_ST,
					            ACTIONS_ST,
					            (yyvsp[-4].specval),
					            (yyvsp[-2].specval),
					            (yyvsp[0].action));
					}
#line 6404 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 398:
#line 1715 "cfg.y" /* yacc.c:1646  */
    {
											mk_action2( (yyval.action), SWITCH_T,
														SCRIPTVAR_ST,
														ACTIONS_ST,
														(yyvsp[-4].specval),
														(yyvsp[-1].action));
									}
#line 6416 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 399:
#line 1724 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=append_action((yyvsp[-1].action), (yyvsp[0].action)); }
#line 6422 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 400:
#line 1725 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=(yyvsp[0].action); }
#line 6428 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 401:
#line 1727 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=append_action((yyvsp[-1].action), (yyvsp[0].action)); }
#line 6434 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 402:
#line 1728 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=(yyvsp[0].action);}
#line 6440 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 403:
#line 1732 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													NUMBER_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-4].intval),
													(yyvsp[-2].action),
													(void*)1);
											}
#line 6453 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 404:
#line 1741 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													NUMBER_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-3].intval),
													0,
													(void*)1);
											}
#line 6466 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 405:
#line 1749 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													NUMBER_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-2].intval),
													(yyvsp[0].action),
													(void*)0);
									}
#line 6479 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 406:
#line 1757 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													NUMBER_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-1].intval),
													0,
													(void*)0);
							}
#line 6492 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 407:
#line 1766 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													STR_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-4].strval),
													(yyvsp[-2].action),
													(void*)1);
											}
#line 6505 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 408:
#line 1775 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													STR_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-3].strval),
													0,
													(void*)1);
											}
#line 6518 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 409:
#line 1783 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													STR_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-2].strval),
													(yyvsp[0].action),
													(void*)0);
									}
#line 6531 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 410:
#line 1791 "cfg.y" /* yacc.c:1646  */
    { mk_action3( (yyval.action), CASE_T,
													STR_ST,
													ACTIONS_ST,
													NUMBER_ST,
													(void*)(yyvsp[-1].strval),
													0,
													(void*)0);
							}
#line 6544 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 411:
#line 1802 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), DEFAULT_T,
													ACTIONS_ST,
													0,
													(yyvsp[0].action),
													0);
									}
#line 6555 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 412:
#line 1808 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), DEFAULT_T,
													ACTIONS_ST,
													0,
													0,
													0);
									}
#line 6566 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 413:
#line 1816 "cfg.y" /* yacc.c:1646  */
    {
										elems[1].type = STRING_ST;
										elems[1].u.data = (yyvsp[0].strval);
										(yyval.intval)=1;
										}
#line 6576 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 414:
#line 1821 "cfg.y" /* yacc.c:1646  */
    {
										if ((yyvsp[-2].intval)+1>=MAX_ACTION_ELEMS) {
											yyerror("too many arguments "
												"in function\n");
											(yyval.intval)=0;
										}
										elems[(yyvsp[-2].intval)+1].type = STRING_ST;
										elems[(yyvsp[-2].intval)+1].u.data = (yyvsp[0].strval);
										(yyval.intval)=(yyvsp[-2].intval)+1;
										}
#line 6591 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 415:
#line 1831 "cfg.y" /* yacc.c:1646  */
    {
										elems[1].type = NULLV_ST;
										elems[1].u.data = NULL;
										elems[2].type = NULLV_ST;
										elems[2].u.data = NULL;
										(yyval.intval)=2;
										}
#line 6603 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 416:
#line 1838 "cfg.y" /* yacc.c:1646  */
    {
										elems[1].type = NULLV_ST;
										elems[1].u.data = NULL;
										elems[2].type = STRING_ST;
										elems[2].u.data = (yyvsp[0].strval);
										(yyval.intval)=2;
										}
#line 6615 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 417:
#line 1845 "cfg.y" /* yacc.c:1646  */
    {
										if ((yyvsp[-1].intval)+1>=MAX_ACTION_ELEMS) {
											yyerror("too many arguments "
												"in function\n");
											(yyval.intval)=0;
										}
										elems[(yyvsp[-1].intval)+1].type = NULLV_ST;
										elems[(yyvsp[-1].intval)+1].u.data = NULL;
										(yyval.intval)=(yyvsp[-1].intval)+1;
										}
#line 6630 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 418:
#line 1855 "cfg.y" /* yacc.c:1646  */
    {
										(yyval.intval)=0;
										yyerror("numbers used as parameters -"
											" they should be quoted");
										}
#line 6640 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 419:
#line 1860 "cfg.y" /* yacc.c:1646  */
    {
										(yyval.intval)=0;
										yyerror("numbers used as parameters -"
											" they should be quoted");
										}
#line 6650 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 420:
#line 1865 "cfg.y" /* yacc.c:1646  */
    {
										(yyval.intval)=0;
										yyerror("numbers used as parameters -"
											" they should be quoted");
										}
#line 6660 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 421:
#line 1872 "cfg.y" /* yacc.c:1646  */
    {
						route_elems[0].type = STRING_ST;
						route_elems[0].u.data = (yyvsp[0].strval);
						(yyval.intval)=1;
			}
#line 6670 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 422:
#line 1877 "cfg.y" /* yacc.c:1646  */
    {
						route_elems[0].type = NUMBER_ST;
						route_elems[0].u.data = (void*)(long)(yyvsp[0].intval);
						(yyval.intval)=1;
			}
#line 6680 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 423:
#line 1882 "cfg.y" /* yacc.c:1646  */
    {
						route_elems[0].type = NULLV_ST;
						route_elems[0].u.data = 0;
						(yyval.intval)=1;
			}
#line 6690 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 424:
#line 1887 "cfg.y" /* yacc.c:1646  */
    {
						route_elems[0].type = SCRIPTVAR_ST;
						route_elems[0].u.data = (yyvsp[0].specval);
						(yyval.intval)=1;
			}
#line 6700 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 425:
#line 1892 "cfg.y" /* yacc.c:1646  */
    {
						if ((yyvsp[-2].intval)>=MAX_ACTION_ELEMS) {
							yyerror("too many arguments in function\n");
							(yyval.intval)=-1;
						} else {
							route_elems[(yyvsp[-2].intval)].type = STRING_ST;
							route_elems[(yyvsp[-2].intval)].u.data = (yyvsp[0].strval);
							(yyval.intval)=(yyvsp[-2].intval)+1;
						}
			}
#line 6715 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 426:
#line 1902 "cfg.y" /* yacc.c:1646  */
    {
						if ((yyvsp[-2].intval)>=MAX_ACTION_ELEMS) {
							yyerror("too many arguments in function\n");
							(yyval.intval)=-1;
						} else {
							route_elems[(yyvsp[-2].intval)].type = NUMBER_ST;
							route_elems[(yyvsp[-2].intval)].u.data = (void*)(long)(yyvsp[0].intval);
							(yyval.intval)=(yyvsp[-2].intval)+1;
						}
			}
#line 6730 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 427:
#line 1912 "cfg.y" /* yacc.c:1646  */
    {
						if ((yyvsp[-2].intval)+1>=MAX_ACTION_ELEMS) {
							yyerror("too many arguments in function\n");
							(yyval.intval)=-1;
						} else {
							route_elems[(yyvsp[-2].intval)].type = SCRIPTVAR_ST;
							route_elems[(yyvsp[-2].intval)].u.data = (yyvsp[0].specval);
							(yyval.intval)=(yyvsp[-2].intval)+1;
						}
			}
#line 6745 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 428:
#line 1924 "cfg.y" /* yacc.c:1646  */
    {
				cmd_tmp=(void*)find_acmd_export_t((yyvsp[-2].strval), 0);
				if (cmd_tmp==0){
					yyerrorf("unknown async command <%s>, "
						"missing loadmodule?", (yyvsp[-2].strval));
					(yyval.action)=0;
				}else{
					elems[0].type = ACMD_ST;
					elems[0].u.data = cmd_tmp;
					mk_action_((yyval.action), AMODULE_T, 1, elems);
				}
			}
#line 6762 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 429:
#line 1936 "cfg.y" /* yacc.c:1646  */
    {
				cmd_tmp=(void*)find_acmd_export_t((yyvsp[-3].strval), (yyvsp[-1].intval));
				if (cmd_tmp==0){
					yyerrorf("unknown async command <%s>, "
						"missing loadmodule?", (yyvsp[-3].strval));
					(yyval.action)=0;
				}else{
					elems[0].type = ACMD_ST;
					elems[0].u.data = cmd_tmp;
					mk_action_((yyval.action), AMODULE_T, (yyvsp[-1].intval)+1, elems);
				}
			}
#line 6779 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 430:
#line 1948 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.action)=0;
				yyerrorf("bad arguments for command <%s>", (yyvsp[-3].strval));
			}
#line 6788 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 431:
#line 1952 "cfg.y" /* yacc.c:1646  */
    {
				(yyval.action)=0;
				yyerrorf("bare word <%s> found, command calls need '()'", (yyvsp[-1].strval));
			}
#line 6797 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 432:
#line 1958 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), FORWARD_T,
											STRING_ST,
											0,
											(yyvsp[-1].strval),
											0);
										}
#line 6808 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 433:
#line 1964 "cfg.y" /* yacc.c:1646  */
    {
										mk_action2( (yyval.action), FORWARD_T,
											0,
											0,
											0,
											0);
										}
#line 6820 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 434:
#line 1971 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 6826 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 435:
#line 1972 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad forward "
										"argument"); }
#line 6833 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 436:
#line 1975 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SEND_T,
											STRING_ST,
											0,
											(yyvsp[-1].strval),
											0);
										}
#line 6844 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 437:
#line 1981 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SEND_T,
											STRING_ST,
											STRING_ST,
											(yyvsp[-3].strval),
											(yyvsp[-1].strval));
										}
#line 6855 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 438:
#line 1987 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 6861 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 439:
#line 1988 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad send"
													"argument"); }
#line 6868 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 440:
#line 1990 "cfg.y" /* yacc.c:1646  */
    {
			mk_action2( (yyval.action), ASSERT_T, EXPR_ST, STRING_ST, (yyvsp[-3].expr), (yyvsp[-1].strval));
			}
#line 6876 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 441:
#line 1993 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), DROP_T,0, 0, 0, 0); }
#line 6882 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 442:
#line 1994 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), DROP_T,0, 0, 0, 0); }
#line 6888 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 443:
#line 1995 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), EXIT_T,0, 0, 0, 0); }
#line 6894 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 444:
#line 1996 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), EXIT_T,0, 0, 0, 0); }
#line 6900 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 445:
#line 1997 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RETURN_T,
																NUMBER_ST,
																0,
																(void*)(yyvsp[-1].intval),
																0);
												}
#line 6911 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 446:
#line 2003 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RETURN_T,
																SCRIPTVAR_ST,
																0,
																(void*)(yyvsp[-1].specval),
																0);
												}
#line 6922 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 447:
#line 2009 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RETURN_T,
																NUMBER_ST,
																0,
																(void*)1,
																0);
												}
#line 6933 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 448:
#line 2015 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RETURN_T,
																NUMBER_ST,
																0,
																(void*)1,
																0);
												}
#line 6944 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 449:
#line 2021 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), LOG_T, NUMBER_ST,
													STRING_ST,(void*)4,(yyvsp[-1].strval));
									}
#line 6952 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 450:
#line 2024 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), LOG_T,
																NUMBER_ST,
																STRING_ST,
																(void*)(yyvsp[-3].intval),
																(yyvsp[-1].strval));
												}
#line 6963 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 451:
#line 2030 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 6969 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 452:
#line 2031 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad log"
									"argument"); }
#line 6976 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 453:
#line 2033 "cfg.y" /* yacc.c:1646  */
    {
			mk_action2((yyval.action), SET_DEBUG_T, NUMBER_ST, 0, (void *)(yyvsp[-1].intval), 0 );
			}
#line 6984 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 454:
#line 2036 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), SET_DEBUG_T, 0, 0, 0, 0 ); }
#line 6990 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 455:
#line 2037 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 6996 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 456:
#line 2038 "cfg.y" /* yacc.c:1646  */
    {
			mk_action2((yyval.action), SETFLAG_T, NUMBER_ST, 0, (void *)(yyvsp[-1].intval), 0 );
			}
#line 7004 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 457:
#line 2041 "cfg.y" /* yacc.c:1646  */
    {mk_action2((yyval.action), SETFLAG_T, STR_ST, 0,
													(void *)(yyvsp[-1].strval), 0 ); }
#line 7011 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 458:
#line 2043 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7017 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 459:
#line 2044 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RESETFLAG_T,
										NUMBER_ST, 0, (void *)(yyvsp[-1].intval), 0 ); }
#line 7024 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 460:
#line 2046 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RESETFLAG_T,
										STR_ST, 0, (void *)(yyvsp[-1].strval), 0 ); }
#line 7031 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 461:
#line 2048 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7037 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 462:
#line 2049 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), ISFLAGSET_T,
										NUMBER_ST, 0, (void *)(yyvsp[-1].intval), 0 ); }
#line 7044 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 463:
#line 2051 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), ISFLAGSET_T,
										STR_ST, 0, (void *)(yyvsp[-1].strval), 0 ); }
#line 7051 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 464:
#line 2053 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7057 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 465:
#line 2054 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), SETSFLAG_T, NUMBER_ST,
										0, (void *)(yyvsp[-1].intval), 0 ); }
#line 7064 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 466:
#line 2056 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), SETSFLAG_T, STR_ST,
										0, (void *)(yyvsp[-1].strval), 0 ); }
#line 7071 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 467:
#line 2058 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7077 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 468:
#line 2059 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RESETSFLAG_T,
										NUMBER_ST, 0, (void *)(yyvsp[-1].intval), 0 ); }
#line 7084 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 469:
#line 2061 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), RESETSFLAG_T,
										STR_ST, 0, (void *)(yyvsp[-1].strval), 0 ); }
#line 7091 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 470:
#line 2063 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7097 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 471:
#line 2064 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), ISSFLAGSET_T,
										NUMBER_ST, 0, (void *)(yyvsp[-1].intval), 0 ); }
#line 7104 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 472:
#line 2066 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), ISSFLAGSET_T,
										STR_ST, 0, (void *)(yyvsp[-1].strval), 0 ); }
#line 7111 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 473:
#line 2068 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7117 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 474:
#line 2069 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													SETBFLAG_T,
													NUMBER_ST, NUMBER_ST,
													(void *)(yyvsp[-3].intval), (void *)(yyvsp[-1].intval) ); }
#line 7126 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 475:
#line 2073 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													SETBFLAG_T,
													NUMBER_ST, STR_ST,
													(void *)(yyvsp[-3].intval), (void *)(yyvsp[-1].strval) ); }
#line 7135 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 476:
#line 2077 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), SETBFLAG_T,
													NUMBER_ST, NUMBER_ST,
													0, (void *)(yyvsp[-1].intval) ); }
#line 7143 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 477:
#line 2080 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), SETBFLAG_T,
													NUMBER_ST, STR_ST,
													0, (void *)(yyvsp[-1].strval) ); }
#line 7151 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 478:
#line 2083 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7157 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 479:
#line 2084 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													RESETBFLAG_T,
													NUMBER_ST, NUMBER_ST,
													(void *)(yyvsp[-3].intval), (void *)(yyvsp[-1].intval) ); }
#line 7166 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 480:
#line 2088 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													RESETBFLAG_T,
													NUMBER_ST, STR_ST,
													(void *)(yyvsp[-3].intval), (void *)(yyvsp[-1].strval) ); }
#line 7175 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 481:
#line 2092 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													RESETBFLAG_T,
													NUMBER_ST, NUMBER_ST,
													0, (void *)(yyvsp[-1].intval) ); }
#line 7184 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 482:
#line 2096 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													RESETBFLAG_T,
													NUMBER_ST, STR_ST,
													0, (void *)(yyvsp[-1].strval) ); }
#line 7193 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 483:
#line 2100 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7199 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 484:
#line 2101 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													ISBFLAGSET_T,
													NUMBER_ST, NUMBER_ST,
													(void *)(yyvsp[-3].intval), (void *)(yyvsp[-1].intval) ); }
#line 7208 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 485:
#line 2105 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													ISBFLAGSET_T,
													NUMBER_ST, STR_ST,
													(void *)(yyvsp[-3].intval), (void *)(yyvsp[-1].strval) ); }
#line 7217 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 486:
#line 2109 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													ISBFLAGSET_T,
													NUMBER_ST, NUMBER_ST,
													0, (void *)(yyvsp[-1].intval) ); }
#line 7226 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 487:
#line 2113 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action),
													ISBFLAGSET_T,
													NUMBER_ST, STR_ST,
													0, (void *)(yyvsp[-1].strval) ); }
#line 7235 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 488:
#line 2117 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')'?"); }
#line 7241 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 489:
#line 2118 "cfg.y" /* yacc.c:1646  */
    {mk_action2( (yyval.action), ERROR_T,
																STRING_ST,
																STRING_ST,
																(yyvsp[-3].strval),
																(yyvsp[-1].strval));
												  }
#line 7252 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 490:
#line 2124 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7258 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 491:
#line 2125 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad error"
														"argument"); }
#line 7265 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 492:
#line 2127 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = get_script_route_idx( (yyvsp[-1].strval), rlist, RT_NO, 0);
						if (i_tmp==-1) yyerror("too many script routes");
						mk_action2( (yyval.action), ROUTE_T, NUMBER_ST,
							0, (void*)(long)i_tmp, 0);
					}
#line 7276 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 493:
#line 2134 "cfg.y" /* yacc.c:1646  */
    {
						i_tmp = get_script_route_idx( (yyvsp[-3].strval), rlist, RT_NO, 0);
						if (i_tmp==-1) yyerror("too many script routes");
						if ((yyvsp[-1].intval) <= 0) yyerror("too many route parameters");

						/* duplicate the list */
						a_tmp = pkg_malloc((yyvsp[-1].intval) * sizeof(action_elem_t));
						if (!a_tmp) {
							yyerror("no more pkg memory");
							YYABORT;
						}
						memcpy(a_tmp, route_elems, (yyvsp[-1].intval)*sizeof(action_elem_t));

						mk_action3( (yyval.action), ROUTE_T, NUMBER_ST,	/* route idx */
							NUMBER_ST,					/* number of params */
							SCRIPTVAR_ELEM_ST,			/* parameters */
							(void*)(long)i_tmp,
							(void*)(long)(yyvsp[-1].intval),
							(void*)a_tmp);
					}
#line 7301 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 494:
#line 2155 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7307 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 495:
#line 2156 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad route"
						"argument"); }
#line 7314 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 496:
#line 2158 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_HOST_T, STR_ST,
														0, (yyvsp[-1].strval), 0); }
#line 7321 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 497:
#line 2160 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7327 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 498:
#line 2161 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"string expected"); }
#line 7334 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 499:
#line 2164 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), PREFIX_T, STR_ST,
														0, (yyvsp[-1].strval), 0); }
#line 7341 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 500:
#line 2166 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7347 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 501:
#line 2167 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"string expected"); }
#line 7354 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 502:
#line 2169 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), STRIP_TAIL_T,
									NUMBER_ST, 0, (void *) (yyvsp[-1].intval), 0); }
#line 7361 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 503:
#line 2171 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7367 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 504:
#line 2172 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"number expected"); }
#line 7374 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 505:
#line 2175 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), STRIP_T, NUMBER_ST,
														0, (void *) (yyvsp[-1].intval), 0); }
#line 7381 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 506:
#line 2177 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7387 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 507:
#line 2178 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"number expected"); }
#line 7394 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 508:
#line 2180 "cfg.y" /* yacc.c:1646  */
    {
				{   qvalue_t q;
				if (str2q(&q, (yyvsp[-1].strval), strlen((yyvsp[-1].strval))) < 0) {
					yyerror("bad argument, q value expected");
				}
				mk_action2( (yyval.action), APPEND_BRANCH_T, STR_ST, NUMBER_ST, (yyvsp[-3].strval),
						(void *)(long)q); }
		}
#line 7407 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 509:
#line 2188 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), APPEND_BRANCH_T,
						STR_ST, NUMBER_ST, (yyvsp[-1].strval), (void *)Q_UNSPECIFIED) ; }
#line 7414 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 510:
#line 2190 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), APPEND_BRANCH_T,
						STR_ST, NUMBER_ST, 0, (void *)Q_UNSPECIFIED) ; }
#line 7421 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 511:
#line 2192 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), APPEND_BRANCH_T,
						STR_ST, NUMBER_ST, 0, (void *)Q_UNSPECIFIED ) ; }
#line 7428 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 512:
#line 2194 "cfg.y" /* yacc.c:1646  */
    {
						mk_action1((yyval.action), REMOVE_BRANCH_T, NUMBER_ST, (void*)(yyvsp[-1].intval));}
#line 7435 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 513:
#line 2196 "cfg.y" /* yacc.c:1646  */
    {
						mk_action1( (yyval.action), REMOVE_BRANCH_T, SCRIPTVAR_ST, (yyvsp[-1].specval));}
#line 7442 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 514:
#line 2198 "cfg.y" /* yacc.c:1646  */
    {
				spec = (pv_spec_t*)pkg_malloc(sizeof(pv_spec_t));
				memset(spec, 0, sizeof(pv_spec_t));
				tstr.s = (yyvsp[-3].strval);
				tstr.len = strlen(tstr.s);
				if(pv_parse_spec(&tstr, spec)==NULL)
				{
					yyerror("unknown script variable in first parameter");
				}
				if(!pv_is_w(spec))
					yyerror("read-only script variable in first parameter");

				pvmodel = 0;
				tstr.s = (yyvsp[-1].strval);
				tstr.len = strlen(tstr.s);
				if(pv_parse_format(&tstr, &pvmodel)<0)
				{
					yyerror("error in second parameter");
				}

				mk_action2( (yyval.action), PV_PRINTF_T,
						SCRIPTVAR_ST, SCRIPTVAR_ELEM_ST, spec, pvmodel) ;
			}
#line 7470 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 515:
#line 2221 "cfg.y" /* yacc.c:1646  */
    {
				if(!pv_is_w((yyvsp[-3].specval)))
					yyerror("read-only script variable in first parameter");
				pvmodel = 0;
				tstr.s = (yyvsp[-1].strval);
				tstr.len = strlen(tstr.s);
				if(pv_parse_format(&tstr, &pvmodel)<0)
				{
					yyerror("error in second parameter");
				}

				mk_action2( (yyval.action), PV_PRINTF_T,
						SCRIPTVAR_ST, SCRIPTVAR_ELEM_ST, (yyvsp[-3].specval), pvmodel) ;
			}
#line 7489 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 516:
#line 2236 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_HOSTPORT_T,
														STR_ST, 0, (yyvsp[-1].strval), 0); }
#line 7496 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 517:
#line 2238 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7502 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 518:
#line 2239 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument,"
												" string expected"); }
#line 7509 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 519:
#line 2241 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_PORT_T, STR_ST,
														0, (yyvsp[-1].strval), 0); }
#line 7516 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 520:
#line 2243 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7522 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 521:
#line 2244 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"string expected"); }
#line 7529 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 522:
#line 2246 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_USER_T,
														STR_ST, 0, (yyvsp[-1].strval), 0); }
#line 7536 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 523:
#line 2248 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7542 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 524:
#line 2249 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"string expected"); }
#line 7549 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 525:
#line 2251 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_USERPASS_T,
														STR_ST, 0, (yyvsp[-1].strval), 0); }
#line 7556 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 526:
#line 2253 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7562 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 527:
#line 2254 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"string expected"); }
#line 7569 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 528:
#line 2256 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_URI_T, STR_ST,
														0, (yyvsp[-1].strval), 0); }
#line 7576 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 529:
#line 2258 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7582 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 530:
#line 2259 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
										"string expected"); }
#line 7589 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 531:
#line 2261 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), REVERT_URI_T, 0,0,0,0); }
#line 7595 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 532:
#line 2262 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), REVERT_URI_T, 0,0,0,0); }
#line 7601 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 533:
#line 2263 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), SET_DSTURI_T,
													STR_ST, 0, (yyvsp[-1].strval), 0); }
#line 7608 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 534:
#line 2265 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7614 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 535:
#line 2266 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
										"string expected"); }
#line 7621 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 536:
#line 2268 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), RESET_DSTURI_T,
															0,0,0,0); }
#line 7628 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 537:
#line 2270 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), RESET_DSTURI_T, 0,0,0,0); }
#line 7634 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 538:
#line 2271 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), ISDSTURISET_T, 0,0,0,0);}
#line 7640 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 539:
#line 2272 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), ISDSTURISET_T, 0,0,0,0); }
#line 7646 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 540:
#line 2273 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), FORCE_RPORT_T,
															0, 0, 0, 0); }
#line 7653 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 541:
#line 2275 "cfg.y" /* yacc.c:1646  */
    { mk_action2( (yyval.action), FORCE_RPORT_T,0, 0, 0, 0); }
#line 7659 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 542:
#line 2276 "cfg.y" /* yacc.c:1646  */
    {
					mk_action2( (yyval.action), FORCE_LOCAL_RPORT_T,0, 0, 0, 0); }
#line 7666 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 543:
#line 2278 "cfg.y" /* yacc.c:1646  */
    {
					mk_action2( (yyval.action), FORCE_LOCAL_RPORT_T,0, 0, 0, 0); }
#line 7673 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 544:
#line 2280 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2( (yyval.action), FORCE_TCP_ALIAS_T,NUMBER_ST, 0,
					(void*)(yyvsp[-1].intval), 0);
		}
#line 7682 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 545:
#line 2284 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2( (yyval.action), FORCE_TCP_ALIAS_T,0, 0, 0, 0);
		}
#line 7690 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 546:
#line 2287 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2( (yyval.action), FORCE_TCP_ALIAS_T,0, 0, 0, 0);
		}
#line 7698 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 547:
#line 2290 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0;
					yyerror("bad argument, number expected");
					}
#line 7706 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 548:
#line 2293 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2( (yyval.action), SET_ADV_ADDR_T, STR_ST,
											0, (yyvsp[-1].strval), 0);
								}
#line 7715 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 549:
#line 2297 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument, "
														"string expected"); }
#line 7722 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 550:
#line 2299 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7728 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 551:
#line 2300 "cfg.y" /* yacc.c:1646  */
    {
								tstr.s = int2str((yyvsp[-1].intval), &tstr.len);
								if (!(tmp = pkg_malloc(tstr.len + 1))) {
										LM_CRIT("out of pkg memory\n");
										(yyval.action) = 0;
										YYABORT;
								} else {
									memcpy(tmp, tstr.s, tstr.len);
									tmp[tstr.len] = '\0';
									mk_action2((yyval.action), SET_ADV_PORT_T, STR_ST,
											   0, tmp, 0);
								}
								            }
#line 7746 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 552:
#line 2313 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2((yyval.action), SET_ADV_PORT_T,
										   STR_ST, NOSUBTYPE,
										   (yyvsp[-1].strval), NULL);
								}
#line 7756 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 553:
#line 2318 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument "
						"(string or integer expected)"); }
#line 7763 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 554:
#line 2320 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7769 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 555:
#line 2321 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2( (yyval.action), FORCE_SEND_SOCKET_T,
									SOCKID_ST, 0, (yyvsp[-1].sockid), 0);
								}
#line 7778 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 556:
#line 2325 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerror("bad argument,"
								" proto:host[:port] expected");
								}
#line 7786 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 557:
#line 2328 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7792 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 558:
#line 2329 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2( (yyval.action), SERIALIZE_BRANCHES_T,
									NUMBER_ST, 0, (void*)(long)(yyvsp[-1].intval), 0);
								}
#line 7801 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 559:
#line 2333 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("bad argument,"
								" number expected");
								}
#line 7809 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 560:
#line 2336 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7815 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 561:
#line 2337 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2( (yyval.action), NEXT_BRANCHES_T, 0, 0, 0, 0);
								}
#line 7823 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 562:
#line 2340 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("no argument is"
								" expected");
								}
#line 7831 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 563:
#line 2343 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7837 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 564:
#line 2344 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2( (yyval.action), USE_BLACKLIST_T,
									STRING_ST, 0, (yyvsp[-1].strval), 0);
								}
#line 7846 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 565:
#line 2348 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("bad argument,"
								" string expected");
								}
#line 7854 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 566:
#line 2351 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7860 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 567:
#line 2352 "cfg.y" /* yacc.c:1646  */
    {
								mk_action2( (yyval.action), UNUSE_BLACKLIST_T,
									STRING_ST, 0, (yyvsp[-1].strval), 0);
								}
#line 7869 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 568:
#line 2356 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("bad argument,"
								" string expected");
								}
#line 7877 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 569:
#line 2359 "cfg.y" /* yacc.c:1646  */
    {(yyval.action)=0; yyerror("missing '(' or ')' ?"); }
#line 7883 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 570:
#line 2360 "cfg.y" /* yacc.c:1646  */
    {
									mk_action3( (yyval.action), CACHE_STORE_T,
													STR_ST,
													STR_ST,
													STR_ST,
													(yyvsp[-5].strval),
													(yyvsp[-3].strval),
													(yyvsp[-1].strval));
							}
#line 7897 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 571:
#line 2370 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-7].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-5].strval);
								elems[2].type = STR_ST;
								elems[2].u.data = (yyvsp[-3].strval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-1].intval);
								mk_action_((yyval.action), CACHE_STORE_T, 4, elems);
							}
#line 7913 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 572:
#line 2382 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-7].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-5].strval);
								elems[2].type = STR_ST;
								elems[2].u.data = (yyvsp[-3].strval);
								elems[3].type = SCRIPTVAR_ST;
								elems[3].u.data = (yyvsp[-1].specval);
								mk_action_((yyval.action), CACHE_STORE_T, 4, elems);
							}
#line 7929 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 573:
#line 2394 "cfg.y" /* yacc.c:1646  */
    {
									mk_action2( (yyval.action), CACHE_REMOVE_T,
													STR_ST,
													STR_ST,
													(yyvsp[-3].strval),
													(yyvsp[-1].strval));
							}
#line 7941 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 574:
#line 2401 "cfg.y" /* yacc.c:1646  */
    {
									mk_action3( (yyval.action), CACHE_FETCH_T,
													STR_ST,
													STR_ST,
													SCRIPTVAR_ST,
													(yyvsp[-5].strval),
													(yyvsp[-3].strval),
													(yyvsp[-1].specval));
							}
#line 7955 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 575:
#line 2410 "cfg.y" /* yacc.c:1646  */
    {
									mk_action3( (yyval.action), CACHE_COUNTER_FETCH_T,
													STR_ST,
													STR_ST,
													SCRIPTVAR_ST,
													(yyvsp[-5].strval),
													(yyvsp[-3].strval),
													(yyvsp[-1].specval));
							}
#line 7969 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 576:
#line 2419 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-7].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-5].strval);
								elems[2].type = NUMBER_ST;
								elems[2].u.number = (yyvsp[-3].intval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-1].intval);
								mk_action_((yyval.action), CACHE_ADD_T, 4, elems);
							}
#line 7985 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 577:
#line 2430 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-7].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-5].strval);
								elems[2].type = SCRIPTVAR_ST;
								elems[2].u.data = (yyvsp[-3].specval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-1].intval);
								mk_action_((yyval.action), CACHE_ADD_T, 4, elems);
							}
#line 8001 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 578:
#line 2441 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-9].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-7].strval);
								elems[2].type = NUMBER_ST;
								elems[2].u.number = (yyvsp[-5].intval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-3].intval);
								elems[4].type = SCRIPTVAR_ST;
								elems[4].u.data = (yyvsp[-1].specval);
								mk_action_((yyval.action), CACHE_ADD_T, 5, elems);
							}
#line 8019 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 579:
#line 2454 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-9].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-7].strval);
								elems[2].type = SCRIPTVAR_ST;
								elems[2].u.data = (yyvsp[-5].specval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-3].intval);
								elems[4].type = SCRIPTVAR_ST;
								elems[4].u.data = (yyvsp[-1].specval);
								mk_action_((yyval.action), CACHE_ADD_T, 5, elems);
							}
#line 8037 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 580:
#line 2467 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-7].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-5].strval);
								elems[2].type = NUMBER_ST;
								elems[2].u.number = (yyvsp[-3].intval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-1].intval);
								mk_action_((yyval.action), CACHE_SUB_T, 4, elems);
							}
#line 8053 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 581:
#line 2478 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-7].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-5].strval);
								elems[2].type = SCRIPTVAR_ST;
								elems[2].u.data = (yyvsp[-3].specval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-1].intval);
								mk_action_((yyval.action), CACHE_SUB_T, 4, elems);
							}
#line 8069 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 582:
#line 2489 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-9].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-7].strval);
								elems[2].type = NUMBER_ST;
								elems[2].u.number = (yyvsp[-5].intval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-3].intval);
								elems[4].type = SCRIPTVAR_ST;
								elems[4].u.data = (yyvsp[-1].specval);
								mk_action_((yyval.action), CACHE_SUB_T, 5, elems);
							}
#line 8087 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 583:
#line 2502 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-9].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-7].strval);
								elems[2].type = SCRIPTVAR_ST;
								elems[2].u.data = (yyvsp[-5].specval);
								elems[3].type = NUMBER_ST;
								elems[3].u.number = (yyvsp[-3].intval);
								elems[4].type = SCRIPTVAR_ST;
								elems[4].u.data = (yyvsp[-1].specval);
								mk_action_((yyval.action), CACHE_SUB_T, 5, elems);
							}
#line 8105 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 584:
#line 2515 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-5].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-3].strval);
								elems[2].type = STR_ST;
								elems[2].u.data = (yyvsp[-1].strval);
								mk_action_((yyval.action), CACHE_RAW_QUERY_T, 3, elems);
							}
#line 8119 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 585:
#line 2524 "cfg.y" /* yacc.c:1646  */
    {
								elems[0].type = STR_ST;
								elems[0].u.data = (yyvsp[-3].strval);
								elems[1].type = STR_ST;
								elems[1].u.data = (yyvsp[-1].strval);
								mk_action_((yyval.action), CACHE_RAW_QUERY_T, 2, elems);
							}
#line 8131 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 586:
#line 2531 "cfg.y" /* yacc.c:1646  */
    {
						 			cmd_tmp=(void*)find_cmd_export_t((yyvsp[-2].strval), 0, rt);
									if (cmd_tmp==0){
										if (find_cmd_export_t((yyvsp[-2].strval), 0, 0)) {
											yyerror("Command cannot be "
												"used in the block\n");
										} else {
											yyerrorf("unknown command <%s>, "
												"missing loadmodule?", (yyvsp[-2].strval));
										}
										(yyval.action)=0;
									}else{
										elems[0].type = CMD_ST;
										elems[0].u.data = cmd_tmp;
										mk_action_((yyval.action), MODULE_T, 1, elems);
									}
								}
#line 8153 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 587:
#line 2548 "cfg.y" /* yacc.c:1646  */
    {
									cmd_tmp=(void*)find_cmd_export_t((yyvsp[-3].strval),(yyvsp[-1].intval),rt);
									if (cmd_tmp==0){
										if (find_cmd_export_t((yyvsp[-3].strval), (yyvsp[-1].intval), 0)) {
											yyerror("Command cannot be "
												"used in the block\n");
										} else {
											yyerrorf("unknown command <%s>, "
												"missing loadmodule?", (yyvsp[-3].strval));
										}
										(yyval.action)=0;
									}else{
										elems[0].type = CMD_ST;
										elems[0].u.data = cmd_tmp;
										mk_action_((yyval.action), MODULE_T, (yyvsp[-1].intval)+1, elems);
									}
								}
#line 8175 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 588:
#line 2565 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0; yyerrorf("bad arguments for "
												"command <%s>", (yyvsp[-3].strval)); }
#line 8182 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 589:
#line 2567 "cfg.y" /* yacc.c:1646  */
    { (yyval.action)=0;
			yyerrorf("bare word <%s> found, command calls need '()'", (yyvsp[-1].strval));
			}
#line 8190 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 590:
#line 2570 "cfg.y" /* yacc.c:1646  */
    {
				mk_action1((yyval.action), XDBG_T, STR_ST, (yyvsp[-1].strval));	}
#line 8197 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 591:
#line 2572 "cfg.y" /* yacc.c:1646  */
    {
				mk_action1((yyval.action), XLOG_T, STR_ST, (yyvsp[-1].strval)); }
#line 8204 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 592:
#line 2574 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2((yyval.action), XLOG_T, STR_ST, STR_ST, (yyvsp[-3].strval), (yyvsp[-1].strval)); }
#line 8211 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 593:
#line 2576 "cfg.y" /* yacc.c:1646  */
    {
				mk_action1((yyval.action), RAISE_EVENT_T, STR_ST, (yyvsp[-1].strval)); }
#line 8218 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 594:
#line 2578 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2((yyval.action), RAISE_EVENT_T, STR_ST, SCRIPTVAR_ST, (yyvsp[-3].strval), (yyvsp[-1].specval)); }
#line 8225 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 595:
#line 2580 "cfg.y" /* yacc.c:1646  */
    {
				mk_action3((yyval.action), RAISE_EVENT_T, STR_ST, SCRIPTVAR_ST,
					SCRIPTVAR_ST, (yyvsp[-5].strval), (yyvsp[-3].specval), (yyvsp[-1].specval)); }
#line 8233 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 596:
#line 2583 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2((yyval.action), SUBSCRIBE_EVENT_T, STR_ST, STR_ST, (yyvsp[-3].strval), (yyvsp[-1].strval)); }
#line 8240 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 597:
#line 2585 "cfg.y" /* yacc.c:1646  */
    {
				mk_action3((yyval.action), SUBSCRIBE_EVENT_T, STR_ST, STR_ST,
					NUMBER_ST, (yyvsp[-5].strval), (yyvsp[-3].strval), (void*)(long)(yyvsp[-1].intval)); }
#line 8248 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 598:
#line 2588 "cfg.y" /* yacc.c:1646  */
    {
				elems[0].type = STR_ST;
				elems[0].u.data = (yyvsp[-11].strval);
				elems[1].type = STR_ST;
				elems[1].u.data = (yyvsp[-9].strval);
				elems[2].type = STR_ST;
				elems[2].u.data = (yyvsp[-7].strval);
				elems[3].type = STR_ST;
				elems[3].u.data = (yyvsp[-5].strval);
				elems[4].type = STR_ST;
				elems[4].u.data = (yyvsp[-3].strval);
				elems[5].type = SCRIPTVAR_ST;
				elems[5].u.data = (yyvsp[-1].specval);
				mk_action_((yyval.action), CONSTRUCT_URI_T,6,elems); }
#line 8267 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 599:
#line 2602 "cfg.y" /* yacc.c:1646  */
    {
				elems[0].type = SCRIPTVAR_ST;
				elems[0].u.data = (yyvsp[-3].specval);
				elems[1].type = SCRIPTVAR_ST;
				elems[1].u.data = (yyvsp[-1].specval);
				mk_action_((yyval.action), GET_TIMESTAMP_T,2,elems); }
#line 8278 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 600:
#line 2608 "cfg.y" /* yacc.c:1646  */
    {
				mk_action2((yyval.action), SCRIPT_TRACE_T, 0, 0, 0, 0); }
#line 8285 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 601:
#line 2610 "cfg.y" /* yacc.c:1646  */
    {
				pvmodel = 0;
				tstr.s = (yyvsp[-1].strval);
				tstr.len = strlen(tstr.s);
				if(pv_parse_format(&tstr, &pvmodel)<0)
					yyerror("error in second parameter");
				mk_action2((yyval.action), SCRIPT_TRACE_T, NUMBER_ST,
						   SCRIPTVAR_ELEM_ST, (void *)(yyvsp[-3].intval), pvmodel); }
#line 8298 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 602:
#line 2618 "cfg.y" /* yacc.c:1646  */
    {
				pvmodel = 0;
				tstr.s = (yyvsp[-3].strval);
				tstr.len = strlen(tstr.s);
				if(pv_parse_format(&tstr, &pvmodel)<0)
					yyerror("error in second parameter");
				mk_action3((yyval.action), SCRIPT_TRACE_T, NUMBER_ST,
						SCRIPTVAR_ELEM_ST, STR_ST, (void *)(yyvsp[-5].intval), pvmodel, (yyvsp[-1].strval)); }
#line 8311 "cfg.tab.c" /* yacc.c:1646  */
    break;

  case 603:
#line 2626 "cfg.y" /* yacc.c:1646  */
    {
				i_tmp = get_script_route_idx( (yyvsp[-1].strval), rlist, RT_NO, 0);
				if (i_tmp==-1) yyerror("too many script routes");
				mk_action2((yyval.action), ASYNC_T, ACTIONS_ST, NUMBER_ST,
						(yyvsp[-3].action), (void*)(long)i_tmp);
				}
#line 8322 "cfg.tab.c" /* yacc.c:1646  */
    break;


#line 8326 "cfg.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 2636 "cfg.y" /* yacc.c:1906  */


static inline void warn(char* s)
{
	LM_WARN("warning in config file %s, line %d, column %d-%d: %s\n",
			get_cfg_file_name, line, startcolumn, column, s);
}

static void yyerror(char* s)
{
	LM_CRIT("parse error in config file %s, line %d, column %d-%d: %s\n",
			get_cfg_file_name, line, startcolumn, column, s);
	cfg_errors++;
}

#define ERROR_MAXLEN 1024
static void yyerrorf(char *fmt, ...)
{
	char *tmp = pkg_malloc(ERROR_MAXLEN);
	va_list ap;
	va_start(ap, fmt);

	vsnprintf(tmp, ERROR_MAXLEN, fmt, ap);
	yyerror(tmp);

	pkg_free(tmp);
	va_end(ap);
}


static struct socket_id* mk_listen_id(char* host, enum sip_protos proto,
																	int port)
{
	struct socket_id* l;
	l=pkg_malloc(sizeof(struct socket_id));
	if (l==0){
		LM_CRIT("cfg. parser: out of memory.\n");
	}else{
		l->name     = host;
		l->adv_name = NULL;
		l->adv_port = 0;
		l->proto    = proto;
		l->port     = port;
		l->children = 0;
		l->next     = NULL;
	}

	return l;
}

static struct socket_id* set_listen_id_adv(struct socket_id* sock,
											char *adv_name,
											int adv_port)
{
	sock->adv_name=adv_name;
	sock->adv_port=adv_port;
	return sock;
}
